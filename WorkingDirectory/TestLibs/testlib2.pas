{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O+,P+,Q-,R+,S+,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}

(*
   Program, using testlib running format:
   	checker.exe [ResultFileName]
*)

unit testlib2;

(* ================================================================= *)
                              interface
(* ================================================================= *)

const eofChar  = #$1A;
      eofRemap = ' ';
      NumberBefore = [#10,#13,' ',#09];
      NumberAfter  = [#10,#13,' ',#09,eofChar];
      lineAfter    = [#10,#13,eofChar];
      Blanks       = [#10,#13,' ',#09];
      eolnChar     = [#10,#13,eofChar];

type REAL = EXTENDED; {!!!!!!!!}

type CharSet = set of char;
     TMode   = (_Input, _Output, _Answer);
     TResult = (_OK, _WA, _PE,  _Fail, _Dirt);
               {_OK - accepted, _WA - wrong answer, _PE - output format mismatch,
                _Fail - when everything fucks up }
               {_Dirt - for inner using}

     TLStream = object
                    cur: char; { current char, =EofChar, if eof }
                    f: text; { file }
                    name: string; { file name }
                    mode: TMode;
                    opened: boolean;

                    { for internal usage }
                    constructor init (fname: string; m: TMode);
                    destructor destroy;

                    function CurChar: char; { returns cur }
                    function nextChar: char;  { moves to next char }

                    function seekeof: boolean;

                    function eof : boolean;  { == cur = EofChar }

                    function eoln: boolean;
                    function seekEoln: boolean;
                    procedure nextLine; { skips current line }

                    { Skips chars from given set }
                    { Does not generate errors }
                    procedure skip (setof: CharSet);

                    { Read word. Skip before all chars from `before`
                      and after all chars from `after`. If eof or word is
                      empty or it contains more than 255 chars, generates _pe }
                    function ReadWord (Before, After: CharSet): string;

                    { reads integer }
                    { _pe if error }
                    { USE readlongint! }
                    function ReadInteger: integer;

                    function ReadInt64: int64;

                    { reads longint }
                    { _pe if error }
                    function ReadLongint: longint;

                    { reads real }
                    { _pe if error }
                    function ReadReal: real;

                    procedure Reset;

                    { same as readword([], [#13 #10]) }
                    function ReadString: string;

                    { for internal usage }
                    procedure QUIT (res: TResult; msg: string);
                    procedure close;
		    function isOpened() : boolean;

                end;


procedure quit(res: TResult; msg: string);

var originalInput, originalOutput, userOutput: TLStream;
    ResultName: string;
    originalInputList,originalOutputList,userOutputList: array of TLStream;
    testIndex: integer;
    points : double;
   
(* ================================================================= *)
                              implementation
(* ================================================================= *)

{$ifdef VER70}
uses crt;
{$else}
uses windows;
{$endif}

{$ifndef VER70}
const LightGray = $07;LightRed  = $0c;LightCyan = $0b;LightGreen = $0a;

var input_file : Text;
    i,input_cnt,output_cnt : integer;
    str,taskID,taskName,testID : string;


procedure textcolor(x: word);
var h: thandle;
begin
    h := getstdhandle(std_output_handle);
    setconsoletextattribute(h, x);
end;
{$endif}

{$ifdef ver70}
procedure beep(freq, duration: integer);
begin
    sound(freq);
    delay(duration);
    nosound;
end;
{$endif}

const outcomes: array[0..7] of string = (
    'accepted',
    'wrong-answer',
    'presentation-error',
    'fail',
    'run-time-error',
    'time-limit-exceeded',
    'compilation-error',
    'security-violation' );

procedure safewrite(var t: text; s: string);
var
  i: longint;
begin
  for i := 1 to length(s) do
  begin
    case s[i] of
      '"': write('`');
      '<': write('&lt;');
      #0..#31: write(t, '.');
      else
        write(t, s[i]);
    end; { case }
  end;
end;


procedure quit (res: TResult; msg: string);
var ResFile: text;
    ErrorName: string;
    i : integer;

    procedure scr (color: word; msg: string);
    begin
       {if ResultName = '' then if no result file
       begin}
          TextColor (color);
          write (msg);
          TextColor (LightGray);
        {end;}
    end;

begin
   if (res = _OK) then
   begin
      userOutput.skip (Blanks);
      if not userOutput.eof then QUIT (_Dirt, 'Extra information in Output');
   end;

   case res of
      _Fail: begin beep(100, 300);
                   ErrorName := 'FAIL ';
                   Scr (LightRed, ErrorName);
             end;

      _Dirt: begin
                   ErrorName := 'wrong output format ';
                   Scr (LightCyan, ErrorName);
                   res := _PE;
                   msg := 'Extra information in output file';
             end;

      _PE: begin
              ErrorName := 'wrong output format ';
              Scr (LightRed, ErrorName);
           end;

      _OK: begin
              ErrorName := 'ok ';
              Scr (LightGreen, ErrorName);
           end;

      _WA: begin
              ErrorName := 'wrong answer ';
              TextColor (LightRed); scr (LightRed, ErrorName);
           end;

      else quit(_Fail, 'What is the code ??? ');
   end;


   if ResultName <> '' then
   begin
      assign (RESFILE, ResultName); { Create file with result of evaluation }
      rewrite (ResFile);
      if IORESULT <> 0 then QUIT (_Fail, 'Can not write to Result file');

      if (res=_ok) then begin
           writeln(resfile,1);
           writeln(resfile,points);
      end else begin
           writeln(resfile,0);
           writeln(resfile,ord(res));
      end;
      writeln(resfile,msg);
      close (ResFile);
      if IORESULT <> 0 then QUIT (_Fail, 'Can not write to Result file');
   end;

   Scr (LightGray, msg + ' ');
   writeln;

   if Res = _Fail then HALT (ord (res));

   if (input_cnt=1) then
      originalInput.destroy
   else
      for i:=1 to input_cnt do
        originalInputList[i-1].destroy;


   if (output_cnt=1) then begin
      originalOutput.destroy;
      userOutput.destroy;
   end else
      for i:=1 to output_cnt do begin
        originalOutputList[i-1].destroy;
        userOutputList[i-1].destroy;
      end;

   TextColor (LightGray);

   halt(0);
{
   if (res = _OK) or (ResultName <> '') then HALT (0)
                                        else HALT (ord (res));
}
end;

destructor TLStream.destroy;
begin
if (opened) then 
  CloseFile(f);
end;

constructor TLStream.init (fname: string; m: TMode);
begin
   name := fname;
   mode := m;
   assign (f, fname);
   opened:=false;
   {$I-}
   system.reset (f);
   {$I+}
   if IORESULT <> 0 then
   begin
      if mode = _answer then QUIT (_PE, 'File not found ' + fname);
       cur := EofChar;
   end
   else begin
      if system.eof (f) then cur := EofChar
                        else begin cur := ' '; nextchar end;
      opened := true;
   end;
end;

function TLStream.curchar: char;
begin
   curchar := cur
end;

function TLStream.nextChar: char;
begin
   nextChar:= curChar;
   if cur = EofChar then { do nothing }
   else if system.eof (f) then cur := EofChar
   else begin
      {$I-} read (f, cur); {$I+}
      if IORESULT <> 0 then Quit (_Fail, 'Read error' + name);
      if cur = eofChar then cur:= eofRemap;
   end;
end;

procedure TLStream.quit(res: TResult; msg: string);
begin
   if mode = _Answer then TESTLIB2.QUIT (res, msg)
   { if can't read input or answer - fail }
   else TESTLIB2.QUIT (_Fail, msg + ' (' + name + ')');
end;

function TLStream.ReadWord (Before, After: CharSet): string;
var res: string;
begin
   while cur in Before do nextchar;

   if (cur = EofChar) and not (cur in after) then
     quit(_PE,'Unexpected end-of-file');

   res := '';
   while not ((cur IN AFTER) or (cur = EofChar))  do
   begin
      res := res + cur;
      nextchar;
   end;
   
   ReadWord := res;
end;


function TLStream.ReadInteger: integer;
var help: string;
    res: longint;
    code: integer;
begin
   help := ReadWord (NumberBefore, NumberAfter);
   val (help, res, code);
   if code <> 0 then QUIT (_PE, 'Expected integer instead of "' + help + '"');
   // TODO : REVIEW
   if (res < -32768) or (res > 32767) then
                     QUIT (_PE, 'ReadInteger can not return LONGINT Value, DO NOT USE READINTEGER!!!');
   ReadInteger := res
end;

function TLStream.ReadInt64: int64;
var help: string;
    res: int64;
    code: integer;
begin
   help := ReadWord (NumberBefore, NumberAfter);
   val (help, res, code);
   if code <> 0 then QUIT (_PE, 'Expected integer instead of "' + help + '"');
   ReadInt64 := res
end;


function TLStream.ReadReal: real;
var help: string;
    res: real;
    code: integer;
begin
   help := ReadWord (NumberBefore, NumberAfter);
   val (help, res, code);
   if code <> 0 then QUIT (_PE, 'Expected real instead of "' + help + '"');
   ReadReal := res
end;

function TLStream.ReadLongint: longint;
var help: string;
    res: longint;
    code: integer;
begin
   help := ReadWord (NumberBefore, NumberAfter);
   val (help, res, code);
   if code <> 0 then QUIT (_PE, 'Expected longint instead of "' + help + '"');
   ReadLongint := res
end;

procedure TLStream.skip (setof: CharSet);
begin
   while (cur in setof) and (cur <> eofchar) do nextchar;
end;

function TLStream.eof: boolean;
begin
   eof := cur = eofChar;
end;

function TLStream.seekEof: boolean;
begin
   while (cur in Blanks) do nextchar;
   seekeof := cur = EofChar;
end;

function TLStream.eoln: boolean;
begin
  eoln:= cur in eolnChar;
end;

function TLStream.seekEoln: boolean;
begin
  skip ( [' ', #9] );
  seekEoln:= eoln;
end;

procedure TLStream.nextLine;
begin
  while not (cur in eolnChar) do nextchar;
  if cur = #13 then nextchar; 
  if cur = #10 then nextchar; 
end;

function TLStream.ReadString: string;
begin
  readstring:= ReadWord ([], [#13,#10]);
  nextLine;
end;

procedure TLStream.Reset;
begin
   {$I-} system.reset (f); {$I+}
   if IORESULT <> 0 then
   begin
      cur := EofChar; { allow for other files }
   end
   else
      if system.eof (f) then cur := EofChar
                        else begin cur := ' '; nextchar end;
   opened := true;

end;

procedure TLStream.close;
begin
  if opened then system.close(f);
  opened := false;
end;

function TLStream.isOpened() : boolean;
begin
  isOpened:=opened;
end;

function upper(s: string): string;
var
  i: longint;
begin
  for i := 1 to length(s) do
    s[i] := upcase(s[i]);
  upper := s;
end;

begin
   if (ParamCount=0) then
        ResultName:='checker.out'
   else
        ResultName:= ParamStr(1);

   AssignFile(input_file,'checker.in');
{$I-}
   Reset(input_file);
{$I+}
   if (IOResult<>0) then
      quit(_Dirt,'Can''t find checker.in file');

   readln(input_file,input_cnt);
   if (input_cnt=1) then begin
       readln(input_file,str);
       originalInput.init (str,_Input);
   end else begin
        SetLength(originalInputList,input_cnt);
         for i:=1 to input_cnt do begin
         readln(input_file,str);
          originalInputList[i-1].init(str,_Input);
        end;
   end;
   readln(input_file,output_cnt);
   if (output_cnt=1) then begin
       readln(input_file,str);
       originalOutput.init(str,_Output);
       readln(input_file,str);
       userOutput.init(str,_Answer);
       if not userOutput.opened then
          quit(_PE,'File '+str+' not found');
   end else begin
        SetLength(originalOutputList,output_cnt);
        SetLength(userOutputList,output_cnt);
        for i:=1 to output_cnt do begin
          readln(input_file,str);
          originalOutputList[i-1].init(str,_Output);
        end;
        for i:=1 to output_cnt do begin
          readln(input_file,str);
          userOutputList[i-1].init(str,_Answer);
          if not userOutputList[i-1].opened then
             quit(_PE,'File '+str+' not found');
        end;
   end;
   readln(input_file,taskID);
   readln(input_file,taskName);
   readln(input_file,testID);
   readln(input_file,points);
   readln(input_file,testIndex);
   TextColor(LightGray);
   Write('TestIndex=',testIndex,' taskID=',taskID,' taskName=',taskName,' testID=',testID,':');
end.
