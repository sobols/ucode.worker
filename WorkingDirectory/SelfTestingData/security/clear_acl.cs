using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security;
using System.Security.AccessControl;

namespace Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSecurity accessControl = File.GetAccessControl("input.txt");
            RemoveAccessRules(accessControl);
            using (var input = new StreamReader("input.txt"))
            {
                string[] tokens = input.ReadLine().Split(' ');
                var a = long.Parse(tokens[0]);
                var b = long.Parse(tokens[1]);
                using (var output = File.Create("output.txt", 1024, FileOptions.None, accessControl))
                {
                    using (var writer = new StreamWriter(output))
                    {
                        writer.WriteLine(a + b);
                    }
                }
            }
        }

        private static void RemoveAccessRules(FileSystemSecurity accessControl)
        {
            var collection = accessControl.GetAccessRules(true, false, typeof(System.Security.Principal.SecurityIdentifier));
            foreach (AccessRule rule in collection)
            {
                var fsRule = rule as FileSystemAccessRule;
                if (fsRule != null)
                {
                    accessControl.RemoveAccessRule(fsRule);
                }
            }
            accessControl.SetAccessRuleProtection(true, false);
        }
    }
}
