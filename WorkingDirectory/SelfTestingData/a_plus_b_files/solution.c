#include <stdio.h>

int main() {
    long long a, b;
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    scanf("%I64d %I64d", &a, &b);
    printf("%I64d\n", a + b);

    return 0;
}
