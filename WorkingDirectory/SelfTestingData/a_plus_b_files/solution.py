from __future__ import print_function
import random

line = open('input.txt').readline()
a, b = line.strip().split()
s = int(a) + int(b)
with open('output.txt', 'w') as fd:
    print(s, file=fd)
