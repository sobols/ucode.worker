using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var input = new StreamReader("input.txt"))
            {
                using (var output = new StreamWriter("output.txt"))
                {
                    string[] tokens = input.ReadLine().Split(' ');
                    var a = long.Parse(tokens[0]);
                    var b = long.Parse(tokens[1]);

                    output.WriteLine(a + b);
                }
            }
        }
    }
}
