import java.io.*;
import java.util.*;
import java.math.BigInteger;

public class Solution
{
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(new File("input.txt"));
        PrintWriter out = new PrintWriter(new FileWriter("output.txt"));

        BigInteger a = new BigInteger(in.next());
        BigInteger b = new BigInteger(in.next());
        out.println(a.add(b));
        out.flush();

        // комментарий
    }
}
