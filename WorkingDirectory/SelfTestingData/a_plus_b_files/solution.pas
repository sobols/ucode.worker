program aplusb;
var
    a, b: int64;
begin
    assign(input, 'input.txt');
    reset(input);
    assign(output, 'output.txt');
    rewrite(output);

    read(a, b);
    writeln(a + b);

    close(input);
    close(output);
end.
