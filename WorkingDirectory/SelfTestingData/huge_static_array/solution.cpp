#include <iostream>

int data[(int)2.1e9 / sizeof(int)];

int main() {
    data[0] = 42;
    int a, b;
    std::cin >> a >> b;
    std::cout << a + b << '\n';
    return 0;
}
