﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstallationHelper
{
    class Program
    {
        const string UCodeTesterUser = "UCodeTester";
        const string UCodeTesterPassword = "3R8DTgz33CYBzs7TpxShLUmb";
        
        const string UCodeMasterUser = "UCodeMaster";
        const string UCodeMasterPassword = "bqnZGdxPEkcAYPjxZMKSmC9H";

        private static void CreateUser(string userName, string password, string description, string[] privileges)
        {
            Console.WriteLine("Creating Windows user '{0}'", userName);
            PrincipalContext context = new PrincipalContext(ContextType.Machine);
            var user = UserPrincipal.FindByIdentity(context, IdentityType.Name, userName);
            if (user != null)
            {
                Console.WriteLine("FAIL: found a user with SID {0}, nothing to do", user.Sid);
            }
            else
            {
                user = new UserPrincipal(context);
                user.SetPassword(password);
                user.DisplayName = userName;
                user.Name = userName;
                user.Description = description;
                user.UserCannotChangePassword = true;
                user.PasswordNeverExpires = true;
                user.Save();

                Console.WriteLine("OK");

                foreach (var privilege in privileges)
                {
                    long retCode = LSAUtility.SwitchAccountPrivilege(userName, privilege, LSAUtility.Action.Add);
                    Console.WriteLine("- Granting privilege {0}: {1}", privilege, retCode == 0 ? "OK" : "FAIL");
                }
            }
        }

        private static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static void PrintStatus()
        {
            Console.WriteLine("Installed Services:");
            ServiceController[] services = ServiceController.GetServices();
            bool ok = false;
            foreach (var service in services)
            {
                if (service.ServiceName.StartsWith("UCode"))
                {
                    Console.WriteLine("  - {0} ({1})", service.ServiceName, service.DisplayName);
                    ok = true;
                }
            }
            if (!ok)
            {
                Console.WriteLine("[no related services found]");
            }
            Console.WriteLine();

            ok = false;
            PrincipalContext context = new PrincipalContext(ContextType.Machine);
            Console.WriteLine("Present Users:");
            foreach (var user in new string[] { UCodeTesterUser, UCodeMasterUser })
            {
                if (UserPrincipal.FindByIdentity(context, IdentityType.Name, user) != null)
                {
                    Console.WriteLine("  - {0}", user);
                    ok = true;
                }
            }
            if (!ok)
            {
                Console.WriteLine("[no related users found]");
            }
            Console.WriteLine();
        }

        private static void CreateUsers()
        {
            CreateUser(UCodeTesterUser, UCodeTesterPassword,
                "User with limited rights that runs compilers and checkers, generators, solutions (untrusted code)",
                new string[] { "SeBatchLogonRight" });
            CreateUser(UCodeMasterUser, UCodeMasterPassword,
                "User that runs UCode.Worker and controls testing process",
                new string[] { "SeServiceLogonRight", "SeAssignPrimaryTokenPrivilege", "SeTakeOwnershipPrivilege", "SeIncreaseQuotaPrivilege" });
        }

        private static void ManageService(bool install)
        {
            const string fileName = "UCode.Worker.exe";
            if (!File.Exists(fileName))
            {
                Console.WriteLine("Executable file not found");
                return;
            }

            if (install)
            {
                Thread thread = new Thread(() => Clipboard.SetText(UCodeMasterPassword));
                thread.SetApartmentState(ApartmentState.STA); //Set the thread to STA
                thread.Start();
                thread.Join();
            }

            ManagedInstallerClass.InstallHelper(install ? new[] { fileName } : new[] { "/u", fileName });

            Console.WriteLine(install ? "Successfully installed" : "Successfully uninstalled");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("=== UCode.Worker installation helper ===");
            bool isAdmin = IsAdministrator();
            Console.WriteLine("Running as Administrator: {0}", isAdmin);
            Console.WriteLine();

            if (isAdmin)
            {
                while (true)
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine("Select action:");
                    Console.WriteLine("1) print status");
                    Console.WriteLine("2) install service");
                    Console.WriteLine("3) uninstall service");
                    Console.WriteLine("4) create users");
                    Console.WriteLine("0) exit");
                    Console.ResetColor();
                    Console.Write("your choise: ");

                    string line = Console.ReadLine();
                    if (String.IsNullOrWhiteSpace(line))
                    {
                        break;
                    }

                    int code = Int32.Parse(line);
                    if (code == 0)
                    {
                        break;
                    }

                    Console.WriteLine();

                    switch (code)
                    {
                        case 1:
                            PrintStatus();
                            break;
                        case 2:
                            ManageService(true);
                            break;
                        case 3:
                            ManageService(false);
                            break;
                        case 4:
                            CreateUsers();
                            break;
                    }
                }
            }
            else
            {
                Console.WriteLine("Administrator rights are required.");
                Console.WriteLine("Press ENTER to exit...");
                Console.ReadLine();
            }
        }
    }
}
