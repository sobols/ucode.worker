﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.Remoting.Messaging;
using UCode.Worker;
using UCode.Worker.Messages;

namespace WorkerTests
{
    [TestClass]
    public class ScorerTest
    {
        [TestMethod]
        public void TestEscape()
        {
            Assert.AreEqual("hello\\;world\\;\\;", TestLibScorer.Escape("hello;world;;"));
            Assert.AreEqual("C:\\\\Windows", TestLibScorer.Escape("C:\\Windows"));
            Assert.AreEqual("one\\ntwo\\nthree\\n", TestLibScorer.Escape("one\ntwo\nthree\n"));
        }

        [TestMethod]
        public void TestLine()
        {
            var result = new TestCaseResult(
                123,
                TestCaseOutcome.OK,
                CheckFailedReason.Unknown,
                15,
                1024,
                0,
                1.0,
                EmptyMessage.Instance,
                null,
                null,
                null,
                null,
                null);
            Assert.AreEqual("1;;;OK;1;15;1024;;;;0;", TestLibScorer.Serialize(1, result));
        }
    }
}
