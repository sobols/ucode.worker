﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using UCode.Worker;

namespace WorkerTests
{
    [TestClass]
    public class ShLexTest
    {
        [TestMethod]
        public void TestQuote()
        {
            Assert.AreEqual(ShLex.Quote("aba"), "aba");
            Assert.AreEqual(ShLex.Quote("hello world"), "\"hello world\"");
            Assert.AreEqual(ShLex.Quote("C:\\Windows"), "C:\\Windows");
            Assert.AreEqual(ShLex.Quote("\"aba\""), "\\\"aba\\\"");
        }

        [TestMethod]
        public void TestJoin()
        {
            Assert.AreEqual(ShLex.Join(new string[] { "aba", "caba" }), "aba caba");
        }
    }
}
