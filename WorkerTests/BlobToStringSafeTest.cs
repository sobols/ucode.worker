﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UCode.Worker;
using System.Text;
using System.Linq;

namespace WorkerTests
{
    [TestClass]
    public class BlobToStringSafeTest
    {
        [TestMethod]
        public void TestBasic()
        {
            Check("", Encoding.ASCII.GetBytes(""));
            Check("a", Encoding.ASCII.GetBytes("a"));
            Check("abacaba", Encoding.ASCII.GetBytes("abacaba"));
        }

        [TestMethod]
        public void TestUtf8()
        {
            Check("привет", Encoding.UTF8.GetBytes("привет"));
            Check("Räksmörgås", Encoding.UTF8.GetBytes("Räksmörgås"));
        }

        [TestMethod]
        public void TestWin1251()
        {
            var encoding = Encoding.GetEncoding(1251);
            Check("привет", encoding.GetBytes("привет"));
        }

        [TestMethod]
        public void TestNoCrash()
        {
            byte[] all = new byte[256];
            for (int i = 0; i < 256; ++i)
            {
                all[i] = (byte)i;
            }
            StringUtils.BlobToStringSafe(all);
        }

        [TestMethod]
        public void TestBinary()
        {
            Check("!:.,'\\/` \\t$%", Encoding.UTF8.GetBytes("!:.,'\\/` \t$%"));
            Check("\\0\\0", Encoding.UTF8.GetBytes("\0\0"));
            Check("a\\nb\\t\\r", Encoding.UTF8.GetBytes("a\nb\t\r"));
            Check(@"\0\x01\x02", Encoding.UTF8.GetBytes("\u0000\u0001\u0002"));
        }

        private static void Check(string expected, byte[] blob)
        {
            Assert.AreEqual(expected, StringUtils.BlobToStringSafe(blob));
        }
    }


    [TestClass]
    public class ValidUTFTest
    {
        [TestMethod]
        public void TestYes()
        {
            Assert.IsTrue(StringUtils.IsValidUTF8(new byte[] { 0x0D }));
            Assert.IsTrue(StringUtils.IsValidUTF8(new byte[0]));
            Assert.IsTrue(StringUtils.IsValidUTF8(Encoding.ASCII.GetBytes("Just plain ASCII!")));
            Assert.IsTrue(StringUtils.IsValidUTF8(Encoding.ASCII.GetBytes("multiline\ntext\twith\tcontrol and binary \0\x03 chars")));
            Assert.IsTrue(StringUtils.IsValidUTF8(Encoding.UTF8.GetBytes("Кириллические символы")));
        }

        [TestMethod]
        public void TestNo()
        {
            Assert.IsFalse(StringUtils.IsValidUTF8(new byte[] { 0xD0 }));

            var encoding = Encoding.GetEncoding(1251);
            Assert.IsFalse(StringUtils.IsValidUTF8(encoding.GetBytes("привет")));

            byte[] all = Enumerable.Range(0, 256).Select(x => (byte)(x)).ToArray();
            Assert.IsFalse(StringUtils.IsValidUTF8(all));
        }
    }

    [TestClass]
    public class ExtractPointsTest
    {
        [TestMethod]
        public void TestCorrect()
        {
            var result = StringUtils.TryExtractPoints("1.25 Answer is correct");
            Assert.AreEqual(result.Message, "Answer is correct");
            Assert.AreEqual(result.Points, 1.25);
        }
        [TestMethod]
        public void TestCorrectSimple()
        {
            var result = StringUtils.TryExtractPoints("1.25");
            Assert.AreEqual(result.Message, "");
            Assert.AreEqual(result.Points, 1.25);
        }
        [TestMethod]
        public void TestIncorrect()
        {
            Assert.IsNull(StringUtils.TryExtractPoints("Answer is correct"));
        }
    }
}