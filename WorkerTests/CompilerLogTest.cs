﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UCode.Worker;

namespace WorkerTests
{
    [TestClass]
    public class CompilerLogTest
    {
        [TestMethod]
        public void TestReplacement()
        {
            var log = new CompilerLogProcessor();
            log.AddPattern("C:\\UCode.Worker\\compiler\\GCC", "$(HOME)");

            string origMsg =
                "solution.cpp(42): syntax error: ';'\n" +
                "c:\\ucode.worker\\compiler\\gcc\\include\\math.h(1): see declaration of 'j0'\n";

            string processedMsg =
                "solution.cpp(42): syntax error: ';'\r\n" +
                "$(HOME)\\include\\math.h(1): see declaration of 'j0'\r\n" +
                "Custom message\r\n";

            log.AppendRawLogData(Encoding.ASCII.GetBytes(origMsg));
            log.AppendMessage("Custom message");

            var result = log.FetchAsString();
            Assert.AreEqual(processedMsg, result);
        }

        [TestMethod]
        public void TestReplacementSimple()
        {
            var log = new CompilerLogProcessor();
            log.AddPattern("a", "b");
            log.AddPattern("b", "c");

            log.AppendRawLogData(Encoding.ASCII.GetBytes("aaa"));
            Assert.AreEqual("bbb\r\n", log.FetchAsString());
        }

        [TestMethod]
        public void TestReplacementOverlapping()
        {
            var log = new CompilerLogProcessor();
            log.AddPattern("a", "b");
            log.AddPattern("aa", "c");

            log.AppendRawLogData(Encoding.ASCII.GetBytes("aaa"));
            Assert.AreEqual("cb\r\n", log.FetchAsString());
        }

        [TestMethod]
        public void TestBrokenEncoding()
        {
            var log = new CompilerLogProcessor();
            log.AppendRawLogData(new byte[] { 0xFE, 0xFF });
            log.AppendMessage("hello");
            Assert.AreEqual("\uFFFD\uFFFDhello\r\n", log.FetchAsString());
        }
    }
}
