﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkerTests
{
    [TestClass]
    public class FileCompareCheckerTest
    {
        [TestMethod]
        public void TestMethod()
        {
            var helper = new CheckerTestingHelper();
            
            helper.AssertOK("", "");
            helper.AssertOK("", "\n");
            helper.AssertWA("", "\n\n");
            helper.AssertWA("", "a");
            helper.AssertWA("", "a\n");
            
            helper.AssertOK("\n", "");
            helper.AssertOK("\n", "\n");
            helper.AssertWA("\n", "a");
            helper.AssertWA("\n", "a\n");
            
            helper.AssertWA("\n\n", "");
            helper.AssertOK("\n\n", "\n");
            helper.AssertOK("\n\n", "\n\n");
            helper.AssertOK("\n\n", "\n\n\n");
            helper.AssertWA("\n\n", "\n\n\n\n");
            helper.AssertWA("\n\n", "\na");

            helper.AssertWA("a", "");
            helper.AssertWA("a", "\n");
            helper.AssertOK("a", "a");
            helper.AssertOK("a", "a\n");
            helper.AssertWA("a", "a\nb");
            helper.AssertWA("a", "a\nb\n");
            helper.AssertWA("a", "a\nb\nc");
            helper.AssertOK("a", "a\n\n"); // BAD

            helper.AssertWA("a\n", "\n");
            helper.AssertWA("a\n", "\na");
            helper.AssertWA("a\n", "\na\n");
            helper.AssertOK("a\n", "a");
            helper.AssertOK("a\n", "a\n");
            helper.AssertOK("a\n", "a\n\n");
            helper.AssertWA("a\n", "a\n\n\n");

            helper.AssertOK("aba", "aba");
            helper.AssertOK("aba\n", "aba");
            helper.AssertOK("aba", "aba\n");
            helper.AssertOK("aba\n", "aba\n");

            helper.AssertWA("aba", "abc");
        }

        private class CheckerTestingHelper
        {
            private UCode.Worker.FileCompareChecker Checker;

            public CheckerTestingHelper()
            {
                Checker = new UCode.Worker.FileCompareChecker(null);
            }

            public void AssertOK(string output, string answer)
            {
                Assert.AreEqual(UCode.Worker.CheckerOutcome.OK, Checker.Check(output, answer).Outcome);
            }
            public void AssertWA(string output, string answer)
            {
                Assert.AreEqual(UCode.Worker.CheckerOutcome.WrongAnswer, Checker.Check(output, answer).Outcome);
            }
        }
    }
}
