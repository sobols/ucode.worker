﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCode.Worker
{
    enum CheckerOutcome
    {
        OK,
        WrongAnswer,
        PresentationError,
        CheckFailed,
    }

    static class CheckerOutcomeExtensions
    {
        static public TestCaseOutcome ToTestCaseOutcome(this CheckerOutcome outcome)
        {
            switch (outcome)
            {
                case CheckerOutcome.OK:
                    return TestCaseOutcome.OK;
                case CheckerOutcome.WrongAnswer:
                    return TestCaseOutcome.WrongAnswer;
                case CheckerOutcome.PresentationError:
                    return TestCaseOutcome.PresentationError;
                case CheckerOutcome.CheckFailed:
                    return TestCaseOutcome.CheckFailed;
                default:
                    throw new InvalidCastException();
            }
        }
    }

    enum CheckFailedReason
    {
        Unknown,

        ReportedByChecker,

        UnexpectedExitCode,					// for Testlib-style checkers

        CheckerTimeLimitExceeded,

        NoCheckerOutFileFound,				// for iRunner-style checkers
        CheckerOutHasWrongFormat,			// for iRunner-style checkers

        InvalidInput,                       // for validators
    }

    class CheckingResult
    {
        public CheckingResult(CheckerOutcome outcome, ICheckerMessage message, Score score)
        {
            Outcome = outcome;
            FailReason = CheckFailedReason.Unknown;
            Message = (message != null) ? message : Messages.EmptyMessage.Instance;
            Score = score;
        }
        public CheckingResult(CheckFailedReason reason, ICheckerMessage message)
        {
            Outcome = CheckerOutcome.CheckFailed;
            FailReason = reason;
            Message = (message != null) ? message : Messages.EmptyMessage.Instance;
            Score = Score.Min();
        }
        public readonly CheckerOutcome Outcome;
        public readonly CheckFailedReason FailReason = CheckFailedReason.Unknown; // even if no "check failed"
        public readonly ICheckerMessage Message; // may be assumed to be not null
        public readonly Score Score;
    }

    class CheckingResultFactory
    {
        public double MaxScore { get; private set; }

        public CheckingResultFactory(double maxScore)
        {
            MaxScore = maxScore;
        }

        public CheckingResult Make(CheckerOutcome checkerOutcome, ICheckerMessage message, double score)
        {
            return new CheckingResult(checkerOutcome, message, Score.Points(score));
        }
        public CheckingResult Make(CheckerOutcome checkerOutcome, ICheckerMessage message)
        {
            switch (checkerOutcome)
            {
                case CheckerOutcome.OK:
                    return new CheckingResult(checkerOutcome, message, Score.Max());
                default:
                    return new CheckingResult(checkerOutcome, message, Score.Min());
            }
        }
        public CheckingResult Make(CheckFailedReason reason, ICheckerMessage message = null)
        {
            return new CheckingResult(reason, message);
        }
        public CheckingResult Make(double score, ICheckerMessage message = null)
        {
            return new CheckingResult(CheckerOutcome.OK, message, Score.Points(score));
        }
    }
}
