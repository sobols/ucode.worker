﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCode.Worker.SelfTestingMode
{
    class Language
    {
        public static bool IsPython(string langID)
        {
            return langID.StartsWith("PYTHON");
        }

        public static bool IsVisualStudioCpp(string langID)
        {
            return Regex.IsMatch(langID, @"^MVCPP2\d+");
        }

        public static bool IsMingwCpp(string langID)
        {
            return Regex.IsMatch(langID, @"^GNUCPP\d+");
        }

        public static bool IsOldCpp(string langID)
        {
            return langID == "BCC502" || langID == "MVCPP60";
        }

        public static bool IsCpp(string langID)
        {
            return IsVisualStudioCpp(langID) || IsMingwCpp(langID);
        }

        public static bool IsPascal(string langID)
        {
            return langID.StartsWith("FPC") || langID.StartsWith("DELPHI") || langID.StartsWith("PABCNET");
        }

        public static bool IsJava(string langID)
        {
            return langID.StartsWith("JAVA");
        }

        public static bool IsMsCSharp(string langID)
        {
            return langID.StartsWith("CSHARP4");
        }

        public static bool IsCSharp(string langID)
        {
            return IsMsCSharp(langID) || langID == "ROSLYN";
        }
    }
}
