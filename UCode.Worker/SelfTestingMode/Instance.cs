﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.SelfTestingMode
{
    class Instance : WorkerInstance
    {
        private readonly string DataDir_;

        public Instance(Configuration config)
            : base(config)
        {
            Sleeper_ = new ConstantSleeper(1);
            DataDir_ = config.SelfTestingDataDirectory;
        }

        void RunRealLifeSelfTesting(CancellationToken token)
        {
            ISingleFileStorage fs = RootDir_.Data.Create("all");
            TestingMachine machine = TestingMachineFactory_.Create(fs);
            var cases = new RealLifeCases(DataDir_, ProgLangs_.ListLangIDs(), fs, machine);
            cases.TestAll(token);
        }

        protected override void DoRun(CancellationToken token)
        {
            RunRealLifeSelfTesting(token);
            RunJsonSelfTesting(token);
            Logger.Info("Selt-testing is finished");
        }

        class FakeFileDownloader : ApiMode.IFileDownloader
        {
            ISingleFileStorage Storage_;

            public FakeFileDownloader(ISingleFileStorage storage)
            {
                Storage_ = storage;
            }
            public IFileLocation Download(string id)
            {
                return Storage_.Put(new MemoryDataSource(id));
            }
        }

        class FakeUploadPack : ApiMode.IUploadPack
        {
            public string EnqueueToUpload(IFileLocation file)
            {
                using (var stream = file.OpenRead())
                {
                    return ApiMode.ResourceIdUtils.Calculate(stream);
                }
            }
            public void Upload()
            {
            }
        }

        class FakeFileUploader : ApiMode.IFileUploader
        {
            public ApiMode.IUploadPack CreatePack()
            {
                return new FakeUploadPack();
            }
        }

        void RunJsonSelfTesting(CancellationToken token)
        {
            var jsonSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                MissingMemberHandling = MissingMemberHandling.Error,
            };

            ISingleFileStorage fs = RootDir_.Data.Create("jsons");
            ApiMode.JsonTester jsonTester = new ApiMode.JsonTester(TestingMachineFactory_, RootDir_.Data, new FakeFileDownloader(fs), new FakeFileUploader(), jsonSettings);
            Logger.Info(jsonTester.GetWelcomeMessage());

            string[] jsons = new string[]{
                "{}",
                "",
                "[]",
                "xxx",
                "{ source: {} }",
                "{ solution: {} }",
                "{ solution: { compiler: 'TRASH!!!', filename: 'sol.cpp', resourceId: 'int main() {}' } }",
                "{ solution: { compiler: 'GNUCPP472', filename: 'main.cpp', resourceId: 'donotcompile' } }",

                "{ problem: {} }",
                "{ problem: { tests: [ {} ] } }",

                @"{ solution: { compiler: 'GNUCPP472', filename: 'main.cpp', resourceId: 
                    '#include <iostream>
                    int main() { int a, b; std::cin >> a >> b; std::cout << a + b; }' },
                    problem: { tests: [{input:{resourceId:'2 2'}, answer:{resourceId:'4'}}] }}",

                @"{ solution: { compiler: 'GNUCPP472', filename: 'main.cpp', resourceId: 
                    '#include <iostream>
                    int main() { int a, b; std::cin >> a >> b; std::cout << a + b; }' },
                    problem: { tests: [{input:{resourceId:'2 2'}, answer:{resourceId:'5'}}] }}",

                @"{ problem: {validator: { source: { compiler: 'GNUCPP472', filename: 'valid.cpp', resourceId: 
                    '#include ""testlib.h""
                    int main() { registerValidation(); inf.readInt(1, 7); inf.readEoln() inf.readEof(); return 0; }' }},
                    tests: [{input:{resourceId:'2'}}, {input:{resourceId:'8'}}] }}",

                @"{ problem: {validator: { source: { compiler: 'GNUCPP472', filename: 'valid.cpp', resourceId: 
                    '#include ""testlib.h""
                    int main() { registerValidation(); inf.readInt(1, 7); inf.readEof(); return 0; }' }},
                    tests: [{input:{resourceId:'8'}}, {input:{resourceId:'2'}}, {input:{resourceId:'a'}}, {input:{resourceId:''}}] }}",

                /* Slow!
                   @"{ problem: {validator: { source: { compiler: 'GNUCPP472', filename: 'valid.cpp', resourceId: 
                    '#include ""testlib.h""
                    int main() { registerValidation(); while(1); return 0; }' }},
                    tests: [{input:{resourceId:'8'}}, {input:{resourceId:'2'}}, {input:{resourceId:'a'}}, {input:{resourceId:''}}] }}",
                 */

                @"{ problem: {validator: { source: { compiler: 'GNUCPP472', filename: 'valid.cpp', resourceId: 
                    '#include ""testlib.h""
                    int main() { registerValidation(); inf.readInt(1, 7); inf.readEof(); return 0; }' }},
                    tests: [{input:{resourceId:'8'}, answer:{resourceId:'8'}}] }}",

                @"{ problem: {validator: { source: { compiler: 'GNUCPP472', filename: 'valid.cpp', resourceId: 
                    '#include ""testlib.h""
                    int main() { registerValidation(); inf.readInt(1, 7); inf.readEof(); return 0; }' }},
                    tests: [{}] }}",

                @"{ solution: { compiler: 'GNUCPP472', filename: 'main.cpp', resourceId: 
                    '#include <iostream>
                    int main() { std::cout << 4; }' },
                    problem: { inputFileName: 'input.txt', outputFileName: '', tests: [{answer:{resourceId:'4'}}, {answer:{resourceId:'5'}}] }}",

                @"{ solution: { compiler: 'GNUCPP1320-23', filename: 'main.cpp', resourceId: 
                    '#include <iostream>
                    int main() { int a, b; std::cin >> a >> b; std::cout << a + b; }' },
                    problem: {
                        tests: [{input:{resourceId:'2 2'}, answer:{resourceId:'4'}}],
                        scorer: { source: { compiler: 'GNUCPP1320-23', filename: 'scorer.cpp', resourceId: 
                        '#include ""testlib.h""
                        int main(int argc, char* argv[]) {
	                        registerScorer(argc, argv, [](std::vector<TestResult> results) -> double {
		                        double sum = 0;
		                        for (const auto& result : results) {
			                        sum += result.points * 2;
		                        }
		                        return sum;
	                        });
	                        return 0;
                        }'
                    }}
                }}",
            };

            foreach (string jsonJob in jsons)
            {
                Logger.Info(">>> JOB: {0}", jsonJob);
                string jsonResult = jsonTester.Run(jsonJob, 0, null, token, (s) => Logger.Info(s));
                Logger.Info(">>> RESULT: {0}", jsonResult);
            }
        }
    }
}
