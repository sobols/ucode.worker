﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.SelfTestingMode
{
    class RealLifeCases
    {
        int JobCount = 0;
        string DataDir;
        IEnumerable<string> LangIDs;
        ISingleFileStorage Storage;
        TestingMachine Machine;

        ProblemInstance APlusBWithFiles_;
        ProblemInstance APlusBWithStreams_;

        public RealLifeCases(string dataDir, IEnumerable<string> langIDs, ISingleFileStorage storage, TestingMachine machine)
        {
            DataDir = dataDir;
            LangIDs = langIDs;
            Storage = storage;
            Machine = machine;

            APlusBWithFiles_ = APlusB.GetInstance(Storage, true);
            APlusBWithStreams_ = APlusB.GetInstance(Storage, false);
        }

        public void TestAll(CancellationToken token)
        {
            TestSecurity(token);
            TestIdle(token);
            TestAPlusBFiles(token);
            TestAPlusBStreams(token);
            TestHugeStaticArray(token);
        }

        private TestingJob CreateJob(string dir, string solutionName, string langID, ProblemInstance problem)
        {
            var path = Path.Combine(DataDir, dir, solutionName);
            var source = new SourceCode(langID, File.ReadAllText(path), solutionName);
            return new TestingJob(++JobCount, problem, source);
        }

        private IEnumerable<TestingJob> GenJobs(string dir, string solutionName, Func<string, bool> predicate, ProblemInstance problem)
        {
            var jobsToRun = new List<TestingJob>();
            foreach (string langID in LangIDs.Where(predicate))
            {
                jobsToRun.Add(CreateJob(dir, solutionName, langID, problem));
            }
            return jobsToRun;
        }

        private IEnumerable<TestingJob> GenJob(string dir, string solutionName, Func<string, bool> predicate, ProblemInstance problem)
        {
            var jobsToRun = new List<TestingJob>();
            string langID = LangIDs.Where(predicate).LastOrDefault();
            if (langID != null)
            {
                jobsToRun.Add(CreateJob(dir, solutionName, langID, problem));
            }
            return jobsToRun;
        }

        private void TestAPlusBFiles(CancellationToken token)
        {
            var dir = "a_plus_b_files";

            var jobsToRun = new List<TestingJob>();
            jobsToRun.AddRange(GenJobs(dir, "solution.cpp", Language.IsCpp, APlusBWithFiles_));
            jobsToRun.AddRange(GenJobs(dir, "solution.old.cpp", Language.IsOldCpp, APlusBWithFiles_));
            jobsToRun.AddRange(GenJobs(dir, "Solution.java", Language.IsJava, APlusBWithFiles_));
            jobsToRun.AddRange(GenJobs(dir, "solution.pas", Language.IsPascal, APlusBWithFiles_));
            jobsToRun.AddRange(GenJobs(dir, "solution.cs", Language.IsCSharp, APlusBWithFiles_));
            jobsToRun.AddRange(GenJobs(dir, "solution.py", Language.IsPython, APlusBWithFiles_));

            foreach (var job in jobsToRun)
            {
                var report = Machine.Test(job, token);
                Assert(report.Outcome == SubmissionOutcome.Correct);
            }
        }

        private void TestAPlusBStreams(CancellationToken token)
        {
            foreach (var job in GenJob("a_plus_b_streams", "solution.cpp", Language.IsCpp, APlusBWithStreams_))
            {
                var report = Machine.Test(job, token);
                Assert(report.Outcome == SubmissionOutcome.Correct);
            }
        }

        private void TestHugeStaticArray(CancellationToken token)
        {
            foreach (var job in GenJob("huge_static_array", "solution.cpp", Language.IsVisualStudioCpp, APlusBWithStreams_))
            {
                var report = Machine.Test(job, token);
                Assert(report.Outcome == SubmissionOutcome.SolutionCompilationError);
            }
        }

        private void TestIdle(CancellationToken token)
        {
            foreach (var job in GenJob("idle", "solution.cpp", Language.IsMingwCpp, APlusBWithFiles_))
            {
                var report = Machine.Test(job, token);
                Assert(report.Outcome == SubmissionOutcome.Incorrect);
                Assert(report.FirstFailedTestNo == 1);
                Assert(report.TestCaseResults[0].Outcome == TestCaseOutcome.IdlenessLimitExceeded);
            }
        }

        private void TestSecurity(CancellationToken token)
        {
            foreach (var name in new string[] { "read_only.cs", "clear_acl.cs" })
            {
                foreach (var job in GenJobs("security", name, Language.IsMsCSharp, APlusBWithFiles_))
                {
                    var report = Machine.Test(job, token);
                    Assert(report.Outcome == SubmissionOutcome.Correct);
                }
            }
        }

        private static void Assert(bool condition)
        {
            if (!condition)
            {
                throw new MechanismException("Self-testing failed");
            }
        }
    }
}
