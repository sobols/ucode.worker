﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.SelfTestingMode
{
    static class APlusB
    {
        private static TestCase MakeTest(ISingleFileStorage storage, string input, string answer, long timeLimit = 1000, long memoryLimit = 256 * 1024 * 1024)
        {
            var inputFile = new TestCaseFile(storage.Put(new MemoryDataSource(input + "\n")));
            var answerFile = new TestCaseFile(storage.Put(new MemoryDataSource(answer + "\n")));
            var test = new TestCase(0, inputFile, answerFile, timeLimit, memoryLimit);
            return test;
        }

        public static ProblemInstance GetInstance(ISingleFileStorage st, bool useFiles)
        {
            string inputName = useFiles ? "input.txt" : "";
            string outputName = useFiles ? "output.txt" : "";
            return new ProblemInstance("A + B", inputName, outputName, CheckerKind.IRunnerStd, null, null, null, null, new List<TestCase>{
                MakeTest(st, "2 2", "4"),
                MakeTest(st, "1 1", "2"),
                MakeTest(st, "0 0", "0"),
                MakeTest(st, "1 2", "3"),
                MakeTest(st, "-1 -1", "-2"),
                MakeTest(st, "42 1", "43")
            });
        }
    }
}
