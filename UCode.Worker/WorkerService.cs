﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace UCode.Worker
{
    public class WorkerService : ServiceBase
    {
        private readonly string HomeDirectory;
        private WorkerInstance Instance;

        public WorkerService(string homeDirectory)
        {
            HomeDirectory = homeDirectory;
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            try
            {
                base.OnStart(args);

                Configuration conf = Configuration.Load(HomeDirectory);
                if (conf.Debug)
                {
                    Logger.IsDebugLevelEnabled = true;
                }
                if (!String.IsNullOrEmpty(conf.MinLogLevel))
                {
                    Logger.MinLogLevel = conf.MinLogLevel;
                }

                switch (conf.Mode)
                {
                    case RunningMode.IRunner:
                        Instance = new IRunnerMode.Instance(conf);
                        break;
                    case RunningMode.SelfTesting:
                        Instance = new SelfTestingMode.Instance(conf);
                        break;
                    case RunningMode.Api:
                        Instance = new ApiMode.Instance(conf);
                        break;
                    default:
                        throw new MechanismException(String.Format("Unsupported mode: {0}", conf.Mode));
                }

                Instance.Start();
                Logger.Info("Service started successfully");
            }
            catch (Exception ex)
            {
                Logger.Error("Could not start Worker: {0}", ex.ToString());

                ExitCode = 1064; // ERROR_EXCEPTION_IN_SERVICE
                Stop();
                //throw;
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            if (Instance != null)
            {
                Instance.Join();
                Instance = null;
            }
            Logger.Info("Service stopped successfully");
        }

        public void RunAsConsoleApp(string[] args)
        {
            OnStart(args);
            Console.WriteLine("Press any key to gracefully terminate the Worker...");
            Console.ReadKey();
            Console.WriteLine("Terminating...");
            OnStop();
        }
    }
}
