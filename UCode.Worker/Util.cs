﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace UCode.Worker
{
    static class Util
    {
        private static DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static double BytesToMegabytes(long bytes)
        {
            return bytes / 1048576.0;
        }

        public static long CurrentTimeMillis()
        {
            return (long)((DateTime.UtcNow - Jan1st1970).TotalMilliseconds);
        }

        public static byte[] Compress(string src)
        {
            var raw = Encoding.UTF8.GetBytes(src);
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }

        public static string SplitPascalStringToWords(string src)
        {
            var sb = new StringBuilder();
            bool prevWasUpper = true;
            for (int i = 0; i < src.Length; ++i)
            {
                bool isUpper = Char.IsUpper(src[i]);
                if (isUpper && !prevWasUpper)
                {
                    sb.Append(' ');
                }
                sb.Append(src[i]);
                prevWasUpper = isUpper;
            }
            return sb.ToString();
        }

        public static string CapitalizeEnum(string src)
        {
            return Util.SplitPascalStringToWords(src).Replace(' ', '_').ToUpper();
        }

        public static byte[] ComputeBinarySHA1(Stream stream)
        {
            SHA1Managed sha = new SHA1Managed();
            byte[] checksum = sha.ComputeHash(stream);
            return checksum;
        }

        public static string ComputeSHA1(Stream stream)
        {
            byte[] checksum = ComputeBinarySHA1(stream);
            string sendCheckSum = BitConverter.ToString(checksum)
                .Replace("-", string.Empty).ToLower();
            return sendCheckSum;
        }

        public static string GetHumanReadableFileSize(double len)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }
    }

    static class ShLex
    {
        public static string Join(IEnumerable<string> args)
        {
            return String.Join(" ", args.Select((x) => Quote(x)));
        }

        public static string Quote(string original)
        {
            if (string.IsNullOrEmpty(original))
            {
                return original;
            }
            string value = Regex.Replace(original, @"(\\*)" + "\"", @"$1\$0");
            value = Regex.Replace(value, @"^(.*\s.*?)(\\*)$", "\"$1$2$2\"");
            return value;
        }
    }

    static class TestCaseOutcomeExtensions
    {
        public static string TwoLetterCode(this TestCaseOutcome outcome)
        {
            var repr = outcome.ToString();
            var upperCaseLetters = new String(repr.Where(Char.IsUpper).ToArray());
            if (upperCaseLetters.Length >= 2)
            {
                return upperCaseLetters.Substring(0, 2);
            }
            else
            {
                return repr.ToUpperInvariant().Substring(0, 2);
            }
        }
    }
}
