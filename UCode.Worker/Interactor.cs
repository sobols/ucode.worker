﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UCode.Worker.SeqNumericFileNameStorage;

namespace UCode.Worker
{
    interface IInteractor
    {
        IInteraction NewInteraction(string input, IDataDestination output, string answer);
    }

    interface IInteraction
    {
        void PrepareInteractiveExecution(ExecutionSettings settings);
        ExecutionCommand GetInteractorCommand();
        CheckingResult GetResult(ExecutionResult interactorResult);
    }

    class TestLibInteractor : IInteractor
    {
        /*
         *   Interactor, using testlib running format:
         *     interactor.exe <Input_File> <Output_File> [<Answer_File> [<Result_File> [-appes]]],
         *   Reads test from inf (mapped to args[1]), writes result to tout (mapped to argv[2],
         *   can be judged by checker later), reads program output from ouf (mapped to stdin),
         *   writes output to program via stdout (use cout, printf, etc).
         */

        private CompiledProgram Program_;

        public TestLibInteractor(CompiledProgram program)
        {
            Program_ = program;
        }
        public IInteraction NewInteraction(string input, IDataDestination output, string answer)
        {
            return new TestLibInteraction(Program_, input, output, answer);
        }
    }

    class TestLibInteraction: IInteraction
    {
        private CompiledProgram Interactor_;
        private string InputPath_;
        private IDataDestination Output_;
        private string AnswerPath_;

        private string OutputFilename_;
        private string ReportFilename_;
        private MemoryDataDestination Report_;

        public TestLibInteraction(CompiledProgram interactor, string inputPath, IDataDestination output, string answerPath)
        {
            Interactor_ = interactor;
            InputPath_ = inputPath;
            Output_ = output;
            AnswerPath_ = answerPath;

            OutputFilename_ = "output." + Path.GetRandomFileName();
            ReportFilename_ = "report." + Path.GetRandomFileName();
            Report_ = new MemoryDataDestination();
        }

        public void PrepareInteractiveExecution(ExecutionSettings settings)
        {
            settings.OutputFiles[OutputFilename_] = Output_;
            settings.OutputFiles[ReportFilename_] = Report_;
        }
        public ExecutionCommand GetInteractorCommand()
        {
            var command = new ExecutionCommand(Interactor_);
            command.TimeLimit = 30 * 1000; // 30 s
            command.MemoryLimit = 1 * 1024 * 1024 * 1024; // 1 GB
            command.CommandLineArgs.Add(InputPath_);
            command.CommandLineArgs.Add(OutputFilename_);
            command.CommandLineArgs.Add(AnswerPath_);
            command.CommandLineArgs.Add(ReportFilename_);
            return command;
        }
        public CheckingResult GetResult(ExecutionResult execResult)
        {
            string reportString = (Report_.OK ? StringUtils.BlobToStringSafe(Report_.Stream.ToArray()) : "");
            ICheckerMessage message = new Messages.RawStringMessage(reportString);
            var resultFactory = new CheckingResultFactory(1.0);

            if (execResult.Outcome == ExecutionResult.OutcomeEnum.OK || execResult.Outcome == ExecutionResult.OutcomeEnum.RuntimeError)
            {
                switch (execResult.ExitCode)
                {
                    case 0:
                        return resultFactory.Make(CheckerOutcome.OK, message);
                    case 1:
                        return resultFactory.Make(CheckerOutcome.WrongAnswer, message);
                    case 2:
                        return resultFactory.Make(CheckerOutcome.PresentationError, message);
                    case 3:
                    case 4:
                        return resultFactory.Make(CheckFailedReason.ReportedByChecker, message);
                    case 7:
                        var parseResult = StringUtils.TryExtractPoints(reportString);
                        if (parseResult == null)
                        {
                            return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, message);
                        }
                        return resultFactory.Make(parseResult.Points, new Messages.RawStringMessage(parseResult.Message));
                }
            }

            // It is unlikely to get here. The program either has been terminated or has died.
            // No sense to look at the message printed by program, we build our own.
            message = new Messages.RawStringMessage(String.Format("Interactor: {0}", execResult));

            switch (execResult.Outcome)
            {
                case ExecutionResult.OutcomeEnum.RuntimeError:
                    return resultFactory.Make(CheckFailedReason.UnexpectedExitCode, message);
                case ExecutionResult.OutcomeEnum.TimeLimitExceeded:
                    return resultFactory.Make(CheckFailedReason.CheckerTimeLimitExceeded, message);
            }
            return resultFactory.Make(CheckFailedReason.Unknown, message);
        }
    }
}
