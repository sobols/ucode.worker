﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class SourceCode
    {
        public string LangID;
        public byte[] Data;
        public string Title;

        private static string DefaultTitle = "Solution";
        private static Regex GoodTitleRegex = new Regex(@"^[0-9a-zA-Z_\.,\-]{1,64}$");

        public SourceCode(string langID, byte[] data, string fileName = null)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            LangID = langID;
            Data = data;
            Title = GenerateTitle(fileName);
        }

        public SourceCode(string langID, string data, string fileName = null)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            LangID = langID;
            Data = Encoding.UTF8.GetBytes(data);
            Title = GenerateTitle(fileName);
        }

        public string GetSrcHash()
        {
            byte[] dataHash;
            using (SHA256 shaM = new SHA256Managed())
            {
                dataHash = shaM.ComputeHash(Data);
            }
            return String.Format("{0} - {1} - {2}", LangID, Title, Convert.ToBase64String(dataHash));
        }

        private static string GenerateTitle(string fileName)
        {
            if (fileName != null)
            {
                string title = Path.GetFileNameWithoutExtension(fileName);
                if (GoodTitleRegex.IsMatch(title))
                {
                    return title;
                }
            }
            // fallback
            return DefaultTitle;
        }
    }
}
