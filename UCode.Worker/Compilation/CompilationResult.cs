﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class CompiledProgram
    {
        public readonly string Title;
        public readonly string BaseDirectory;
        public readonly string LangID;
        public readonly long SizeInBytes;
        public readonly int FileCount;

        public CompiledProgram(string title, string baseDirectory, string langID, long sizeInBytes, int fileCount)
        {
            Title = title;
            BaseDirectory = baseDirectory;
            LangID = langID;
            SizeInBytes = sizeInBytes;
            FileCount = fileCount;
        }
    }

    class CompilationResult
    {
        public enum OutcomeEnum
        {
            Success,
            UnknownProgrammingLanguage,
            ResourceLimitsExceeded,
            Error,
        }

        public readonly OutcomeEnum Outcome;
        public readonly CompiledProgram Program;
        public readonly string CompilationLog;
        public readonly IFileLocation CompilationLogFile;

        public bool OK
        {
            get
            {
                return (Outcome == OutcomeEnum.Success);
            }
        }

        // Constructors

        // Compilation fails
        public CompilationResult(OutcomeEnum outcome)
        {
            if (outcome == OutcomeEnum.Success)
            {
                throw new ArgumentException();
            }
            Outcome = outcome;
        }

        public CompilationResult(OutcomeEnum outcome, string message, IFileLocation messageFile)
        {
            if (outcome == OutcomeEnum.Success)
            {
                throw new ArgumentException();
            }
            Outcome = outcome;
            Program = null;
            CompilationLog = message;
            CompilationLogFile = messageFile;
        }

        // Compilation successes
        public CompilationResult(CompiledProgram program, string message, IFileLocation messageFile)
        {
            Outcome = OutcomeEnum.Success;
            Program = program;
            CompilationLog = message;
            CompilationLogFile = messageFile;
        }
    }
}
