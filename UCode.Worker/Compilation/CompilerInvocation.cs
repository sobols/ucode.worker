﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using CompilerEnum = UCode.Worker.CompilationResult.OutcomeEnum;

namespace UCode.Worker
{
    class CompilerInvocation
    {
        private const string COMPILATION_LOG = "compilation.log";
        private const string ARTIFACTS = "artifacts";

        private readonly SourceCode Source_;
        private readonly ProgrammingLanguage Language_;
        private readonly CompilerLogProcessor Log_;
        private readonly string CommonLibsDirectory_;
        private readonly string TempDirectory_;
        private readonly string TargetDirectory_;

        public CompilerInvocation(SourceCode source, ProgrammingLanguage language, CompilerLogProcessor log,
            string commonLibsDirectory, string tempDirectory, string targetDirectory)
        {
            Source_ = source;
            Language_ = language;
            Log_ = log;
            CommonLibsDirectory_ = commonLibsDirectory;
            TempDirectory_ = tempDirectory;
            TargetDirectory_ = targetDirectory;

            Logger.Debug("Temp dir: '{0}'; target dir: '{1}'", TempDirectory_, TargetDirectory_);
        }

        public CompilationResult Run(int? cpuCore, CancellationToken token)
        {
            var args = new Dictionary<string, string>() {
                { "title" , Source_.Title },
                { "home", Language_.HomeDirectory },
                { "windows", Environment.GetFolderPath(Environment.SpecialFolder.Windows) },
                { "common", GetCommonLibsDirectory() },
            };
            var formatter = new StringFormatter(args);

            // this is just the name of file, no directories
            string sourceFileName = formatter.Format(Language_.SrcFile);

            File.WriteAllBytes(Path.Combine(TempDirectory_, sourceFileName), Source_.Data);

            formatter.AddRule("srcFile", sourceFileName);

            string fullCommandLine = formatter.Format(Language_.CompileCmd);
            if (!String.IsNullOrWhiteSpace(Language_.UTF8SourceOption))
            {
                if (StringUtils.IsValidUTF8(Source_.Data))
                {
                    fullCommandLine += " " + Language_.UTF8SourceOption;
                }
            }

            if (String.IsNullOrEmpty(fullCommandLine))
            {
                // Nothing to compile, pass the source file directly to destination
                return MakeSuccess(formatter);
            }

            string stdOutFile = Path.GetRandomFileName();
            Logger.Debug("Compiler stdout/stderr goes to {0}", stdOutFile);

            using (var command = new CliRunLib2.Command(fullCommandLine))
            {
                command.CurrentDirectory = TempDirectory_;

                command.RedirectAllTo(Path.Combine(TempDirectory_, stdOutFile));
                command.TimeLimit = 30 * 1000; // 30 s
                command.MemoryLimit = 1 * 1024 * 1024 * 1024; // 1 GB
                command.IdlenessLimit = false;
                command.ProcessLimit = null; // compilers usually spawn child processes
                command.LimitAccountingMode = CliRunLib2.LimitAccountingModeEnum.PerJob;
                if (cpuCore.HasValue)
                {
                    command.AffinityMask = 1UL << cpuCore.Value;
                }

                // Env vars
                command.IsEnvironmentOverridden = true;
                foreach (var kv in Language_.EnvVarSettings)
                {
                    command.AddEnvironmentVariable(kv.Name, formatter.Format(kv.Value));
                }

                if (Language_.NeedTempForCompiling)
                {
                    string tempDir = Path.Combine(TempDirectory_, Path.GetRandomFileName());
                    Directory.CreateDirectory(tempDir);
                    tempDir = FileSystem.Paths.LongPathTo8Dot3Path(tempDir);

                    command.AddEnvironmentVariable("TMP", tempDir);
                    command.AddEnvironmentVariable("TEMP", tempDir);
                    command.AddEnvironmentVariable("USERPROFILE", tempDir); // was needed for C# compiler csc.exe
                }

                {
                    var runResult = CliRunLib2.Runner.Run(command, token);

                    if (runResult.Outcome != CliRunLib2.OutcomeEnum.Ok)
                    {
                        // Try to avoid "IOException: ... used by another process" in rare case of compiler TLE/MLE
                        Thread.Sleep(200);
                    }
                    Log_.AppendRawLogData(File.ReadAllBytes(Path.Combine(TempDirectory_, stdOutFile)));
                    switch (runResult.Outcome)
                    {
                        case CliRunLib2.OutcomeEnum.Ok:
                            return MakeSuccess(formatter);

                        case CliRunLib2.OutcomeEnum.TimeLimitExceeded:
                        case CliRunLib2.OutcomeEnum.IdlenessLimitExceeded:
                            return MakeError(CompilerEnum.ResourceLimitsExceeded, "The compilation took too much time.");
                        case CliRunLib2.OutcomeEnum.MemoryLimitExceeded:
                            return MakeError(CompilerEnum.ResourceLimitsExceeded, "The compiler used too much memory.");

                        case CliRunLib2.OutcomeEnum.RuntimeError:
                            // Usual compilation error: the compiler exits with non-zero code
                            return MakeError(CompilerEnum.Error);

                        default:
                            var excMsg = String.Format("Compilation aborted with unexpected result:\n{0}", runResult.ToString());
                            throw new MechanismException(excMsg);
                    }
                }
            }
        }

        class Artifacts
        {
            public List<string> FileNames = new List<string>();
            public long TotalSizeInBytes = 0;

            public int FileCount
            {
                get { return FileNames.Count; }
            }
        }

        private Artifacts DiscoverCompilationArtifacts(string mask)
        {
            var result = new Artifacts();
            var sandboxInfo = new DirectoryInfo(TempDirectory_);
            foreach (var file in sandboxInfo.EnumerateFiles(mask, SearchOption.TopDirectoryOnly))
            {
                result.FileNames.Add(file.Name);
                result.TotalSizeInBytes += file.Length;
            }
            return result;
        }

        private CompiledProgram StoreNewProgramFromSandbox(Artifacts artifacts)
        {
            string dir = Path.Combine(TargetDirectory_, ARTIFACTS);
            Directory.CreateDirectory(dir);

            foreach (var name in artifacts.FileNames)
            {
                var oldPath = Path.Combine(TempDirectory_, name);
                var newPath = Path.Combine(dir, name);
                File.Move(oldPath, newPath);
                FileSystem.Security.SanitizeFileAccess(newPath);
            }

            Logger.Debug("{0} {1} moved to cache", artifacts.FileCount, artifacts.FileCount == 1 ? "file" : "files");
            return new CompiledProgram(Source_.Title, dir, Source_.LangID, artifacts.TotalSizeInBytes, artifacts.FileCount);
        }

        private IFileLocation SaveLogFile()
        {
            string logPath = Path.Combine(TargetDirectory_, COMPILATION_LOG);
            using (var file = new FileStream(logPath, FileMode.Create))
            {
                Log_.SaveToStream(file);
            }
            return new FileLocation(logPath);
        }

        private CompilationResult MakeError(CompilerEnum outcome, string msg = null)
        {
            if (msg != null)
            {
                Log_.AppendMessage(msg);
            }
            Logger.Debug("Compilation log:\n{0}", Log_.FetchAsShortDebugString());
            return new CompilationResult(outcome, Log_.FetchAsString(), SaveLogFile());
        }

        private CompilationResult MakeSuccess(StringFormatter formatter)
        {
            var dstFileMask = formatter.Format(Language_.DstFileMask);
            var artifacts = DiscoverCompilationArtifacts(dstFileMask);
            if (artifacts.FileCount == 0)
            {
                return MakeError(CompilerEnum.Error, "The compiler execution was successful, but it did not produce any output.");
            }
            var program = StoreNewProgramFromSandbox(artifacts);
            if (CheckForBrokenExe(artifacts, program, formatter))
            {
                return MakeError(CompilerEnum.Error, "Compiled file is not a valid executable. Probably, the source tried to use too large static array(s).");
            }
            return new CompilationResult(program, Log_.FetchAsString(), SaveLogFile());
        }

        private bool CheckForBrokenExe(Artifacts artifacts, CompiledProgram program, StringFormatter formatter)
        {
            if (artifacts.FileCount != 1)
            {
                return false;
            }
            string name = artifacts.FileNames.First();
            if (Path.GetExtension(name).ToLower() != ".exe")
            {
                return false;
            }

            using (var command = new CliRunLib2.Command(Path.Combine(program.BaseDirectory, name)))
            {
                command.CurrentDirectory = TempDirectory_;

                command.IsEnvironmentOverridden = true;
                foreach (var kv in Language_.EnvVarSettings)
                {
                    command.AddEnvironmentVariable(kv.Name, formatter.Format(kv.Value));
                }

                Logger.Debug("Checking CanRun()...");
                if (CliRunLib2.Runner.CanRun(command))
                {
                    return false;
                }
            }
            Logger.Error("The executable {0} is considered invalid", name);
            return true;
        }

        private string GetCommonLibsDirectory()
        {
            if (!String.IsNullOrEmpty(Language_.TestLibsSubdirectory))
            {
                return Path.Combine(CommonLibsDirectory_, Language_.TestLibsSubdirectory);
            }
            return CommonLibsDirectory_;
        }
    }
}
