﻿using System.IO;

namespace UCode.Worker
{
    class CompiledDirectory
    {
        private string Root_;
        private int Counter_;

        public CompiledDirectory(string root)
        {
            FileSystem.Utils.CompletelyRemoveDirectory(root);
            Directory.CreateDirectory(root);
            Root_ = root;
            Counter_ = 0;
        }

        public string Create()
        {
            Counter_++;
            string dir = Path.Combine(Root_, Counter_.ToString("D6"));
            Directory.CreateDirectory(dir);
            return dir;
        }
    }
}
