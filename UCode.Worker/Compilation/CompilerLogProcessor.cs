﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BinaryPair = System.Collections.Generic.KeyValuePair<byte[], byte[]>;

namespace UCode.Worker
{
    class CompilerLogProcessor
    {
        private const int BUFFER_SIZE = 1024;

        private Encoding Encoding_;
        private Dictionary<string, string> Patterns_ = new Dictionary<string, string>();
        private MemoryStream Output_ = new MemoryStream();

        public CompilerLogProcessor()
        {
            Encoding_ = Encoding.UTF8;
        }

        public CompilerLogProcessor(Encoding encoding)
        {
            Encoding_ = encoding;
        }

        public void AddPattern(string pattern, string replacement)
        {
            DoAddPattern(pattern, replacement);
        }

        public void AddPatterns(IEnumerable<string> patterns, string replacement)
        {
            foreach (string pattern in patterns)
            {
                DoAddPattern(pattern, replacement);
            }
        }

        public void AppendRawLogData(byte[] data)
        {
            WriteWithReplace(data, Output_, ConvertToBinaryPatterns());
        }

        public void AppendMessage(string msg)
        {
            using (var writer = new StreamWriter(Output_, Encoding_, BUFFER_SIZE, true))
            {
                writer.WriteLine(msg);
            }
        }

        public void SaveToStream(FileStream output)
        {
            Output_.Seek(0, SeekOrigin.Begin);
            Output_.CopyTo(output);
            Output_.Seek(0, SeekOrigin.End);
        }

        public string FetchAsString()
        {
            return FetchAsStringImpl(100);
        }

        public string FetchAsShortDebugString()
        {
            return FetchAsStringImpl(10);
        }

        private string FetchAsStringImpl(int maxLines)
        {
            const int maxLineWidth = 256;
            const string postfix = "...";
            const string cuttingLine = "<...>";

            var writer = new StringWriter();
            Output_.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(Output_, Encoding_, false, BUFFER_SIZE, true))
            {
                string line;
                int counter = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    if (counter == 0 && line.Length == 0)
                    {
                        continue;
                    }
                    if (counter < maxLines)
                    {
                        string trimmed = (line.Length <= maxLineWidth) ? line : line.Substring(0, maxLineWidth - postfix.Length) + postfix;
                        writer.WriteLine(trimmed);
                    }
                    else if (line.Length > 0)
                    {
                        writer.WriteLine(cuttingLine);
                        break;
                    }
                    counter++;
                }
            }
            Output_.Seek(0, SeekOrigin.End);
            return writer.ToString();
        }

        private void DoAddPattern(string pattern, string replacement)
        {
            Patterns_[pattern] = replacement;
            Patterns_[pattern.ToLower()] = replacement;
            pattern = pattern.Replace('\\', '/');
            Patterns_[pattern] = replacement;
            Patterns_[pattern.ToLower()] = replacement;
        }

        private static void WriteWithReplace(byte[] input, Stream output, List<BinaryPair> patterns)
        {
            for (int i = 0; i < input.Length; i++)
            {
                bool anyMatched = false;

                foreach (var kv in patterns)
                {
                    var pattern = kv.Key;
                    var replacement = kv.Value;
                    if (i + pattern.Length <= input.Length)
                    {
                        bool foundMatch = true;

                        for (int j = 0; j < pattern.Length; j++)
                        {
                            if (input[i + j] != pattern[j])
                            {
                                foundMatch = false;
                                break;
                            }
                        }

                        if (foundMatch)
                        {
                            output.Write(replacement, 0, replacement.Length);
                            i += pattern.Length - 1;
                            anyMatched = true;
                        }
                    }
                    if (anyMatched)
                    {
                        break;
                    }
                }

                if (!anyMatched)
                {
                    output.WriteByte(input[i]);
                }
            }
        }

        private List<KeyValuePair<byte[], byte[]>> ConvertToBinaryPatterns()
        {
            return Patterns_.Select(pair => new BinaryPair(
                Encoding_.GetBytes(pair.Key),
                Encoding_.GetBytes(pair.Value)
            )).OrderByDescending(pair => pair.Key.Length).ToList();
        }
    }
}
