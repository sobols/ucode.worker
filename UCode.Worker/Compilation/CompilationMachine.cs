﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class CompilationMachine
    {
        private CompiledDirectory Root_;
        private ProgrammingLanguageContainer ProgLangs_;
        private ITempDirFactory TempDirFactory_;
        private ConcurrentDictionary<string, CompilationResult> Cache_;
        private HashSet<string> ExtraDirectoriesToHideInLogs_;

        public CompilationMachine(string root, ProgrammingLanguageContainer progLangs, ITempDirFactory tempDirFactory)
        {
            Root_ = new CompiledDirectory(root);
            ProgLangs_ = progLangs;
            TempDirFactory_ = tempDirFactory;
            Cache_ = new ConcurrentDictionary<string, CompilationResult>();
            ExtraDirectoriesToHideInLogs_ = new HashSet<string>();
        }

        // Ex. "C:\Woker\bin" -> "$(HOME)"
        public void AddDirectoryToHide(string path)
        {
            ExtraDirectoriesToHideInLogs_.Add(path);
        }

        public CompilationResult Compile(SourceCode src, ProgramType type, int? cpuCore, CancellationToken token)
        {
            CompilationResult result = null;

            string srcHash = src.GetSrcHash();
            Logger.Debug("Looking up compiled program \"{0}\"", srcHash);

            if (!Cache_.TryGetValue(srcHash, out result))
            {
                var sw = new Stopwatch();
                sw.Start();
                result = DoCompile(src, cpuCore, token);
                sw.Stop();
                Logger.Debug(String.Format("Compilation of \"{0}\" took {1} ms, build {2} bytes of code",
                    srcHash, sw.Elapsed.Milliseconds, result.Program != null ? result.Program.SizeInBytes : 0));

                Cache_[srcHash] = result;
            }
            else
            {
                Logger.Info("Program is taken from cache");
            }

            return result;
        }

        private CompilationResult DoCompile(SourceCode src, int? cpuCore, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();

            if (!ProgLangs_.Has(src.LangID))
            {
                return new CompilationResult(CompilationResult.OutcomeEnum.UnknownProgrammingLanguage);
            }
            var language = ProgLangs_.Get(src.LangID);
            var log = CreateLog(language);

            using (TempDir sandbox = TempDirFactory_.MakeTempDir())
            {
                var targetPath = Root_.Create();
                return new CompilerInvocation(src, language, log, ProgLangs_.CommonLibsDirectory, sandbox.Path, targetPath).Run(cpuCore, token);
            }
        }

        private CompilerLogProcessor CreateLog(ProgrammingLanguage language)
        {
            var log = new CompilerLogProcessor(language.CompilerLogEncoding);
            log.AddPattern(language.HomeDirectory, "$(HOME)");
            log.AddPatterns(ExtraDirectoriesToHideInLogs_, "$(PRIVATE)");
            return log;
        }
    }
}
