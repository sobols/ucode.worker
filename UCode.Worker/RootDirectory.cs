﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class RootDirectory
    {
        private string Path_;
        private SandboxFactory SandboxFactory_;
        private FileLocationBase EmptyFile_;
        private SingleFileStorageFactory SingleFileStorageFactory_;

        private const string DATA = "data";
        private const string SANDBOX = "sandbox";
        private const string COMPILED = "compiled";
        private const string EMPTY = "empty";

        public RootDirectory(string path, string sandboxPath)
        {
            Path_ = path;

            FileSystem.Utils.CompletelyRemoveDirectory(path);
            FileSystem.LowIntegrity.CreateDirectoryNotWritable(path);

            Directory.CreateDirectory(Path.Combine(Path_, COMPILED));

            if (String.IsNullOrEmpty(sandboxPath))
            {
                Directory.CreateDirectory(Path.Combine(Path_, SANDBOX));
                SandboxFactory_ = new SandboxFactory(Path.Combine(Path_, SANDBOX));
            }
            else
            {
                FileSystem.Utils.CompletelyRemoveDirectory(sandboxPath);
                FileSystem.LowIntegrity.CreateDirectoryNotWritable(sandboxPath);
                SandboxFactory_ = new SandboxFactory(sandboxPath);
            }

            // put immutable empty file
            File.WriteAllBytes(Path.Combine(Path_, EMPTY), new byte[]{});
            EmptyFile_ = new FileLocation(Path.Combine(Path_, EMPTY));

            Directory.CreateDirectory(Path.Combine(Path_, DATA));
            SingleFileStorageFactory_ = new SingleFileStorageFactory(Path.Combine(Path_, DATA));
        }

        public string CompiledDirectoryPath
        {
            get { return Path.Combine(Path_, COMPILED); }
        }

        public IFileLocation EmptyFile
        {
            get { return EmptyFile_; }
        }

        public ITempDirFactory Sandbox
        {
            get { return SandboxFactory_; }
        }

        public ISingleFileStorageFactory Data
        {
            get { return SingleFileStorageFactory_; }
        }

        private class SandboxFactory: ITempDirFactory
        {
            private string SandboxPath_;

            public SandboxFactory(string sandboxPath)
            {
                SandboxPath_ = sandboxPath;
            }

            public TempDir MakeTempDir()
            {
                string randomPart = System.IO.Path.GetRandomFileName();
                string tempDir = System.IO.Path.Combine(SandboxPath_, randomPart);
                FileSystem.LowIntegrity.CreateDirectoryWritable(tempDir);
                return new TempDir(tempDir);
            }
        }

        private class SingleFileStorageFactory: ISingleFileStorageFactory
        {
            private string DataPath_;

            public SingleFileStorageFactory(string dataPath)
            {
                DataPath_ = dataPath;
            }

            public ISingleFileStorage Create()
            {
                return Create(null);
            }

            public ISingleFileStorage Create(string nameHint)
            {
                string randomPart = System.IO.Path.GetRandomFileName();
                if (nameHint != null)
                {
                    if (!FileSystem.Paths.IsCorrectFileName(nameHint))
                    {
                        throw new MechanismException(String.Format("'{0}' is not a good file name", nameHint));
                    }
                    randomPart = String.Concat(nameHint, ".", randomPart);
                }

                string tempDir = System.IO.Path.Combine(DataPath_, randomPart);
                Directory.CreateDirectory(tempDir);
                return new RandomFileNameStorage(tempDir);
            }
        }
    }
}
