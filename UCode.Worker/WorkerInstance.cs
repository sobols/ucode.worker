﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UCode.Worker
{
    abstract class WorkerInstance
    {
        protected RootDirectory RootDir_;
        protected ProgrammingLanguageContainer ProgLangs_;
        protected ISleeper Sleeper_;
        protected TestingMachineFactory TestingMachineFactory_;

        private CheckerFactory CheckerFactory_;
        private CompilationMachine CompilationMachine_;
        private ExecutionMachine ExecutionMachine_;
        private SimpleWebServer.WebServer WebServer_;
        private CancellationTokenSource TokenSource_;
        private Thread MainThread_;

        public WorkerInstance(Configuration config)
        {
            if (config.HttpServerPort != 0)
            {
                string url = String.Format("http://localhost:{0}/", config.HttpServerPort);
                WebServer_ = new SimpleWebServer.WebServer(this.ProcessHttpRequest, url);
                Logger.Info("Running Web server at {0}", url);
                WebServer_.Run();
            }

            Logger.Info(Logger.Separator);
            ProgLangs_ = ReadLanguageConfigs(config.LanguagesDirectory, config.TestLibsDirectory, config.GlobalLanguageParams);

            RootDir_ = new RootDirectory(config.RuntimeDirectory, config.SandboxDirectory);

            CompilationMachine_ = new CompilationMachine(RootDir_.CompiledDirectoryPath, ProgLangs_, RootDir_.Sandbox);
            CompilationMachine_.AddDirectoryToHide(config.TestLibsDirectory);

            ExecutionMachine_ = new ExecutionMachine(ProgLangs_, RootDir_.Sandbox, config.TimeLimitFactor, config.RobustTimeLimits);

            CompiledProgram defaultChecker = new CompiledProgram("std_checker", config.TestLibsDirectory, null, 0, 1);
            CheckerFactory_ = new CheckerFactory(defaultChecker, RootDir_.EmptyFile, ExecutionMachine_);

            Sleeper_ = new FakeSleeper();
            TestingMachineFactory_ = new Worker.TestingMachineFactory(CompilationMachine_, ExecutionMachine_, CheckerFactory_, config.UploadOutputs);
 
            TokenSource_ = new CancellationTokenSource();
            MainThread_ = new Thread(() => this.Run(TokenSource_.Token));
            MainThread_.IsBackground = false;
        }

        public virtual void Run(CancellationToken token)
        {
            try
            {
                DoRun(token);
            }
            catch (OperationCanceledException)
            {
                Logger.Info("Operation cancelled, terminating the worker...");
            }
            catch (AggregateException ex)
            {
                ex.Handle(inner =>
                {
                    Logger.Error("Unhandled AggregateException: {0}", inner.ToString());
                    return false;
                });
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception: {0}\n{1}", ex.GetType().Name, ex.ToString());
                throw;
            }
        }

        public void Start()
        {
            MainThread_.Start();
        }

        public void Join()
        {
            TokenSource_.Cancel();
            
            if (WebServer_ != null)
            {
                WebServer_.Stop();
            }

            if (MainThread_.ThreadState != ThreadState.Unstarted)
            {
                MainThread_.Join();
            }
        }

        protected abstract void DoRun(CancellationToken token);

        private static ProgrammingLanguageContainer ReadLanguageConfigs(string[] languagesDirectory, string commonDirectory, List<Param> globalLanguageParams)
        {
            const string defaultLangFileName = "lang2.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(ProgrammingLanguageGroup));
            var progLangs = new ProgrammingLanguageContainer(commonDirectory);

            foreach (var home in languagesDirectory)
            {
                foreach (var dir in Directory.EnumerateDirectories(home))
                {
                    string langPath = Path.Combine(dir, defaultLangFileName);
                    if (File.Exists(langPath))
                    {
                        using (var input = new StreamReader(langPath))
                        {
                            var group = (ProgrammingLanguageGroup)serializer.Deserialize(input);

                            foreach (var lang in group.Languages)
                            {
                                var langIDs = lang.LangID.Split(' ');
                                lang.LangID = null;

                                foreach (var langID in langIDs)
                                {
                                    lang.Init(dir, globalLanguageParams);
                                    if (lang.IsValid)
                                    {
                                        if (lang.Enabled)
                                        {
                                            progLangs.AddIfNotExist(langID, lang);
                                        }
                                    }
                                    else
                                    {
                                        Logger.Error("Configuration for language '{0}' is wrong", langID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Logger.Info("Available programming languages: {0}", String.Join(", ", progLangs.ListLangIDs()));
            return progLangs;
        }

        public virtual string DoProcessHttpRequest(HttpListenerRequest request)
        {
            return null;
        }

        // Internal debug morda rendering
        public string ProcessHttpRequest(HttpListenerRequest request)
        {
            string result = DoProcessHttpRequest(request);
            if (result != null)
            {
                return result;
            }
            switch (request.RawUrl)
            {
                case "/cmdlines":
                    return HTML.MakeLanguagesPage(ProgLangs_);
                case "/log":
                    return HTML.MakeLogPage();
                default:
                    return HTML.MakeIndexPage();
            }
        }
    }
}
