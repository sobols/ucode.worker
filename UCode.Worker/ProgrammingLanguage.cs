﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UCode.Worker
{
    // public is set for XML serializer

    public class EnvironmentVariableSetting
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlText]
        public string Value;
    }

    public class ProgrammingLanguage
    {
        [XmlAttribute("id")]
        public string LangID;

        [XmlAttribute("enabled")]
        public bool Enabled = true;

        // No need to set in config, will be set automatically.
        public string HomeDirectory; // no slash at the end

        public string TestLibsSubdirectory;

        public Encoding CompilerLogEncoding = Encoding.GetEncoding(1251);
        public bool NeedTempForCompiling = true;
        //public bool ShortTempPath = true; 
        public bool MeasureWorkingSet = false;

        public string SrcFile = "{title}";
        public string DstFileMask = "{title}.exe";

        public string CompileCmd;
        public string RunCmd = "{location}\\{title}.exe";

        // if set, this will be appended to CompileCmd
        public string UTF8SourceOption;

        [XmlElement("EnvVar")]
        public List<EnvironmentVariableSetting> EnvVarSettings;


        public void Init(string homeDirectory, List<Param> globalLanguageParams)
        {
            HomeDirectory = homeDirectory;
            var map = new Dictionary<string, string>();
            foreach (var param in globalLanguageParams)
            {
                map[param.Name] = param.Value;
            }
            var formatter = new StringFormatter(map, false);

            CompileCmd = formatter.Format(CompileCmd);
            RunCmd = formatter.Format(RunCmd);
        }
        public bool IsValid
        {
            get
            {
                return (LangID == null && CompileCmd != null && HomeDirectory != null);
            }
        }
    }

    [XmlRoot(Namespace = "", IsNullable = false, ElementName = "Languages")]
    public class ProgrammingLanguageGroup
    {
        [XmlElement("Lang")]
        public List<ProgrammingLanguage> Languages;
    }
}
