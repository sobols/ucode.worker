﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    [Serializable]
    class MechanismException : Exception
    {
        public MechanismException(string message = "")
            : base(message)
        {
        }
        protected MechanismException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }

    enum FailReason
    {
        Unknown,
        MalformedProblemInstance,
        ProblemNotFound,
        CheckerNotFound,
        CheckerNotCompiled,
        CheckerTimeLimit,
        GeneratorNotFound,
        GeneratorNotCompiled,
        GenerationFailed,
        ValidatorNotCompiled,
        NoFileGenerated,
        SolutionNotFound,
        InvalidJobJson,
        UnknownProgrammingLanguage,
        RunLibError,
        ScorerNotCompiled,
        ScoringFailed,
        InteractorNotCompiled,
    }

    [Serializable]
    class AbortTestingException : MechanismException
    {
        public FailReason Reason
        {
            get
            {
                return (FailReason)Data["Reason"];
            }
        }

        public AbortTestingException(FailReason reason, string message = "")
            : base(message)
        {
            Data["Reason"] = reason;
        }
        protected AbortTestingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
