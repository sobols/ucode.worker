﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class ProgrammingLanguageContainer
    {
        private Dictionary<string, ProgrammingLanguage> Data;
        public string CommonLibsDirectory;

        public ProgrammingLanguageContainer(string commonLibsDirectory)
        {
            Data = new Dictionary<string, ProgrammingLanguage>();

            // Check that commonLibsDirectory iv valid
            if (commonLibsDirectory == null)
            {
                throw new ArgumentNullException();
            }
            if (!Directory.Exists(commonLibsDirectory))
            {
                throw new DirectoryNotFoundException();
            }
            CommonLibsDirectory = commonLibsDirectory;            
        }

        public void Add(string langID, ProgrammingLanguage pl)
        {
            if (Data.ContainsKey(langID))
            {
                throw new MechanismException(String.Format("programming language '{0}' is defined twice", langID));
            }
            Data[langID] = pl;
        }

        public void AddIfNotExist(string langID, ProgrammingLanguage pl)
        {
            if (Data.ContainsKey(langID))
            {
                Logger.Info("Language {0} is already defined, skipping it (home: {1})", langID, pl.HomeDirectory);
                return;
            }
            Data[langID] = pl;
        }

        public bool Has(string langID)
        {
            return Data.ContainsKey(langID);
        }

        public ProgrammingLanguage Get(string langID)
        {
            ProgrammingLanguage result = null;
            if (Data.TryGetValue(langID, out result))
            {
                return result;
            }
            else
            {
                throw new MechanismException(String.Format("unknown programming language: '{0}'", langID));
            }
        }

        public IEnumerable<string> ListLangIDs()
        {
            return Data.Keys.OrderBy((x) => x);
        }
    }
}
