﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class StringFormatter
    {
        private Dictionary<string, string> Substitutions;
        private bool NeedFullSubstitution;

        public StringFormatter(Dictionary<string, string> substitutions, bool needFullSubstitution = true)
        {
            Substitutions = substitutions;
            NeedFullSubstitution = needFullSubstitution;
        }
        public string Format(string input)
        {
            foreach (var pair in Substitutions)
            {
                input = FormatSingle(input, pair.Key, pair.Value);
            }
            if (NeedFullSubstitution && (input.Contains("{") || input.Contains("}")))
            {
                throw new MechanismException(String.Format("string was not fully formatted: {0}", input));
            }
            return input;
        }
        public void AddRule(string key, string value)
        {
            Substitutions[key] = value;
        }

        private static string FormatSingle(string input, string key, string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("Value", String.Format("value for key '{0}' in format rule is null", key));
            }
            return input.Replace("{" + key + "}", value);
        }
    }
}
