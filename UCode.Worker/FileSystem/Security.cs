﻿using ProcessPrivileges;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.FileSystem
{
    static class Security
    {
        public static void SanitizeFileAccess(string path)
        {
            Logger.Debug("SanitizeFileAccess: '{0}'", path);
            FileSecurity accessControl = File.GetAccessControl(path);
            if (RemoveAccessRules(accessControl))
            {
                using (new ProcessPrivileges.PrivilegeEnabler(Process.GetCurrentProcess(), Privilege.Security))
                {
                    Logger.Warning("File '{0}' -> SetAccessControl", path);
                    File.SetAccessControl(path, accessControl);
                }
            }
            var attrs = File.GetAttributes(path);
            if (attrs.HasFlag(FileAttributes.ReadOnly))
            {
                Logger.Warning("File '{0}' -> SetAttributes", path);
                attrs &= ~FileAttributes.ReadOnly;
                File.SetAttributes(path, attrs);
            }
        }

        private static void SanitizeDirectoryAccess(string path)
        {
            Logger.Debug("SanitizeDirectoryAccess: '{0}'", path);
            DirectorySecurity accessControl = Directory.GetAccessControl(path);
            if (RemoveAccessRules(accessControl))
            {
                using (new ProcessPrivileges.PrivilegeEnabler(Process.GetCurrentProcess(), Privilege.Security))
                {
                    Logger.Warning("Directory '{0}' -> SetAccessControl", path);
                    Directory.SetAccessControl(path, accessControl);
                }
            }
        }

        /// <summary>
        /// The same as above, but it is safe to call it when the file does not exist.
        /// It is OK that path points to non-existent file.
        /// </summary>
        /// <returns>true if file exists, else false</returns>
        public static bool SanitizeFileAccessIfExists(string path)
        {
            if (File.Exists(path))
            {
                SanitizeFileAccess(path);
                return true;
            }
            return false;
        }

        public static void SanitizeDirectoryAccessRecursive(string path)
        {
            Logger.Debug("SanitizeDirectoryAccessRecursive: '{0}'", path);
            SanitizeDirectoryAccess(path);
            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (FileInfo fi in dir.EnumerateFiles())
            {
                SanitizeFileAccess(Path.Combine(path, fi.Name));
            }
            foreach (DirectoryInfo di in dir.EnumerateDirectories())
            {
                SanitizeDirectoryAccessRecursive(Path.Combine(path, di.Name));
            }
        }

        private static bool RemoveAccessRules(FileSystemSecurity accessControl)
        {
            bool changed = false;
            var collection = accessControl.GetAccessRules(true, false, typeof(System.Security.Principal.SecurityIdentifier));
            foreach (AccessRule rule in collection)
            {
                var fsRule = rule as FileSystemAccessRule;
                if (fsRule != null)
                {
                    accessControl.RemoveAccessRule(fsRule);
                    changed = true;
                }
            }
            if (accessControl.AreAccessRulesProtected)
            {
                accessControl.SetAccessRuleProtection(false, false);
                changed = true;
            }
            return changed;
        }

        // TODO?
        private static bool ChangeOwner(FileSystemSecurity accessControl)
        {
            SecurityIdentifier currentUser = WindowsIdentity.GetCurrent().User;
            IdentityReference currentOwner = accessControl.GetOwner(typeof(System.Security.Principal.SecurityIdentifier));
            if (currentOwner != currentUser)
            {
                accessControl.SetOwner(currentUser);
                return true;
            }
            return false;
        }
    }
}
