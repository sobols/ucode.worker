﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.FileSystem
{
    static class LowIntegrity
    {
        public static void CreateDirectoryNotWritable(string path)
        {
            Directory.CreateDirectory(path);
        }

        public static void CreateDirectoryWritable(string path)
        {
            CliRunLib2.Runner.CreateDirectory(path);
        }
    }
}
