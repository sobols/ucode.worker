﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace UCode.Worker.FileSystem
{
    static class Paths
    {
        private static readonly HashSet<char> InvalidChars_;

        static Paths()
        {
            InvalidChars_ = new HashSet<char>(Path.GetInvalidFileNameChars());
            InvalidChars_.Add('*');
            InvalidChars_.Add('?');
        }

        public static string LongPathTo8Dot3Path(string path)
        {
            const int maxLength = 300;
            StringBuilder sb = new StringBuilder(maxLength);
            int n = GetShortPathName(path, sb, maxLength);
            if (n == 0)
            {
                throw new Win32Exception("GetShortPathName() failed");
            }
            else
            {
                return sb.ToString();
            }
        }

        public static bool IsCorrectFileName(string onlyName)
        {
            const int maxLength = 128; // as a reserve for directory name component
            return (onlyName.Length < maxLength) && !onlyName.Any(c => InvalidChars_.Contains(c));
        }

        public static void EnsureCorrectFileName(string onlyName)
        {
            if (!IsCorrectFileName(onlyName))
            {
                throw new MechanismException("not a good file name");
            }
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern int GetShortPathName(String pathName, StringBuilder shortName, int cbShortName);
    }
}
