﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.FileSystem
{
    static class Utils
    {
        public static void CompletelyRemoveDirectory(string path)
        {
            Logger.Debug("CompletelyRemoveDirectory: '{0}'", path);
            if (CompletelyRemoveDirectoryImpl(path, 3))
            {
                return;
            }

            // Last resort...
            Security.SanitizeDirectoryAccessRecursive(path);
            if (CompletelyRemoveDirectoryImpl(path, 1))
            {
                return;
            }
            throw new MechanismException(String.Format("CompletelyRemoveDirectory failed for '{0}'", path));
        }

        private static bool CompletelyRemoveDirectoryImpl(string path, int numRetries)
        {
            int iter = 0;
            while (true)
            {
                try
                {
                    Directory.Delete(path, true);
                    return true;
                }
                catch (DirectoryNotFoundException)
                {
                    return true; // good
                }
                catch (Exception ex)
                {
                    // IOException: The process cannot access the file ... because it is being used by another process.
                    // UnauthorizedAccessException: Access to the path ... is denied.
                    if (ex is IOException || ex is UnauthorizedAccessException)
                    {
                        Logger.Warning("CompletelyRemoveDirectory '{0}': iteration {1} failed with {2}: {3}",
                            path, iter, ex.GetType().Name, ex.Message);
                    }
                    else
                    {
                        throw;
                    }
                }
                if (++iter == numRetries)
                {
                    break;
                }
                Thread.Sleep(50);
            }
            return false;
        }
    }
}
