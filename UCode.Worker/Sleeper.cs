﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UCode.Worker
{
    interface ISleeper
    {
        void Sleep(CancellationToken token);
        void Reset();
    }

    class FakeSleeper : ISleeper
    {
        public void Sleep(CancellationToken token)
        {
        }
        public void Reset()
        {
        }
    }
    
    class ConstantSleeper : ISleeper
    {
        private TimeSpan Timeout;

        public ConstantSleeper(int seconds)
        {
            Timeout = new TimeSpan(0, 0, seconds);
        }

        public void Sleep(CancellationToken token)
        {
            Logger.Debug("Sleeping {0}...", Timeout.TotalSeconds);
            Thread.Sleep(Timeout);
        }
        public void Reset()
        {
        }
    }
}
