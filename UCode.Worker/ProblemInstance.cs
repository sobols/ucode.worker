﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCode.Worker
{
    /**
     * All the classes here are designed as immutable.
     * The only exception is TestCaseFile: is supports SetGeneratedFile() method for generated test caching.
     */
    class TestCaseFile
    {
        public IFileLocation Handle { get; private set; }
        public SourceCode Generator { get; private set; }
        public string Attributes { get; private set; }

        public bool NeedsToBeGenerated
        {
            get
            {
                return (Handle == null) && (Generator != null);
            }
        }

        public TestCaseFile(IFileLocation handle)
        {
            if (handle == null)
            {
                throw new ArgumentNullException("handle");
            }
            Handle = handle;
            Generator = null;
            Attributes = null;
        }

        public TestCaseFile(SourceCode generator, string attributes)
        {
            if (generator == null)
            {
                throw new ArgumentNullException("generator");
            }
            Handle = null;
            Generator = generator;
            Attributes = attributes;
        }

        private TestCaseFile(IFileLocation handle, SourceCode generator, string attributes)
        {
            Handle = handle;
            Generator = generator;
            Attributes = attributes;
        }

        public TestCaseFile SetGeneratedFile(IFileLocation newHandle)
        {
            if (newHandle == null)
            {
                throw new ArgumentNullException("newHandle");
            }
            return new TestCaseFile(newHandle, Generator, Attributes);
        }
    }

    class TestCase
    {
        public readonly long ID;
        public readonly TestCaseFile Input;
        public readonly TestCaseFile Answer;
        public readonly long? TimeLimit;    // in milliseconds
        public readonly long? MemoryLimit;  // in bytes
        public readonly double MaxScore;
        public readonly bool IsSample;

        public TestCase(long id, TestCaseFile input, TestCaseFile answer, long? timeLimit, long? memoryLimit, double maxScore = 1.0, bool isSample = false)
        {
            if (Double.IsInfinity(maxScore) || Double.IsNaN(maxScore) || (maxScore < 0.0))
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "bad max score");
            }

            ID = id;
            Input = input;
            Answer = answer;
            TimeLimit = timeLimit;
            MemoryLimit = memoryLimit;
            MaxScore = maxScore;
            IsSample = isSample;
        }
    }

    class ProblemInstance
    {
        public readonly string Name;
        public readonly string InputFileName;  // null means reading from stdin
        public readonly string OutputFileName; // null means writing to stdout
        public readonly CheckerKind CheckerKind;
        public readonly SourceCode CheckerSource;
        public readonly SourceCode ValidatorSource;
        public readonly SourceCode ScorerSource;
        public readonly SourceCode InteractorSource;
        public readonly ReadOnlyCollection<TestCase> Tests;
        public readonly bool RunTwice;

        private static Regex GoodFileNameRegex = new Regex(@"^[0-9a-zA-Z_\.,\-]{1,64}$");

        public static bool IsGoodFileName(string s)
        {
            return (s != null) ? (s == "" || GoodFileNameRegex.IsMatch(s)) : false;
        }

        public ProblemInstance(string name, string inputFileName, string outputFileName, CheckerKind checkerKind, SourceCode checkerSource, SourceCode validatorSource, SourceCode scorerSource, SourceCode interactorSource, List<TestCase> testCases, bool runTwice = false)
        {
            if (!IsGoodFileName(inputFileName))
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "bad input file name");
            }
            if (!IsGoodFileName(outputFileName))
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "bad output file name");
            }
            if (inputFileName != "" && outputFileName != "" && inputFileName == outputFileName)
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "input and output files must be different");
            }
            if (CheckerFactory.IsCheckerSourceRequired(checkerKind) && checkerSource == null)
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "checker source is not provided");
            }
            if (!CheckerFactory.IsCheckerSourceRequired(checkerKind) && checkerSource != null)
            {
                throw new AbortTestingException(FailReason.MalformedProblemInstance, "checker source must not be provided");
            }

            Name = name;
            InputFileName = inputFileName;
            OutputFileName = outputFileName;
            CheckerKind = checkerKind;
            CheckerSource = checkerSource;
            ValidatorSource = validatorSource;
            ScorerSource = scorerSource;
            InteractorSource = interactorSource;
            Tests = testCases.AsReadOnly();
            RunTwice = runTwice;
        }
        
        public long CalculateTestsSizeInBytes()
        {
            long result = 0;
            foreach (var test in Tests)
            {
                if (test.Input != null && test.Input.Handle != null)
                {
                    result += test.Input.Handle.Size;
                }
                if (test.Answer != null && test.Answer.Handle != null)
                {
                    result += test.Answer.Handle.Size;
                }
            }
            return result;
        }

        public double CalculateMaxScore()
        {
            if (ScorerSource != null)
            {
                return 0.0; // the scorer may set any final score
            }
            return Tests.Sum(t => t.MaxScore);
        }
    }
}
