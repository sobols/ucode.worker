﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace UCode.Worker
{
    class TestingReportFactory
    {
        private readonly long JobID;
        private readonly List<TestCaseResult> TestResults;
        private SubmissionOutcome Outcome;
        public string SolutionCompilationLog { private get; set; }
        public IFileLocation SolutionCompilationLogFile { private get; set; }
        public string CheckerCompilationLog { private get; set; }
        public IFileLocation CheckerCompilationLogFile { private get; set; }
        private FailReason FailReason;
        private string FailMessage;
        private double Score;

        public TestingReportFactory(long jobID)
        {
            JobID = jobID;
            TestResults = new List<TestCaseResult>();
            Outcome = SubmissionOutcome.Correct;
            SolutionCompilationLog = null;
            CheckerCompilationLog = null;
            FailReason = Worker.FailReason.Unknown;
            FailMessage = null;
        }

        public void AddTestResult(TestCaseResult result)
        {
            TestResults.Add(result);
            if (!result.OK && Outcome == SubmissionOutcome.Correct)
            {
                Outcome = SubmissionOutcome.Incorrect;
            }
        }

        public ReadOnlyCollection<TestCaseResult> GetResults()
        {
            return TestResults.AsReadOnly();
        }

        public void SetScore(double score)
        {
            Score = score;
        }

        public TestingReport RespondWithCompilationError(ProgramType type)
        {
            switch (type)
            {
                case ProgramType.Solution:
                    Outcome = SubmissionOutcome.SolutionCompilationError;
                    break;
                case ProgramType.Checker:
                    Outcome = SubmissionOutcome.GeneralFailure;
                    FailReason = FailReason.CheckerNotCompiled;
                    break;
                case ProgramType.Scorer:
                    Outcome = SubmissionOutcome.GeneralFailure;
                    FailReason = FailReason.ScorerNotCompiled;
                    break;
                case ProgramType.Interactor:
                    Outcome = SubmissionOutcome.GeneralFailure;
                    FailReason = FailReason.InteractorNotCompiled;
                    break;
                default:
                    throw new MechanismException();
            }
            return Respond();
        }

        public TestingReport RespondWithSuccessfulTermination()
        {
            return Respond();
        }

        public TestingReport RespondWithGeneralFailure(FailReason reason, string message = null)
        {
            Outcome = SubmissionOutcome.GeneralFailure;
            FailReason = reason;
            FailMessage = message;
            return Respond();
        }

        private TestingReport Respond()
        {
            return new TestingReport(JobID, Outcome, TestResults, Score,
                SolutionCompilationLog, SolutionCompilationLogFile,
                CheckerCompilationLog, CheckerCompilationLogFile,
                FailReason, FailMessage);
        }
    }
}
