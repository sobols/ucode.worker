﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    static class HTML
    {
        private static string MakePage(string contents)
        {
            const string pageTemplate =
            @"
			<!DOCTYPE html>
			<html>
				<head>
					<meta charset='utf-8'>
					<title>UCode.Worker debug interface</title>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<link href='http://yandex.st/bootstrap/3.0.3/css/bootstrap.min.css' rel='stylesheet'>
					<meta http-equiv='Cache-Control' content='no-cache, no-store, must-revalidate' />
					<meta http-equiv='Pragma' content='no-cache' />
					<meta http-equiv='Expires' content='0' />
				</head>
				<body>
	                <div class='container'>
						{0}
					</div>
				</body>
			</html>
			";
            return String.Format(pageTemplate, contents);
        }

        public static string MakeLanguagesPage(ProgrammingLanguageContainer progLangs)
        {
            var map = new Dictionary<ProgrammingLanguage, List<string>>();
            foreach (string languageID in progLangs.ListLangIDs())
            {
                var lang = progLangs.Get(languageID);
                if (map.ContainsKey(lang))
                {
                    map[lang].Add(languageID);
                }
                else
                {
                    map[lang] = new List<string>() { languageID };
                }
            }
            
            using (var writer = new StringWriter())
            {
                writer.WriteLine("<table class='table table-bordered table-condensed'>");
                foreach (var pair in map)
                {
                    var languageIDs = pair.Value;
                    var lang = pair.Key;
                    languageIDs.Sort();
                    writer.WriteLine(String.Format("<tr><td rowspan='2'>{0}</td><td><samp>{1}</samp></td></tr><tr><td><samp>{2}</samp></td></tr>",
                        Escape(String.Join(", ", languageIDs)), Escape(lang.CompileCmd), Escape(lang.RunCmd)));
                }
                writer.WriteLine("</table>");

                writer.Flush();
                return MakePage(writer.ToString());
            }
        }

        public static string MakeIndexPage()
        {
            using (var writer = new StringWriter())
            {
                writer.WriteLine("<h1>UCode.Worker</h1>");
                writer.WriteLine("<a href='cmdlines'>Compiling and running command lines</a><br>");
                writer.WriteLine("<a href='log'>Live log</a><br>");
                writer.WriteLine("<table class='table table-bordered table-condensed'>");
                writer.WriteLine("</table>");

                writer.Flush();
                return MakePage(writer.ToString());
            }
        }

        public static string MakeLogPage()
        {
            using (var writer = new StringWriter())
            {
                Logger.Dump(writer);
                return MakePage(String.Concat("<pre>", writer.ToString(), "</pre>"));
            }
        }

        public static string Escape(string str)
        {
            return SecurityElement.Escape(str);
        }
    }
}
