﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    // Source

    interface IDataSource
    {
        void SaveToFile(string path);
    }

    class MemoryDataSource : IDataSource
    {
        private readonly byte[] Data;

        public MemoryDataSource(string data)
        {
            Data = Encoding.UTF8.GetBytes(data);
        }
        public MemoryDataSource(byte[] data)
        {
            Data = data;
        }

        public void SaveToFile(string path)
        {
            const int bufferSize = 4096;
            using (var fs = File.Create(path, bufferSize, FileOptions.SequentialScan))
            {
                fs.Write(Data, 0, Data.Length);
            }
        }

        // empty source
        public static readonly MemoryDataSource Empty = new MemoryDataSource(new byte[] { });
    }

    class CopyOtherFileDataSource : IDataSource
    {
        private readonly string OtherPath;

        public CopyOtherFileDataSource(string otherPath)
        {
            OtherPath = otherPath;
        }

        public void SaveToFile(string path)
        {
            File.Copy(OtherPath, path);
            //FileSystem.Security.SanitizeFileAccess(path);
        }
    }

    class MoveOtherFileDataSource : IDataSource
    {
        private readonly string OtherPath;

        public MoveOtherFileDataSource(string otherPath)
        {
            OtherPath = otherPath;
        }

        public void SaveToFile(string path)
        {
            File.Move(OtherPath, path);
            //FileSystem.Security.SanitizeFileAccess(path);
        }
    }

    class StreamDataSource : IDataSource
    {
        private readonly Stream SrcStream;

        public StreamDataSource(Stream srcStream)
        {
            SrcStream = srcStream;
        }
        
        public void SaveToFile(string path)
        {
            const int bufferSize = 4096;
            using (var fs = File.Create(path, bufferSize, FileOptions.SequentialScan))
            {
                SrcStream.CopyTo(fs, bufferSize);
            }
        }
    }

    class MySqlDataSource : IDataSource
    {
        private readonly MySqlDataReader Reader;
        private readonly int Column;

        public MySqlDataSource(MySqlDataReader reader, int column)
        {
            Reader = reader;
            Column = column;
        }

        public void SaveToFile(string path)
        {
            const int bufferSize = 4096;
            byte[] buffer = new byte[bufferSize];
            long length = (long)Reader.GetBytes(Column, 0, null, 0, 0);
            long index = 0;

            using (var fs = File.Create(path, bufferSize, FileOptions.SequentialScan))
            {
                while (index < length)
                {
                    long bytesRead = Reader.GetBytes(Column, index, buffer, 0,
                                                    (int)Math.Min(buffer.Length, length - index));

                    fs.Write(buffer, 0, (int)bytesRead);
                    index += bytesRead;
                }
            }
        }
    }

    // Destination

    interface IDataDestination
    {
        void LoadFromFile(string path);
        bool OK
        {
            get;
        }
    }

    class MemoryDataDestination : IDataDestination
    {
        public MemoryStream Stream { get; private set; }

        public void LoadFromFile(string path)
        {
            var data = File.ReadAllBytes(path);
            Stream = new MemoryStream(data, false);
        }

        public bool OK
        {
            get
            {
                return Stream != null;
            }
        }
    }

    class OneFileStorageMoveDataDestination : IDataDestination
    {
        public IFileLocation Handle;
        private ISingleFileStorage Storage_;

        public OneFileStorageMoveDataDestination(ISingleFileStorage storage)
        {
            Storage_ = storage;
        }

        public void LoadFromFile(string path)
        {
            var src = new MoveOtherFileDataSource(path);
            Handle = Storage_.Put(src);
        }

        public bool OK
        {
            get
            {
                return Handle != null;
            }
        }
    }
}
