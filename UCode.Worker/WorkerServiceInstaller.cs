﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Install;
using System.ServiceProcess;
using System.ComponentModel;

namespace UCode.Worker
{
    [RunInstaller(true)]
    public class WorkerServiceInstaller : Installer
    {
        public WorkerServiceInstaller()
        {
            ServiceProcessInstaller process = new ServiceProcessInstaller();

            process.Account = ServiceAccount.LocalService;

            ServiceInstaller serviceAdmin = new ServiceInstaller();

            serviceAdmin.StartType = ServiceStartMode.Automatic;
            serviceAdmin.ServiceName = "UCodeWorkerIR2";
            serviceAdmin.DisplayName = "UCode.Worker Service (iRunner 2)";
            serviceAdmin.Description = "Testing Module";

            Installers.Add(process);
            Installers.Add(serviceAdmin);
        }
    }
}
