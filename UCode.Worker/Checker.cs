﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker
{
    enum CheckerKind
    {
        FileCompare,
        StrictFileCompare,
        IRunner,
        IRunnerStd,
        AcceptAll,
        TestlibH,
    }

    class CheckingHandles
    {
        public readonly IFileLocation Input;
        public readonly IFileLocation Output;
        public readonly IFileLocation Answer;

        public CheckingHandles(IFileLocation input, IFileLocation output, IFileLocation answer)
        {
            Input = input;
            Output = output;
            Answer = answer;
        }
    }

    class CheckerFactory
    {
        private readonly CompiledProgram DefaultChecker_;
        private readonly IFileLocation EmptyFile_;
        private readonly ExecutionMachine ExecutionMachine_;

        public CheckerFactory(CompiledProgram defaultChecker, IFileLocation emptyFile, ExecutionMachine executionMachine)
        {
            DefaultChecker_ = defaultChecker;
            EmptyFile_ = emptyFile;
            ExecutionMachine_ = executionMachine;
        }

        public static bool IsCheckerSourceRequired(CheckerKind kind)
        {
            return (kind == CheckerKind.IRunner || kind == CheckerKind.TestlibH);
        }

        public IChecker Create(CheckerKind kind, CompiledProgram checkerProgram)
        {
            IChecker checker = null;
            switch (kind)
            {
                case CheckerKind.StrictFileCompare:
                    checker = new StrictFileCompareChecker(EmptyFile_);
                    break;
                case CheckerKind.FileCompare:
                    checker = new FileCompareChecker(EmptyFile_);
                    break;
                case CheckerKind.IRunner:
                    checker = new IRunnerChecker(EmptyFile_, ExecutionMachine_, checkerProgram);
                    break;
                case CheckerKind.IRunnerStd:
                    checker = new IRunnerChecker(EmptyFile_, ExecutionMachine_, DefaultChecker_);
                    break;
                case CheckerKind.TestlibH:
                    checker = new TestLibChecker(EmptyFile_, ExecutionMachine_, checkerProgram);
                    break;
                case CheckerKind.AcceptAll:
                    checker = new AcceptAllChecker(EmptyFile_);
                    break;
                default:
                    throw new MechanismException(String.Format("Checker kind {0} is not supported by CheckerFactory", kind));
            }
            return checker;
        }
    }

    interface IChecker
    {
        CheckingResult Check(CheckingHandles handles, int? cpuCore, CancellationToken token, double maxScore = 1.0);
    }

    abstract class CheckerBase : IChecker
    {
        private readonly IFileLocation EmptyFile_;

        protected CheckerBase(IFileLocation emptyFile)
        {
            EmptyFile_ = emptyFile;
        }

        public CheckingResult Check(CheckingHandles handles, int? cpuCore, CancellationToken token, double maxScore)
        {
            CheckingResultFactory resultFactory = new CheckingResultFactory(maxScore);

            if (handles.Output == null)
            {
                return resultFactory.Make(CheckerOutcome.PresentationError, new Messages.OutputFileNotFoundMessage());
            }

            CheckingHandles allNonNullHandles = new CheckingHandles(
                handles.Input != null ? handles.Input : EmptyFile_,
                handles.Output != null ? handles.Output : EmptyFile_,
                handles.Answer != null ? handles.Answer : EmptyFile_
            );

            return CheckImpl(allNonNullHandles, cpuCore, token, resultFactory);
        }

        /// <param name="handles">All three handles are non-null.</param>
        protected abstract CheckingResult CheckImpl(CheckingHandles handles, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory);
    }

    abstract class RealFileCheckerBase : CheckerBase
    {
        /// Base class for external checkers that read real files from disk and require real filesystem paths to open.

        protected RealFileCheckerBase(IFileLocation emptyFile)
            : base(emptyFile)
        {
        }

        protected override CheckingResult CheckImpl(CheckingHandles handles, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory)
        {
            using (var inputToken = handles.Input.MakeReadable())
            {
                using (var outputToken = handles.Output.MakeReadable())
                {
                    using (var answerToken = handles.Answer.MakeReadable())
                    {
                        return DoCheckImpl(inputToken.Path, outputToken.Path, answerToken.Path, cpuCore, token, resultFactory);
                    }
                }
            }
        }

        protected abstract CheckingResult DoCheckImpl(string inputPath, string outputPath, string answerPath, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory);
    }

    class AcceptAllChecker : CheckerBase
    {
        public AcceptAllChecker(IFileLocation emptyFile)
            : base(emptyFile)
        {
        }

        protected override CheckingResult CheckImpl(CheckingHandles handles, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory)
        {
            return resultFactory.Make(CheckerOutcome.OK, Messages.EmptyMessage.Instance);
        }
    }

    class IRunnerChecker : RealFileCheckerBase
    {
        private ExecutionMachine ExecMachine;
        private CompiledProgram CheckerProgram;

        public IRunnerChecker(IFileLocation emptyFile, ExecutionMachine execMachine, CompiledProgram checkerProgram)
            : base(emptyFile)
        {
            ExecMachine = execMachine;
            CheckerProgram = checkerProgram;
        }

        protected override CheckingResult DoCheckImpl(string inputPath, string outputPath, string answerPath, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory)
        {
            IDataSource checkerIn = null;
            using (var sstream = new StringWriter())
            {
                sstream.WriteLine(1); // number of tests
                sstream.WriteLine(inputPath); // input file              
                sstream.WriteLine(1); // once again
                sstream.WriteLine(answerPath); // answer (correct) file
                sstream.WriteLine(outputPath); // solution output file
                sstream.WriteLine(1); // problem ID
                sstream.WriteLine("Problem"); // problem name
                sstream.WriteLine(1); // test ID
                sstream.WriteLine(resultFactory.MaxScore.ToString(CultureInfo.InvariantCulture)); // points as double with dot!
                sstream.WriteLine(1); // solution ID
                sstream.Flush();
                checkerIn = new MemoryDataSource(sstream.ToString());
            }

            MemoryDataDestination checkerOut = new MemoryDataDestination();
            var execSettings = new ExecutionSettings();
            execSettings.InputFiles.Add("checker.in", checkerIn);
            execSettings.OutputFiles.Add("checker.out", checkerOut);
            execSettings.CpuCore = cpuCore;
            var execCommand = new ExecutionCommand(CheckerProgram);
            execCommand.TimeLimit = 30000; // 30 seconds
            execCommand.CommandLineArgs.Add("checker.out");
            ExecutionResult execResult = ExecMachine.Run(execSettings, execCommand, token);

            if (execResult.Outcome != ExecutionResult.OutcomeEnum.OK)
            {
                ICheckerMessage message = new Messages.RawStringMessage(String.Format("checker: {0}", execResult));
                switch (execResult.Outcome)
                {
                    case ExecutionResult.OutcomeEnum.RuntimeError:
                        return resultFactory.Make(CheckFailedReason.UnexpectedExitCode, message);
                    case ExecutionResult.OutcomeEnum.TimeLimitExceeded:
                        return resultFactory.Make(CheckFailedReason.CheckerTimeLimitExceeded, message);
                    default:
                        return resultFactory.Make(CheckFailedReason.Unknown, message);
                }
            }

            if (!checkerOut.OK)
            {
                return resultFactory.Make(CheckFailedReason.NoCheckerOutFileFound);
            }
            return ParseCheckerOut(checkerOut.Stream, resultFactory);
        }

        private CheckingResult ParseCheckerOut(MemoryStream stream, CheckingResultFactory resultFactory)
        {
            using (var reader = new StreamReader(stream))
            {
                string firstLine = reader.ReadLine();
                if (firstLine == null)
                {
                    return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutLineNotFoundMessage(1));
                }
                else if (firstLine == "1")
                {
                    string secondLine = reader.ReadLine();
                    if (secondLine == null)
                    {
                        return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutLineNotFoundMessage(2));
                    }
                    secondLine = secondLine.Trim();
                    double score = 0;
                    if (!Double.TryParse(secondLine, NumberStyles.Float, CultureInfo.InvariantCulture, out score))
                    {
                        return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutWrongDoubleNumber(secondLine));
                    }

                    string thirdLine = reader.ReadLine();
                    return resultFactory.Make(score, new Messages.RawStringMessage(thirdLine));
                }
                else if (firstLine == "0")
                {
                    string secondLine = reader.ReadLine();
                    if (secondLine == null)
                    {
                        return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutLineNotFoundMessage(2));
                    }
                    secondLine = secondLine.Trim();
                    int errorCode = 0;
                    if (!Int32.TryParse(secondLine, out errorCode))
                    {
                        return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutWrongIntegerNumber(secondLine));
                    }

                    string thirdLine = StringUtils.Escape(reader.ReadLine());
                    var message = new Messages.RawStringMessage(thirdLine);
                    switch (errorCode)
                    {
                        case 1:
                            return resultFactory.Make(CheckerOutcome.WrongAnswer, message);
                        case 2:
                            return resultFactory.Make(CheckerOutcome.PresentationError, message);
                        case 3:
                            return resultFactory.Make(CheckFailedReason.ReportedByChecker, message);
                        default:
                            return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutUnknownErrorCode(errorCode));
                    }
                }
                else
                {
                    return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutWrongFirstLineMessage(firstLine));
                }
            }
        }
    }

    class TestLibChecker : RealFileCheckerBase
    {
        private ExecutionMachine ExecMachine;
        private CompiledProgram CheckerProgram;

        public TestLibChecker(IFileLocation emptyFile, ExecutionMachine execMachine, CompiledProgram checkerProgram)
            : base(emptyFile)
        {
            ExecMachine = execMachine;
            CheckerProgram = checkerProgram;
        }

        protected override CheckingResult DoCheckImpl(string inputPath, string outputPath, string answerPath, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory)
        {
            MemoryDataDestination report = new MemoryDataDestination();
            string reportFileName = "report.txt";

            var execCommand = new ExecutionCommand(CheckerProgram);
            execCommand.TimeLimit = 30 * 1000; // 30 s
            execCommand.MemoryLimit = 1 * 1024 * 1024 * 1024; // 1 GB
            execCommand.CommandLineArgs.Add(inputPath);
            execCommand.CommandLineArgs.Add(outputPath);
            execCommand.CommandLineArgs.Add(answerPath);
            execCommand.CommandLineArgs.Add(reportFileName);
            var execSettings = new ExecutionSettings();
            execSettings.OutputFiles.Add(reportFileName, report);
            execSettings.CpuCore = cpuCore;

            ExecutionResult execResult = ExecMachine.Run(execSettings, execCommand, token);

            string reportString = (report.OK ? StringUtils.BlobToStringSafe(report.Stream.ToArray()) : "");
            ICheckerMessage message = new Messages.RawStringMessage(reportString);

            if (execResult.Outcome == ExecutionResult.OutcomeEnum.OK || execResult.Outcome == ExecutionResult.OutcomeEnum.RuntimeError)
            {
                switch (execResult.ExitCode)
                {
                    case 0:
                        return resultFactory.Make(CheckerOutcome.OK, message);
                    case 1:
                        return resultFactory.Make(CheckerOutcome.WrongAnswer, message);
                    case 2:
                        return resultFactory.Make(CheckerOutcome.PresentationError, message);
                    case 3:
                    case 4:
                        return resultFactory.Make(CheckFailedReason.ReportedByChecker, message);
                    case 7:
                        var parseResult = StringUtils.TryExtractPoints(reportString);
                        if (parseResult == null)
                        {
                            return resultFactory.Make(CheckFailedReason.CheckerOutHasWrongFormat, message);
                        }
                        return resultFactory.Make(parseResult.Points, new Messages.RawStringMessage(parseResult.Message));
                }
            }

            // It is unlikely to get here. Checker either was terminated or died.
            // No sense to look at the message printed by checker, we build our own.
            message = new Messages.RawStringMessage(String.Format("checker: {0}", execResult));

            switch (execResult.Outcome)
            {
                case ExecutionResult.OutcomeEnum.RuntimeError:
                    return resultFactory.Make(CheckFailedReason.UnexpectedExitCode, message);
                case ExecutionResult.OutcomeEnum.TimeLimitExceeded:
                    return resultFactory.Make(CheckFailedReason.CheckerTimeLimitExceeded, message);
            }
            return resultFactory.Make(CheckFailedReason.Unknown, message);
        }
    }

    abstract class InternalCheckerBase : CheckerBase
    {
        protected Stream OutputStream;
        protected Stream AnswerStream;

        public InternalCheckerBase(IFileLocation emptyFile)
            : base(emptyFile)
        {
        }

        public CheckingResult Check(string output, string answer)
        {
            var factory = new CheckingResultFactory(1.0);
            using (OutputStream = new MemoryStream(Encoding.UTF8.GetBytes(output)))
            {
                using (AnswerStream = new MemoryStream(Encoding.UTF8.GetBytes(answer)))
                {
                    return CompareStreams(factory);
                }
            }
        }

        protected abstract CheckingResult CompareStreams(CheckingResultFactory resultFactory);

        protected override CheckingResult CheckImpl(CheckingHandles handles, int? cpuCore, CancellationToken token, CheckingResultFactory resultFactory)
        {
            using (OutputStream = handles.Output.OpenRead())
            {
                using (AnswerStream = handles.Answer.OpenRead())
                {
                    return CompareStreams(resultFactory);
                }
            }
        }
    }


    /// <summary>
    /// Simple bitwise comparison of output and answer
    /// Ported from Glados.
    /// </summary>
    class StrictFileCompareChecker : InternalCheckerBase
    {
        public StrictFileCompareChecker(IFileLocation emptyFile)
            : base(emptyFile)
        {
        }

        protected override CheckingResult CompareStreams(CheckingResultFactory resultFactory)
        {
            // Check the file sizes. If they are not the same, the files 
            // are not the same.
            if (OutputStream.Length != AnswerStream.Length)
            {
                return resultFactory.Make(CheckerOutcome.WrongAnswer, new Messages.LengthDiffer(OutputStream.Length, AnswerStream.Length));
            }

            // Read and compare a byte from each file until either a
            // non-matching set of bytes is found or until the end of
            // file is reached.
            long length = OutputStream.Length;
            long pos = 0;
            while (pos < length)
            {
                // Read one byte from each file.
                int outputByte = OutputStream.ReadByte();
                int answerByte = AnswerStream.ReadByte();

                if (outputByte != answerByte)
                {
                    return resultFactory.Make(CheckerOutcome.WrongAnswer, new Messages.FilesDifferAtPos(pos + 1));
                }
                ++pos;
            }
            return resultFactory.Make(CheckerOutcome.OK, new Messages.OKMessage());
        }
    }

    class FileCompareChecker : InternalCheckerBase
    {
        public FileCompareChecker(IFileLocation emptyFile)
            : base(emptyFile)
        {
        }
        
        private static string Trim(string s)
        {
            const int trimSize = 40;
            return (s.Length <= trimSize) ? s : s.Substring(0, trimSize - 3) + "...";
        }
        private static string SafeSubstr(string s, int beg, int end)
        {
            // returns substring s[beg, ..., end - 1]
            if (beg >= end || beg >= s.Length || end <= 0)
            {
                return String.Empty;
            }
            beg = Math.Max(beg, 0);
            end = Math.Min(end, s.Length);

            return (beg > 0 ? "..." : "") + s.Substring(beg, end - beg) + (end < s.Length ? "..." : "");
        }
        private static string ShowRegion(string s, int pos)
        {
            const int leftBound = 30;
            const int rightBound = 10;
            // pos should be in [0, s.Length]
            return SafeSubstr(s, pos - leftBound, pos + rightBound);
        }

        private static Tuple<int, string, string> Difference(string s, string t)
        {
            // s and t must be non-equal
            int pos = -1;
            for (int i = 0; i < s.Length && i < t.Length; ++i)
            {
                if (s[i] != t[i])
                {
                    pos = i;
                    break;
                }
            }
            if (pos == -1)
            {
                pos = (s.Length > t.Length) ? t.Length : s.Length;
            }
            return new Tuple<int, string, string>(pos, ShowRegion(s, pos), ShowRegion(t, pos));
        }

        protected override CheckingResult CompareStreams(CheckingResultFactory resultFactory)
        {
            // Read and compare files line by line
            var encoding = Encoding.GetEncoding(1251);


            using (var outputReader = new StreamReader(OutputStream, encoding))
            {
                using (var answerReader = new StreamReader(AnswerStream, encoding))
                {
                    for (int realLines = 1; ; ++realLines)
                    {
                        string outputLine = outputReader.ReadLine();
                        string answerLine = answerReader.ReadLine();

                        if (outputLine != null && answerLine != null)
                        {
                            if (outputLine != answerLine)
                            {
                                return resultFactory.Make(CheckerOutcome.WrongAnswer, new Messages.FilesDifferAtLineMessage(realLines));
                            }
                        }
                        else if (outputLine == null && answerLine == null)
                        {
                            break;
                        }
                        else if (outputLine == null)
                        {
                            if (answerLine != "" || answerReader.ReadLine() != null)
                            {
                                return resultFactory.Make(CheckerOutcome.WrongAnswer, new Messages.FilesDifferAtLineMessage(realLines));
                            }
                            break;
                        }
                        else
                        {
                            if (outputLine != "" || outputReader.ReadLine() != null)
                            {
                                return resultFactory.Make(CheckerOutcome.WrongAnswer, new Messages.FilesDifferAtLineMessage(realLines));
                            }
                            break;
                        }
                    }
                }
            }
            return resultFactory.Make(CheckerOutcome.OK, new Messages.OKMessage());
        }
    }

}
