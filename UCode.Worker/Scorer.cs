﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace UCode.Worker
{
    class ScoringResult
    {
        public readonly bool OK;
        public readonly double Score;
        public readonly CheckFailedReason Reason;
        public readonly ICheckerMessage Message;

        private ScoringResult(bool ok, double score, CheckFailedReason reason, ICheckerMessage message)
        {
            OK = ok;
            Score = score;
            Reason = reason;
            Message = message;
        }

        public static ScoringResult Success(double score)
        {
            return new ScoringResult(true, score, CheckFailedReason.Unknown, Messages.EmptyMessage.Instance);
        }
        public static ScoringResult Fail(CheckFailedReason reason, ICheckerMessage message)
        {
            return new ScoringResult(false, 0.0, reason, message);
        }
        public override string ToString()
        {
            if (OK)
            {
                return DoubleIO.ToString(Score);
            }
            else
            {
                string message = Message.AsString(NaturalLanguage.English);
                if (String.IsNullOrEmpty(message))
                {
                    return String.Format("{0}", Reason);
                }
                else
                {
                    return String.Format("{0}: {1}", Reason, message);
                }
            }
        }
    }

    interface IScorer
    {
        ScoringResult Evaluate(ReadOnlyCollection<TestCaseResult> results, CancellationToken token);
    }

    class SumUpScorer : IScorer
    {
        public ScoringResult Evaluate(ReadOnlyCollection<TestCaseResult> results, CancellationToken token)
        {
            double score = results.Sum(result => result.Score);
            return ScoringResult.Success(score);
        }
    }

    class TestLibScorer : IScorer
    {
        private readonly ExecutionMachine ExecutionMachine_;
        private readonly CompiledProgram ScorerProgram_;

        private const long SCORER_TIME_LIMIT = 30 * 1000; // 30 seconds

        public TestLibScorer(ExecutionMachine executionMachine, CompiledProgram scorerProgram)
        {
            ExecutionMachine_ = executionMachine;
            ScorerProgram_ = scorerProgram;
        }

        private static bool NeedEscapeChar(char c)
        {
            return c == ';' || c == '\n' || c == '\\';
        }

        public static string Escape(string value)
        {
            if (!value.Any(NeedEscapeChar))
            {
                return value; // most common fast path
            }
            StringBuilder sb = new StringBuilder(value.Length);
            foreach (char nextChar in value)
            {
                if (NeedEscapeChar(nextChar))
                {
                    sb.Append('\\');
                    sb.Append(nextChar == '\n' ? 'n' : nextChar);
                }
                else
                {
                    sb.Append(nextChar);
                }
            }
            return sb.ToString();
        }

        // See `deserializeTestResultVerdict()` in testlib.h
        public static string Serialize(TestCaseOutcome outcome)
        {
            switch (outcome)
            {
                case TestCaseOutcome.OK:
                    return "OK";
                case TestCaseOutcome.WrongAnswer:
                case TestCaseOutcome.PresentationError:
                    return "WRONG_ANSWER";
                case TestCaseOutcome.RuntimeError:
                    return "RUNTIME_ERROR";
                case TestCaseOutcome.TimeLimitExceeded:
                    return "TIME_LIMIT_EXCEEDED";
                case TestCaseOutcome.IdlenessLimitExceeded:
                    return "IDLENESS_LIMIT_EXCEEDED";
                case TestCaseOutcome.MemoryLimitExceeded:
                    return "MEMORY_LIMIT_EXCEEDED";
                case TestCaseOutcome.CheckFailed:
                    return "FAILED";
            }
            return "CRASHED"; // Unknown?
        }

        public static string Serialize(int testNo, TestCaseResult result)
        {
            var tokens = new string[]
            {
                /*testIndex=*/testNo.ToString(),
                /*testset=*/String.Empty,
                /*group=*/String.Empty,
                /*verdict=*/Serialize(result.Outcome),
                /*points=*/DoubleIO.ToString(result.Score),
                /*timeConsumed=*/result.TimeUsed.ToString(),
                /*memoryConsumed=*/result.MemoryUsed.ToString(),
                /*input=*/String.Empty, // TODO
                /*output=*/String.Empty, // TODO
                /*answer=*/String.Empty, // TODO
                /*exitCode=*/result.ExitCode.ToString(),
                /*checkerComment=*/result.CheckerMessage.AsString(NaturalLanguage.English),
            };
            if (tokens.Length != 12)
            {
                throw new MechanismException("Internal error in TestCaseResult serialization to testlib format");
            }
            return String.Join(";", tokens);
        }

        public static string Serialize(ReadOnlyCollection<TestCaseResult> results)
        {
            using (var sstream = new StringWriter())
            {
                int testNo = 0;
                foreach (TestCaseResult result in results)
                {
                    sstream.WriteLine(Serialize(++testNo, result));
                }
                sstream.Flush();
                return sstream.ToString();
            }
        }

        public ScoringResult Evaluate(ReadOnlyCollection<TestCaseResult> results, CancellationToken token)
        {
            MemoryDataSource scorerIn = new MemoryDataSource(Serialize(results));
            MemoryDataDestination scorerOut = new MemoryDataDestination();
            
            var execSettings = new ExecutionSettings();
            var execCommand = new ExecutionCommand(ScorerProgram_);
            execCommand.TimeLimit = SCORER_TIME_LIMIT;
            execCommand.StdIn = scorerIn;
            execCommand.StdOut = scorerOut;
            ExecutionResult execResult = ExecutionMachine_.Run(execSettings, execCommand, token);
            var message = new Messages.RawStringMessage(String.Format("scorer: {0}", execResult));

            switch (execResult.Outcome)
            {
                case ExecutionResult.OutcomeEnum.OK:
                    return ParseScorerOutput(scorerOut.Stream);
                case ExecutionResult.OutcomeEnum.RuntimeError:
                    return ScoringResult.Fail(CheckFailedReason.UnexpectedExitCode, message);
                case ExecutionResult.OutcomeEnum.TimeLimitExceeded:
                    return ScoringResult.Fail(CheckFailedReason.CheckerTimeLimitExceeded, message);
                default:
                    return ScoringResult.Fail(CheckFailedReason.Unknown, message);
            }
        }

        private static ScoringResult ParseScorerOutput(Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                var body = reader.ReadToEnd().Trim();
                double score = 0.0;
                if (DoubleIO.TryFromString(body, out score))
                {
                    return ScoringResult.Success(score);
                }
                else
                {
                    return ScoringResult.Fail(CheckFailedReason.CheckerOutHasWrongFormat, new Messages.CheckerOutWrongDoubleNumber(body));
                }
            }
        }
    }
}
