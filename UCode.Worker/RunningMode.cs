﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    public enum RunningMode
    {
        SelfTesting,
        UCode,
        IRunner,
        Api,
    }
}
