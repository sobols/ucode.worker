﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCode.Worker.IRunnerMode
{
    static class TestCaseOutcomeExtensions
    {
        public static SolutionState.Error ToIRunnerError(this TestCaseOutcome outcome)
        {
            switch (outcome)
            {
                case TestCaseOutcome.OK:
                    return SolutionState.Error.NoError;

                case TestCaseOutcome.WrongAnswer:
                    return SolutionState.Error.WrongAnswer;

                case TestCaseOutcome.PresentationError:
                    return SolutionState.Error.PresentationError;

                case TestCaseOutcome.CheckFailed:
                    return SolutionState.Error.InternalError;

                case TestCaseOutcome.IdlenessLimitExceeded:
                case TestCaseOutcome.TimeLimitExceeded:
                    return SolutionState.Error.TimeOut;

                case TestCaseOutcome.MemoryLimitExceeded:
                    return SolutionState.Error.MemoryOut;

                case TestCaseOutcome.SecurityViolation:
                case TestCaseOutcome.RuntimeError:
                    return SolutionState.Error.RuntimeError;

                default:
                    throw new MechanismException(String.Format("unsupported test case outcome: {0}", outcome));
            }
        }

        public static SolutionState.Error ToIRunnerError(this TestingReport report)
        {
            switch (report.Outcome)
            {
                case SubmissionOutcome.Correct:
                    return SolutionState.Error.NoError;

                case SubmissionOutcome.Incorrect:
                    return report.ResultOfFirstFailedTest.Outcome.ToIRunnerError();

                case SubmissionOutcome.SolutionCompilationError:
                    return SolutionState.Error.CompilationError;

                case SubmissionOutcome.GeneralFailure:
                    return SolutionState.Error.InternalError;

                default:
                    throw new MechanismException(String.Format("unsupported submission outcome: {0}", report.Outcome));
            }
        }
    }
}
