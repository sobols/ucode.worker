﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.IRunnerMode
{
    class ProblemCache
    {
        private class Item
        {
            public readonly ProblemInstance Instance;
            public readonly ISingleFileStorage FileStorage;
            public readonly long Version;

            public Item(ProblemInstance instance, ISingleFileStorage fileStorage, long version)
            {
                Instance = instance;
                FileStorage = fileStorage;
                Version = version;
            }
        }

        private Dictionary<int, Item> Data_;

        private Item GetItem(int id)
        {
            Item result = null;
            if (!Data_.TryGetValue(id, out result))
            {
                throw new MechanismException(String.Format("problem {0} is not present in cache", id));
            }
            return result;
        }

        public ProblemCache()
        {
            Data_ = new Dictionary<int, Item>();
        }

        public ProblemInstance Get(int id)
        {
            return GetItem(id).Instance;
        }

        public bool HasProblem(int id)
        {
            return Data_.ContainsKey(id);
        }

        public long GetVersion(int id)
        {
            return GetItem(id).Version;
        }

        public void Update(int id, long version, ProblemInstance instance, ISingleFileStorage fileStorage)
        {
            if (Data_.ContainsKey(id))
            {
                Item oldItem = Data_[id];
                if (oldItem.FileStorage != null)
                {
                    oldItem.FileStorage.Dispose();
                }
            }
            Data_[id] = new Item(instance, fileStorage, version);
        }
    }
}
