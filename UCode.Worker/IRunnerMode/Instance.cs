﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.IRunnerMode
{
    class Instance : WorkerInstance
    {
        protected IRunnerDatabase IRunnerDB;
        protected ProblemCache Problems;
        protected Dictionary<int, string> LangMap;
        protected IPublicFileStorage PublicFileStorage;
        protected IRunnerFileStorage FileStorage;

        public Instance(Configuration config)
            : base(config)
        {
            Sleeper_ = new ConstantSleeper(5);
            IRunnerDB = new IRunnerDatabase(config.IRunnerModeConfig.DBConnectionString);
            Problems = new ProblemCache();

            if (config.PublicFileStorageSettings != null)
            {
                PublicFileStorage = MakePublicFileStorage(config.PublicFileStorageSettings);
            }

            LangMap = SetupIRunnerLangMap();
            //UpdateLangCmdLinesInDB();
            if (config.IRunnerModeConfig.MongoConnectionString != null)
            {
                FileStorage = new IRunnerFileStorage(config.IRunnerModeConfig.MongoConnectionString);
            }

            Logger.Info("Waiting for solutions...");
        }

        private static IPublicFileStorage MakePublicFileStorage(PublicFileStorageSettings settings)
        {
            if (settings.Directory != null && settings.URL == null)
            {
                return new LocalPublicFileStorage(settings.Directory);
            }
            else if (settings.Directory == null && settings.URL != null)
            {
                return new RemotePublicFileStorage(new Uri(settings.URL));
            }
            return null;
        }

        protected override void DoRun(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                long jobID;
                if (!GetNewJobID(out jobID, token))
                {
                    Sleeper_.Sleep(token);
                    continue;
                }

                using (CrossProcessLockFactory.CreateCrossProcessLock())
                {
                    // we have a jobID!
                    Sleeper_.Reset();

                    using (ISingleFileStorage fs = RootDir_.Data.Create(jobID.ToString()))
                    {
                        TestingMachine machine = TestingMachineFactory_.Create(fs);

                        machine.TestingCases += machine_TestingCases;

                        TestingReport report = machine.FetchAndTest(jobID, new JobFetcher(FetchJob), 0, token).Report;
                        PublishReport(report);
                    }
                }
            }
        }

        private void machine_TestingCases(object sender, TestingCasesEventArgs e)
        {
            DBOperationRetryer.Action action = () =>
            {
                using (var session = IRunnerDB.CreateSession())
                {
                    int solutionID = Convert.ToInt32(e.Job.ID);
                    session.SetSolutionState(solutionID, new SolutionState(SolutionState.Status.UnderTesting));
                    session.Commit();
                }
            };
            DBOperationRetryer.Run(action);
        }

        private void PublishReport(TestingReport report)
        {
            int solutionID = Convert.ToInt32(report.JobID);
            SolutionState st = Conversion.GenerateSolutionState(report);

            List<int> testResultIDs = new List<int>();

            DBOperationRetryer.Action action = () =>
                {
                    using (var session = IRunnerDB.CreateSession())
                    {
                        session.SetSolutionState(solutionID, st);

                        foreach (var testResult in report.TestCaseResults)
                        {
                            int resultID = IRunnerDB.AllocateTestResultID();
                            session.AddTestResult(resultID, solutionID, new IRunnerTestCaseResult(
                                Convert.ToInt32(testResult.TestID),
                                testResult.Outcome.ToIRunnerError(),
                                testResult.TimeUsed,
                                testResult.MemoryUsed,
                                testResult.ExitCode,
                                testResult.OK,
                                testResult.Score,
                                testResult.CheckerMessage.AsString(NaturalLanguage.Russian)));
                            testResultIDs.Add(resultID);
                        }
                        if (report.SolutionCompilationLog != null)
                        {
                            var gzippedCompilationLog = Util.Compress(report.SolutionCompilationLog);
                            session.AddCompilationLog(solutionID, gzippedCompilationLog);
                        }

                        if (FileStorage != null)
                        {
                            FileStorage.Publish(report, testResultIDs, session);
                        }

                        session.Commit();
                    }
                };

            DBOperationRetryer.Run(action);

            //PublishOutputFiles(report);
        }

        protected void PublishOutputFiles(TestingReport report)
        {
            if (PublicFileStorage != null)
            {
                foreach (var test in report.TestCaseResults)
                {
                    if (test.OutputHandle != null)
                    {
                        PublicFileStorage.Put(test.OutputHandle, EDataOrigin.OutputFile);
                    }
                    if (test.StdOutHandle != null)
                    {
                        PublicFileStorage.Put(test.StdOutHandle, EDataOrigin.StdOut);
                    }
                    if (test.StdErrHandle != null)
                    {
                        PublicFileStorage.Put(test.StdErrHandle, EDataOrigin.StdErr);
                    }
                }
            }
        }

        private Dictionary<int, string> SetupIRunnerLangMap()
        {
            Dictionary<int, string> langMap = new Dictionary<int, string>();
            const int logPadding = 16;
            Logger.Info("Setting up languages to test");

            var testingSystemLangs = IRunnerDB.ListAvailableProgrammingLanguages();
            foreach (var lang in testingSystemLangs)
            {
                if (ProgLangs_.Has(lang.ID))
                {
                    langMap.Add(lang.InternalID, lang.ID);
                    Logger.InfoPositive("+", lang.ID.PadRight(logPadding), lang.Name);
                }
                else
                {
                    Logger.InfoNegative("-", lang.ID.PadRight(logPadding), lang.Name);
                }
            }
            if (langMap.Count == 0)
            {
                throw new MechanismException("No languages to test");
            }
            return langMap;
        }

        // This should be rather fast and lightweight method that returns only
        // job (or submission) identifier. This should not download tests,
        // compile something, etc. It is helpful to get id as soon as possible
        // to be able to respond with error on any future step.
        // Method should throw only OperationCanceledException.
        private bool GetNewJobID(out long jobID, CancellationToken token)
        {
            int? solutionID = null;
            try
            {
                solutionID = IRunnerDB.GetNewSolutionID(LangMap.Keys);
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Logger.Error("MySQL error while getting new solution ID: {0}", ex.Message);
                Logger.Debug("Full exception:\n{0}", ex);
            }
            if (solutionID.HasValue)
            {
                jobID = solutionID.Value;
                return true;
            }
            else
            {
                jobID = 0;
                //Logger.Info("Nothing to test");
                return false;
            }
        }

        protected TestingJob FetchJob(long jobID)
        {
            int solutionID = Convert.ToInt32(jobID);

            int problemID = 0;
            long currentVersion = 0;
            ProblemInstance newProblem = null;
            ISingleFileStorage newFileStorage = null;
            bool stopAfterFirstFailedTest = false;
            SourceCode solution = null;

            DBOperationRetryer.Action action = () =>
            {
                using (var session = IRunnerDB.CreateSession())
                {
                    problemID = session.GetProblemForSolution(solutionID);
                    currentVersion = session.GetProblemVersion(problemID);

                    bool needUpdate = false;
                    // checking whether we need to fetch new problem instance: tests, checker, etc.
                    if (!Problems.HasProblem(problemID))
                    {
                        Logger.Info("New problem {0} (ver. {1})", problemID, currentVersion);
                        needUpdate = true;
                    }
                    else
                    {
                        long presentVersion = Problems.GetVersion(problemID);
                        if (presentVersion < currentVersion)
                        {
                            Logger.Info("Updating problem {0} (from ver. {1} to ver. {2})", problemID, presentVersion, currentVersion);
                            needUpdate = true;
                        }
                    }
                    if (!needUpdate)
                    {
                        Logger.Info("Taking problem {0} (ver. {1}) from cache", problemID, currentVersion);
                    }

                    if (needUpdate)
                    {
                        string slug = String.Format("p{0}.v{1}", problemID, currentVersion);
                        newFileStorage = RootDir_.Data.Create(slug);
                        newProblem = session.FetchProblem(problemID, newFileStorage, LangMap);
                        long totalSize = newProblem.CalculateTestsSizeInBytes();
                        Logger.Info("Fetched {0} of tests from DB", Util.GetHumanReadableFileSize(totalSize));
                    }

                    solution = session.FetchSolution(solutionID, LangMap);
                    stopAfterFirstFailedTest = session.IsSolutionFromACMContest(solutionID);
                }
            };

            DBOperationRetryer.Run(action);

            if (newProblem != null)
            {
                Problems.Update(problemID, currentVersion, newProblem, newFileStorage);
            }
            var problem = Problems.Get(problemID);

            return new TestingJob(solutionID, problem, solution, stopAfterFirstFailedTest);
        }

        private void UpdateLangCmdLinesInDB()
        {
            using (var session = IRunnerDB.CreateSession())
            {
                foreach (var pair in LangMap)
                {
                    var lang = ProgLangs_.Get(pair.Value);
                    session.UpdateLanguageCmdLime(pair.Key, lang.CompileCmd, lang.RunCmd);
                }
                session.Commit();
            }
        }
    }
}
