﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.IRunnerMode
{
    class IRunnerFileStorage
    {
        private MongoServer Server;
        private const long FileSizeLimit = 100 * 1024 * 1024;

        public IRunnerFileStorage(string connString)
        {
            Server = MongoServer.Create(connString);
        }

        public void Publish(TestingReport report, List<int> testResultIDs, IRunnerDatabase.IRunnerSession session)
        {
            var ctx = new SubmissionContext(session, Server);
            for (int i = 0; i < testResultIDs.Count; ++i)
            {
                ctx.AddTest(testResultIDs[i], report.TestCaseResults[i]);
            }
            
            var tsk = ctx.PublishToMongo();
            tsk.Wait();
        }

        class SubmissionContext
        {
            private class Item
            {
                public byte[] Key;
                public IFileLocation Handle;

                public Item(byte[] key, IFileLocation handle)
                {
                    Key = key;
                    Handle = handle;
                }
            }
            
            private Dictionary<string, Item> ToUpload;
            private IRunnerDatabase.IRunnerSession MySQLSession;
            private MongoServer MongoServer;

            public SubmissionContext(IRunnerDatabase.IRunnerSession mySQLSession, MongoServer mongoServer)
            {
                ToUpload = new Dictionary<string, Item>();
                MySQLSession = mySQLSession;
                MongoServer = mongoServer;
            }

            public void AddTest(int id, TestCaseResult r)
            {
                byte[] inputFile = PushFile(r.InputHandle);
                byte[] outputFile = PushFile(r.OutputHandle);
                byte[] answerFile = PushFile(r.AnswerHandle);
                byte[] stdout = PushFile(r.StdOutHandle);
                byte[] stderr = PushFile(r.StdErrHandle);

                MySQLSession.AddTestOutput(id, inputFile, outputFile, answerFile, stdout, stderr);
            }

            public async Task PublishToMongo()
            {
                Logger.Info("{0} files to upload, finding new...", ToUpload.Count);

                var keysToUpload = new List<BsonValue>();
                foreach (var item in ToUpload.Values)
                {
                    keysToUpload.Add(new BsonBinaryData(item.Key));
                }

                var database = MongoServer.GetDatabase("irunner");

                var filter = Query.In("_id", keysToUpload);
                var presentFiles = database.GridFS.Find(filter);

                foreach (var file in presentFiles)
                {
                    string strKey = BitConverter.ToString(file.Id.AsByteArray);
                    ToUpload.Remove(strKey);
                }

                Logger.Info("{0} new files not present in Mongo", ToUpload.Count);

                foreach (var item in ToUpload.Values)
                {
                    using (Stream stream = item.Handle.OpenRead())
                    {
                        var createOptions = new MongoGridFSCreateOptions { Id = item.Key };
                        database.GridFS.Upload(stream, null, createOptions);
                    }
                }

                Logger.Info("Uploading complete");

                ToUpload.Clear();
            }

            private byte[] PushFile(IFileLocation handle)
            {
                if (handle == null)
                {
                    return null;
                }

                byte[] key = null;

                long length = handle.Size;

                if (length < FileSizeLimit)
                {
                    if (length < 20)
                    {
                        using (FileStream stream = handle.OpenRead())
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                stream.CopyTo(ms);
                                key = ms.ToArray();
                            }
                        }
                    }
                    else
                    {
                        using (FileStream stream = handle.OpenRead())
                        {
                            key = Util.ComputeBinarySHA1(stream);
                        }
                        EnqueueForUploading(key, handle);
                    }
                }
                return key;
            }

            private void EnqueueForUploading(byte[] key, IFileLocation handle)
            {
                String strKey = BitConverter.ToString(key);
                if (!ToUpload.ContainsKey(strKey))
                {
                    ToUpload[strKey] = new Item(key, handle);
                }
            }
        }
    }
}
