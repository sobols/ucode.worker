﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UCode.Worker.IRunnerMode
{
    [XmlType("IRunnerMode.CustomConfiguration")]
    public class CustomConfiguration
    {
        public string DBConnectionString;
        public string MongoConnectionString;
    }
}
