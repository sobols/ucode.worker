﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.IRunnerMode
{
    struct SolutionState
    {
        // Status
        public enum Status : uint
        {
            Init = 0,
            UnderCompiling = 1,
            UnderTesting = 2,
            Done = 3,
        }
        public enum Error : uint
        {
            NoError = 0,
            WrongAnswer = 1,
            PresentationError = 2,
            InternalError = 3,
            CompilationError = 4,
            TimeOut = 5,
            RuntimeError = 6,
            MemoryOut = 7,
            //SecurityError = 8,
            //ErrorNotAvailable = 9,
        }
        
        private uint State;

        public SolutionState(uint state)
        {
            this.State = state;
        }
        
        public SolutionState(Status status, Error error = Error.NoError, bool isAccepted = false, uint number = 0)
        {
            this.State = 0;
            SetStatus(status);
            SetError(error);
            SetAcceptInfo(isAccepted);
            SetNumber(number);
        }

        public uint GetFullState()
        {
            return this.State;
        }
        public void SetFullState(uint state)
        {
            this.State = state;
        }
        
        public bool IsAccepted()
        {
            return (this.State & 0x8U) != 0L;
        }
        public void SetAcceptInfo(bool accepted)
        {
            SetFullState((this.State & 0xFFFF7U) | (accepted ? (1U << 3) : 0U));
        }

        Error GetError()
        {
            return (Error)((this.State >> 4) & 0x7U);
        }
        public void SetError(Error value)
        {
            SetFullState((this.State & 0x7FFF8FU) | ((uint)value << 4));
        }

        uint GetNumber()
        {
            return (this.State >> 7) & 0xFFFFU;
        }
        public void SetNumber(uint paramInt)
        {
            if (!(0 <= paramInt && paramInt < (1 << 16)))
            {
                throw new MechanismException(String.Format("setting number to {0}", paramInt));
            }
            uint uNumber = (uint)paramInt;
            SetFullState((this.State & 0x7FU) | (uNumber << 7));
        }

        Status GetStatus()
        {
            return (Status)(State & 0x7U);
        }
        public void SetStatus(Status value)
        {
            SetFullState((this.State & 0x7FFF8U) | ((uint)value));
        }

        public override string ToString()
        {
            string accepted = IsAccepted() ? "accepted" : "rejected";
            return String.Format("{{{0}, {1}, {2}, code {3}}}", GetStatus(), accepted, GetError(), GetNumber());
        }
    }

    struct IRunnerTestCaseResult
    {
        public readonly int TestID;
        public readonly SolutionState.Error Error;
        public readonly long ExecutionTime;
        public readonly long MemoryUsed;
        public readonly int ExitCode;
        public readonly bool Accepted;
        public readonly double Points;
        public readonly string CheckerStr;

        public IRunnerTestCaseResult(int testID, SolutionState.Error error, long executionTime, long memoryUsed, int exitCode, bool accepted, double points, string checkerStr)
        {
            TestID = testID;
            Error = error;
            ExecutionTime = executionTime;
            MemoryUsed = memoryUsed;
            ExitCode = exitCode;
            Accepted = accepted;
            Points = points;
            CheckerStr = checkerStr;
        }

        public override string ToString()
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "test {0,-7}: {1,-16} [Accepted: {2}] (time {3,6}, memory {4,6:0.0}, code {5}, pts {6}): \"{7}\"", TestID, Error, (Accepted ? "Y" : "N"), ExecutionTime, Util.BytesToMegabytes(MemoryUsed), ExitCode, Points, CheckerStr);
        }
    }
}
