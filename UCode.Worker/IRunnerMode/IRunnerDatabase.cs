﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.IRunnerMode
{
    class IRunnerDatabase
    {
        private string ConnectionString;
        private IRunnerSequence TestResultSequence = null;

        public class IRunnerLanguage : Tuple<int, string, string>
        {
            public IRunnerLanguage(int internalID, string id, string name) : base(internalID, id, name) { }
            public int InternalID { get { return this.Item1; } }
            public string ID { get { return this.Item2; } }
            public string Name { get { return this.Item3; } }
        }
        private struct IRunnerMemoryFile
        {
            public int ID;
            public string Name;
            public byte[] Data;
        }
        private struct IRunnerDiskFile
        {
            public string Name;
            public IFileLocation Handle;

            public bool Valid
            {
                get
                {
                    return (Handle != null);
                }
            }
        }

        public IRunnerDatabase(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public IRunnerSession CreateSession()
        {
            return new IRunnerSession(ConnectionString);
        }

        public int? GetNewSolutionID(IEnumerable<int> langIDFilter)
        {
            var filter = String.Join(", ", langIDFilter);
            var selectQuery = String.Format("SELECT solutionID, receivedTime FROM irunner_solution WHERE status = 0 AND languageID in ({0}) LIMIT 1", filter);

            int? funcResult = null;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                MySqlHelper.ExecuteNonQuery(conn, "LOCK TABLES irunner_solution WRITE");
                int solutionID = 0;
                object receivedTime = null;
                using (var reader = MySqlHelper.ExecuteReader(conn, selectQuery))
                {
                    if (reader.Read())
                    {
                        solutionID = reader.GetInt32(0);
                        receivedTime = reader.GetValue(1);
                    }
                }

                if (solutionID != 0)
                {
                    var state = new SolutionState(SolutionState.Status.UnderCompiling);

                    var ps = new List<MySqlParameter>();
                    ps.Add(new MySqlParameter("solutionID", solutionID));
                    ps.Add(new MySqlParameter("status", state.GetFullState()));
                    ps.Add(new MySqlParameter("startTestingTime", Util.CurrentTimeMillis()));
                    ps.Add(new MySqlParameter("receivedTime", receivedTime));

                    MySqlHelper.ExecuteNonQuery(conn, "UPDATE irunner_solution SET status = @status, startTestingTime = @startTestingTime, receivedTime = @receivedTime WHERE solutionID = @solutionID", ps.ToArray());
                    funcResult = solutionID;
                }
                MySqlHelper.ExecuteNonQuery(conn, "UNLOCK TABLES");
            }
            return funcResult;
        }

        public int? GetSolutionID(IEnumerable<int> langIDFilter, int min, int max)
        {
            var filter = String.Join(", ", langIDFilter);
            var selectQuery = String.Format("SELECT solutionID FROM irunner_solution WHERE solutionID >= {0} AND solutionID <= {1} AND status <> 0 AND languageID IN ({2}) LIMIT 1", min, max, filter);

            int? funcResult = null;

            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                int solutionID = 0;
                using (var reader = MySqlHelper.ExecuteReader(conn, selectQuery))
                {
                    if (reader.Read())
                    {
                        solutionID = reader.GetInt32(0);
                    }
                }

                if (solutionID != 0)
                {
                   funcResult = solutionID;
                }
            }
            return funcResult;
        }

        public bool CheckConnection()
        {
            bool ok = false;
            try
            {
                using (var conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (var cmd = new MySqlCommand("SELECT 1", conn))
                    {
                        var result = cmd.ExecuteScalar().ToString();
                        conn.Close();
                        ok = (result == "1");
                    }
                    // conn will be closed automatically
                }
            }
            catch (MySqlException ex)
            {
                Logger.Error("MySQL connection error {0}: {1}", ex.Number, ex.Message);
            }
            return ok;
        }

        public int AllocateTestResultID()
        {
            if (TestResultSequence == null)
            {
                TestResultSequence = new IRunnerSequence(ConnectionString, "irunner_testresult", "testResultID");
            }
            return TestResultSequence.AllocateOne();
        }

        public List<IRunnerLanguage> ListAvailableProgrammingLanguages()
        {
            var result = new List<IRunnerLanguage>();
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Open();
                using (var reader = MySqlHelper.ExecuteReader(conn, "SELECT languageID, title, description FROM irunner_language"))
                {
                    while (reader.Read())
                    {
                        result.Add(new IRunnerLanguage(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
                    }
                }
            }
            return result;
        }

        private static IRunnerMemoryFile FetchMemoryFile(int id, MySqlConnection conn, MySqlTransaction trans = null)
        {
            using (var cmd = new MySqlCommand("SELECT fileID, name, data FROM katrin_file WHERE fileID = @id", conn, trans))
            {
                cmd.Parameters.AddWithValue("id", id);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        IRunnerMemoryFile file;
                        file.ID = reader.GetInt32(0);
                        file.Name = reader.GetString(1);
                        file.Data = (byte[])reader.GetValue(2);
                        return file;
                    }
                    else
                    {
                        return new IRunnerMemoryFile();
                    }
                }
            }
        }
        
        private static IRunnerDiskFile FetchDiskFile(int id, ISingleFileStorage storage, MySqlConnection conn, MySqlTransaction trans = null)
        {
            using (var cmd = new MySqlCommand("SELECT name, data FROM katrin_file WHERE fileID = @id", conn, trans))
            {
                cmd.Parameters.AddWithValue("id", id);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        IRunnerDiskFile file;
                        file.Name = reader.GetString(0);
                        if (reader.IsDBNull(1))
                        {
                            throw new MechanismException(String.Format("null data in katrin_file for ID {0}", id));
                        }
                        var src = new MySqlDataSource(reader, 1);
                        try
                        {
                            file.Handle = storage.Put(src);
                        }
                        catch (Exception)
                        {
                            Logger.Error("Error while getting file with fileID = {0} from database", id);
                            throw;
                        }
                        return file;
                    }
                    else
                    {
                        return new IRunnerDiskFile();
                    }
                }
            }
        }
        
        private class ProblemFetcher
        {
            private int ID;
            private MySqlConnection Conn;
            private MySqlTransaction Trans;
            private ISingleFileStorage Storage;
            private Dictionary<int, string> LangMap;

            private Dictionary<int, SourceCode> Generators;

            private long? GlobalMemoryLimit;
            int InputMetafileID = 0;
            int OutputMetafileID = 0;
            
            // for future ProblemInstance initialization
            string ProblemName;
            string InputFileName;
            string OutputFileName;
            private SourceCode CheckerSource;
            List<TestCase> TestCases;

            public ProblemFetcher(int problemID, MySqlConnection conn, MySqlTransaction trans, ISingleFileStorage storage, Dictionary<int, string> langMap)
            {
                ID = problemID;
                Conn = conn;
                Trans = trans;

                Storage = storage;
                LangMap = langMap;

                Generators = new Dictionary<int, SourceCode>();
                TestCases = new List<TestCase>();
            }
            public ProblemInstance DoFetch()
            {
                FetchNameAndML();
                FetchInputOutputMetafileIDs();
                FetchCheckerAndGenerators();
                FetchTestCases();
                var problem = MakeProblem();
                return problem;
            }

            ProblemInstance MakeProblem()
            {
                return new ProblemInstance(
                    ProblemName,
                    InputFileName,
                    OutputFileName,
                    (CheckerSource == null ? CheckerKind.FileCompare : CheckerKind.IRunner),
                    CheckerSource,
                    null,
                    null,
                    null,
                    TestCases);
            }

            private void FetchNameAndML()
            {
                using (var cmd = MakeCmd("SELECT name, memoryLimit FROM irunner_task WHERE taskID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            ProblemName = reader.GetString(0);
                            if (!reader.IsDBNull(1) && reader.GetString(1).Length > 0)
                            {
                                GlobalMemoryLimit = ParseMemoryLimit(reader.GetString(1));
                            }
                        }
                        else
                        {
                            throw new AbortTestingException(FailReason.ProblemNotFound);
                        }
                    }
                }
            }

            private void FetchInputOutputMetafileIDs()
            {
                using (var cmd = MakeCmd("SELECT metafileID, userFileName, fileTypeID FROM irunner_metafile WHERE taskID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int metafileID = reader.GetInt32(0);
                            string name = reader.GetString(1);
                            int typeID = reader.GetInt32(2);
                            switch (typeID)
                            {
                                case 10032:
                                    InputFileName = name;
                                    InputMetafileID = metafileID;
                                    break;
                                case 10033:
                                    OutputFileName = name;
                                    OutputMetafileID = metafileID;
                                    break;
                                default:
                                    throw new MechanismException(String.Format("unknown metafile type: {0}", typeID));
                            }
                        }
                    }
                }
            }

            private void FetchCheckerAndGenerators()
            {
                var dt = new DataTable();
                // from resources.xml:
                // 216 - checker
                // 218 - generator
                using (var cmd = MakeCmd("SELECT taskfileID, fileID, languageID, filetypeID FROM irunner_taskfile WHERE taskID = @id AND (filetypeID = 216 OR filetypeID = 218)"))
                {
                    cmd.Parameters.AddWithValue("id", ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
                foreach (DataRow row in dt.Rows)
                {
                    bool isChecker = (row["filetypeID"].ToString() == "216");

                    int internalLangID = Int32.Parse(row["languageID"].ToString());

                    string langID = "";
                    LangMap.TryGetValue(internalLangID, out langID);

                    int fileID = Int32.Parse(row["fileID"].ToString());
                    var file = IRunnerDatabase.FetchMemoryFile(fileID, Conn, Trans);
                    if (file.Data == null)
                    {
                        throw new AbortTestingException(isChecker ? FailReason.CheckerNotFound : FailReason.GeneratorNotFound);
                    }

                    var src = new SourceCode(langID, file.Data, file.Name);

                    if (isChecker)
                    {
                        if (CheckerSource != null)
                        {
                            // use the first checker
                            // throw new MechanismException("More than one checker for problem");
                        }
                        else
                        {
                            CheckerSource = src;
                        }
                    }
                    else
                    {
                        var generatorID = Int32.Parse(row["taskfileID"].ToString());
                        Generators[generatorID] = src;
                    }
                }
            }

            private void FetchTestCases()
            {
                var pointsForTest = new Dictionary<int, double>();
                var timeLimitForTest = new Dictionary<int, long>();
                var testIDs = new List<int>(); // in order which MySQL returns...

                using (var cmd = MakeCmd("SELECT testID, points, time FROM irunner_test WHERE taskID = @id ORDER BY testID"))
                {
                    cmd.Parameters.AddWithValue("id", ID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int testID = reader.GetInt32(0);
                            testIDs.Add(testID);
                            pointsForTest.Add(testID, reader.GetDouble(1));
                            timeLimitForTest.Add(testID, reader.GetInt32(2) * 1000);
                        }
                    }
                }

                var inputForTest = new Dictionary<int, TestCaseFile>();
                var answerForTest = new Dictionary<int, TestCaseFile>();

                foreach (int testID in testIDs)
                {
                    var dt = new DataTable();
                    using (var cmd = MakeCmd("SELECT metafileID, fileID, generatorID, attributes FROM irunner_test_file WHERE testID = @id"))
                    {
                        cmd.Parameters.AddWithValue("id", testID);
                        using (var reader = cmd.ExecuteReader())
                        {
                            dt.Load(reader);
                        }
                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        TestCaseFile file = null;

                        int metafileID = Int32.Parse(row["metafileID"].ToString());

                        if (row["fileID"] != DBNull.Value)
                        {
                            // Test file is stored directly
                            int fileID = Int32.Parse(row["fileID"].ToString());
                            var irFile = IRunnerDatabase.FetchDiskFile(fileID, Storage, Conn, Trans);
                            if (!irFile.Valid)
                            {
                                throw new AbortTestingException(FailReason.MalformedProblemInstance, "test file not found");
                            }
                            file = new TestCaseFile(irFile.Handle);
                        }
                        else if (row["generatorID"] != DBNull.Value)
                        {
                            // Test file is generated
                            int generatorID = Int32.Parse(row["generatorID"].ToString());
                            SourceCode generator = null;
                            if (!Generators.TryGetValue(generatorID, out generator))
                            {
                                throw new AbortTestingException(FailReason.GeneratorNotFound);
                            }

                            string attributes = null;
                            if (row["attributes"] != null)
                            {
                                attributes = row["attributes"].ToString();
                            }
                            file = new TestCaseFile(generator, attributes);
                        }
                        else
                        {
                            throw new MechanismException("neither file nor generator is given");
                        }

                        Debug.Assert(file != null);

                        if (metafileID == InputMetafileID)
                        {
                            inputForTest.Add(testID, file); // throws if already exists
                        }
                        else if (metafileID == OutputMetafileID)
                        {
                            answerForTest.Add(testID, file);
                        }
                    }
                }

                foreach (int testID in testIDs)
                {
                    TestCaseFile input;
                    inputForTest.TryGetValue(testID, out input);
                    
                    TestCaseFile answer;
                    answerForTest.TryGetValue(testID, out answer);

                    long? timeLimit = null;
                    if (timeLimitForTest.ContainsKey(testID))
                    {
                        timeLimit = timeLimitForTest[testID];
                    }
                    double points = 1.0;
                    pointsForTest.TryGetValue(testID, out points);
                    var tc = new TestCase(testID, input, answer, timeLimit, GlobalMemoryLimit, points);
                    TestCases.Add(tc);
                }
            }

            private static long ParseMemoryLimit(string str)
            {
                if ((str.EndsWith("m")) || (str.EndsWith("M")))
                {
                    return Int64.Parse(str.Substring(0, str.Length - 1)) * (1L << 20);
                }
                if ((str.EndsWith("k")) || (str.EndsWith("K")))
                {
                    return Int64.Parse(str.Substring(0, str.Length - 1)) * (1L << 10);
                }
                return Int64.Parse(str);
            }

            private MySqlCommand MakeCmd(string text)
            {
                return new MySqlCommand(text, Conn, Trans);
            }
        }

        public class IRunnerSession : IDisposable
        {
            private MySqlConnection Conn;
            private MySqlTransaction Trans;

            public IRunnerSession(string connectionString)
            {
                Conn = null;
                Trans = null;
                try
                {
                    Conn = new MySqlConnection(connectionString);
                    Conn.Open();

                    try
                    {
                        Trans = Conn.BeginTransaction();
                    }
                    catch (Exception)
                    {
                        if (Trans != null)
                        {
                            Trans.Dispose();
                        }
                        throw;
                    }
                }
                catch (Exception)
                {
                    if (Conn != null)
                    {
                        Conn.Dispose();
                    }
                    throw;
                }
            }
            public void Commit()
            {
                Trans.Commit();
            }
            public void Rollback()
            {
                Trans.Rollback();
            }

            public void Dispose()
            {
                if (Trans != null)
                {
                    Trans.Dispose();
                }
                if (Conn != null)
                {
                    Conn.Dispose();
                }
            }

            private MySqlCommand MakeCmd(string commandText)
            {
                return new MySqlCommand(commandText, Conn, Trans);
            }

            public ProblemInstance FetchProblem(int id, ISingleFileStorage storage, Dictionary<int, string> langMap)
            {
                var pf = new ProblemFetcher(id, Conn, Trans, storage, langMap);
                return pf.DoFetch();
            }
            public SourceCode FetchSolution(int id, Dictionary<int, string> langMap)
            {
                using (var cmd = MakeCmd("SELECT fileID, languageID FROM irunner_solution WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    int fileID = 0;
                    string langID = null;

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            throw new AbortTestingException(FailReason.SolutionNotFound);
                        }
                        fileID = reader.GetInt32(0);
                        var internalLangID = reader.GetInt32(1);
                        langID = langMap[internalLangID];
                    }

                    var file = IRunnerDatabase.FetchMemoryFile(fileID, Conn, Trans);
                    if (file.Data == null)
                    {
                        throw new AbortTestingException(FailReason.SolutionNotFound);
                    }
                    
                    return new SourceCode(langID, file.Data, file.Name);
                }
            }

            public bool IsSolutionFromACMContest(int id)
            {
                const int acmContestType = 1023;
                using (var cmd = MakeCmd("SELECT contestTypeID from irunner_contest JOIN irunner_solution ON irunner_contest.contestID = irunner_solution.contestID WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    var value = cmd.ExecuteScalar();
                    return (value != null && Int32.Parse(value.ToString()) == acmContestType);
                }
            }
            public int GetProblemForSolution(int id)
            {
                using (var cmd = MakeCmd("SELECT taskID FROM irunner_solution WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    var obj = cmd.ExecuteScalar();
                    return Int32.Parse(obj.ToString());
                }
            }
            public long GetProblemVersion(int id)
            {
                using (var cmd = MakeCmd("SELECT entityVersion FROM katrin_version WHERE doctypeID = 400 AND entityID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.Read())
                        {
                            return -1L;
                        }
                        long version = reader.GetInt64(0);
                        if (reader.Read())
                        {
                            throw new MechanismException("more than one version for single problem");
                        }
                        return version;
                    }
                }
            }

            public SolutionState GetSolutionState(int id)
            {
                using (var cmd = MakeCmd("SELECT status FROM irunner_solution WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    var result = cmd.ExecuteScalar();
                    if (result == null)
                    {
                        throw new MechanismException("solution not found");
                    }
                    return new SolutionState((UInt32)result);
                }
            }

            public void SetSolutionState(int id, SolutionState state)
            {
                object lastReceivedTime = null;
                using (var cmd = MakeCmd("SELECT receivedTime FROM irunner_solution WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("id", id);
                    var result = cmd.ExecuteScalar();
                    if (result == null)
                    {
                        throw new MechanismException("not found");
                    }
                    lastReceivedTime = result;
                }
                using (var cmd = MakeCmd("UPDATE irunner_solution SET status = @status, receivedTime = @receivedTime WHERE solutionID = @id"))
                {
                    cmd.Parameters.AddWithValue("status", state.GetFullState());
                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("receivedTime", lastReceivedTime);
                    var result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        throw new MechanismException("Updating state failed");
                    }
                }
            }

            public void AddTestResult(int id, int solutionID, IRunnerTestCaseResult res)
            {
                using (var cmd = MakeCmd("INSERT INTO irunner_testresult VALUES (@testResultID, @errorType, @testID, @solutionID, @timeExecution, @memoryUsed, @exitCode, @accepted, @points, @checkerStr)"))
                {
                    cmd.Parameters.AddWithValue("testResultID", id);
                    cmd.Parameters.AddWithValue("errorType", (uint)res.Error);
                    cmd.Parameters.AddWithValue("testID", res.TestID);
                    cmd.Parameters.AddWithValue("solutionID", solutionID);
                    cmd.Parameters.AddWithValue("timeExecution", res.ExecutionTime);
                    cmd.Parameters.AddWithValue("memoryUsed", res.MemoryUsed);
                    cmd.Parameters.AddWithValue("exitCode", res.ExitCode);
                    cmd.Parameters.AddWithValue("accepted", res.Accepted ? 1 : 0);
                    cmd.Parameters.AddWithValue("points", res.Points);
                    cmd.Parameters.AddWithValue("checkerStr", res.CheckerStr);
                    var result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        throw new MechanismException("Inserting result failed");
                    }
                }
            }

            public List<IRunnerTestCaseResult> FetchResultsForSolution(int solutionID)
            {
                var list = new List<IRunnerTestCaseResult>();
                using (var cmd = MakeCmd("SELECT errorType, testID, timeExecution, memoryUsed, exitCode, accepted, points, checkerStr FROM irunner_testresult WHERE solutionID = @solutionID"))
                {
                    cmd.Parameters.AddWithValue("solutionID", solutionID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SolutionState.Error error = (SolutionState.Error)reader.GetUInt32("errorType");
                            int testID = reader.GetInt32("testID");
                            long executionTime = reader.GetInt64("timeExecution");
                            long memoryUsed = reader.GetInt64("memoryUsed");
                            int exitCode = reader.GetInt32("exitCode");
                            bool accepted = reader.GetBoolean("accepted");
                            double points = reader.GetDouble("points");
                            
                            int msg = reader.GetOrdinal("checkerStr");
                            string checkerStr = reader.IsDBNull(msg) ? null : reader.GetString(msg);

                            list.Add(new IRunnerTestCaseResult(testID, error, executionTime, memoryUsed, exitCode, accepted, points, checkerStr));
                        }
                    }
                }
                return list;
            }

            public void AddCompilationLog(int solutionID, byte[] gzippedLog)
            {
                /*using (var cmd = MakeCmd("SHOW TABLES LIKE \"irunner_compilationlog\""))
                {
                    var result = cmd.ExecuteScalar();
                    if (result == null)
                    {
                        return;
                    }
                }*/
                using (var cmd = MakeCmd("REPLACE INTO irunner_compilationlog VALUES (@solutionID, @gzippedLog)"))
                {
                    cmd.Parameters.AddWithValue("solutionID", solutionID);
                    cmd.Parameters.AddWithValue("gzippedLog", gzippedLog);
                    cmd.ExecuteNonQuery();
                }
            }

            public void AddTestOutput(int testResultID, byte[] inputFile, byte[] outputFile, byte[] answerFile, byte[] stdout, byte[] stderr)
            {
                using (var cmd = MakeCmd("REPLACE INTO irunner_testoutput VALUES (@testResultID, @inputFile, @outputFile, @answerFile, @stdout, @stderr)"))
                {
                    cmd.Parameters.AddWithValue("testResultID", testResultID);
                    cmd.Parameters.AddWithValue("inputFile", inputFile);
                    cmd.Parameters.AddWithValue("outputFile", outputFile);
                    cmd.Parameters.AddWithValue("answerFile", answerFile);
                    cmd.Parameters.AddWithValue("stdout", stdout);
                    cmd.Parameters.AddWithValue("stderr", stderr);
                    cmd.ExecuteNonQuery();
                }
            }

            public void UpdateLanguageCmdLime(int languageID, string compileCmd, string runCmd)
            {
                using (var cmd = MakeCmd("REPLACE INTO irunner_languagecmdline VALUES (@id, @compileCmd, @runCmd)"))
                {
                    cmd.Parameters.AddWithValue("id", languageID);
                    cmd.Parameters.AddWithValue("compileCmd", compileCmd);
                    cmd.Parameters.AddWithValue("runCmd", runCmd);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private class IRunnerSequence
        {
            private const int MaxAttempts = 1000;
            private string ConnectionString;
            
            private readonly int SequenceID;
            private readonly int Step;
            
            private int CurStartValueInDB;
            
            private int BeginAllocated;
            private int EndAllocated;
            private int CurValue;

            public IRunnerSequence(string connectionString, string tableName, string idName)
            {
                ConnectionString = connectionString;

                using (var conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (var reader = MySqlHelper.ExecuteReader(conn, "SELECT sequenceID, value, step FROM katrin_sequence WHERE targetValues = @values", new MySqlParameter("values", tableName + ":" + idName)))
                    {
                        if (!reader.Read())
                        {
                            throw new MechanismException("Sequence was not found");
                        }
                        SequenceID = reader.GetInt32(0);
                        CurStartValueInDB = reader.GetInt32(1);
                        Step = reader.GetInt32(2);

                        if (Step <= 0)
                        {
                            throw new MechanismException("Step must be positive");
                        }

                        BeginAllocated = 0;
                        EndAllocated = 0;
                        CurValue = BeginAllocated;
                    }
                }
            }

            private void AllocateNewBlock()
            {
                using (var conn = new MySqlConnection(ConnectionString))
                {
                    conn.Open();
                    for (int attempt = 0; attempt < MaxAttempts; ++attempt)
                    {
                        int newValue = CurStartValueInDB + Step;
                        using (var cmd = new MySqlCommand("UPDATE katrin_sequence SET value = @newValue WHERE sequenceID = @id AND value = @curValue", conn))
                        {
                            cmd.Parameters.AddWithValue("id", SequenceID);
                            cmd.Parameters.AddWithValue("curValue", CurStartValueInDB);
                            cmd.Parameters.AddWithValue("newValue", newValue);
                            if (cmd.ExecuteNonQuery() == 1)
                            {
                                // cool, we took a new block of IDs of length Step
                                BeginAllocated = CurStartValueInDB;
                                EndAllocated = newValue;
                                CurStartValueInDB = newValue;
                                return;
                            }
                        }
                        
                        // Bad, we probably have not guessed the CurStartValueInDB, somebody has changed it.
                        // Try again.
                        
                        Logger.Info("Stale update");
                        
                        using (var cmd = new MySqlCommand("SELECT value FROM katrin_sequence WHERE sequenceID = @id", conn))
                        {
                            cmd.Parameters.AddWithValue("id", SequenceID);
                            CurStartValueInDB = Int32.Parse(cmd.ExecuteScalar().ToString());
                        }
                    }
                    throw new MechanismException("Impossible to allocate new range");
                }
            }

            public int AllocateOne()
            {
                if (CurValue >= EndAllocated)
                {
                    AllocateNewBlock();
                    CurValue = BeginAllocated;
                }
                if (CurValue < EndAllocated)
                {
                    return CurValue++;
                }
                else
                {
                    throw new MechanismException("Allocation did not help us no get ID range");
                }
            }
        }
    }
}
