﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace UCode.Worker.IRunnerMode
{
    enum EDataOrigin
    {
        OutputFile,
        StdOut,
        StdErr,
    }

    interface IPublicFileStorage
    {
        bool Put(IFileLocation handle, EDataOrigin origin);
    }

    class LocalPublicFileStorage : IPublicFileStorage
    {
        private string Directory;
        private HashSet<string> PresentFiles;

        public LocalPublicFileStorage(string directory)
        {
            Directory = directory;
            Logger.Info("loading public cache...");
            PresentFiles = new HashSet<string>();

            DirectoryInfo d = new DirectoryInfo(directory);
            FileInfo[] files = d.GetFiles();
            foreach (FileInfo file in files)
            {
                PresentFiles.Add(file.Name);
            }
            Logger.Info("{0} files in public file cache", PresentFiles.Count);
        }

        public bool Put(IFileLocation handle, EDataOrigin origin)
        {
            string sha1;
            long size;
            using (FileStream stream = handle.OpenRead())
            {
                sha1 = Util.ComputeSHA1(stream);
                size = stream.Length;
            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(Directory, "stats.txt"), true))
            {
                file.WriteLine("{0}\t{1}\t{2}", sha1, size, origin);
            }
            if (!PresentFiles.Contains(sha1))
            {
                handle.CopyTo(Path.Combine(Directory, sha1));
                PresentFiles.Add(sha1);
            }
            return true;
        }
    }

    class RemotePublicFileStorage : IPublicFileStorage
    {
        private Uri StorageURI;
        private Uri StorageUploadURI;
        private enum LookupResult
        {
            Exists,
            NotExists,
            Error,
        }

        public RemotePublicFileStorage(Uri storageURI)
        {
            StorageURI = storageURI;
            StorageUploadURI = new Uri(StorageURI, "/upload");
        }

        public bool Put(IFileLocation handle, EDataOrigin origin)
        {
            string sha1;
            using (FileStream stream = handle.OpenRead())
            {
                sha1 = Util.ComputeSHA1(stream);
            }
            LookupResult result = CheckFileAlreadyExists(sha1);
            Logger.Debug("FS File SHA1 {0}: {1}", sha1, result);

            switch (result)
            {
                case LookupResult.Exists:
                    return true;
                case LookupResult.NotExists:
                    using (var token = handle.MakeReadable())
                    {
                        return UploadFile(sha1, token.Path);
                    }
                case LookupResult.Error:
                    return false;
                default:
                    throw new MechanismException("unexpected result");
            }
        }

        private LookupResult CheckFileAlreadyExists(string sha1)
        {
            Uri fileURI = new Uri(StorageURI, "/info/" + sha1);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileURI);
            request.Method = "GET";
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return LookupResult.Exists;
                    }
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    if (httpResponse != null && httpResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        return LookupResult.NotExists;
                    }
                    Logger.Error("Public File Storage: unable to get status of file with SHA1 {0}: {1}", sha1, e.Message);
                }
            }
            return LookupResult.Error;
        }

        private bool UploadFile(string sha1, string path)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.UploadFile(StorageUploadURI, path);
                    return true;
                }
            }
            catch (WebException e)
            {
                Logger.Error("Public File Storage: unable to upload file with SHA1 {0}: {1}", sha1, e.Message);
                return false;
            }
        }
    }

    /*
    class MongoFileStorage : IPublicFileStorage
    {
        public MongoFileStorage(string directory)
        {
            Directory = directory;

        }
        public bool Put(OneFileStorage.Handle handle, EDataOrigin origin)
        {
            string sha1;
            long size;
            using (FileStream stream = File.OpenRead(handle.FullPath))
            {
                sha1 = Util.ComputeSHA1(stream);
                size = stream.Length;
            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Path.Combine(Directory, "stats.txt"), true))
            {
                file.WriteLine("{0}\t{1}\t{2}", sha1, size, origin);
            }
            if (!PresentFiles.Contains(sha1))
            {
                File.Copy(handle.FullPath, Path.Combine(Directory, sha1));
                PresentFiles.Add(sha1);
            }
            return true;
        }
    }*/
}
