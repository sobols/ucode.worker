﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCode.Worker.IRunnerMode
{
    static class Conversion
    {
        public static SolutionState GenerateSolutionState(TestingReport report)
        {
            SolutionState st = new SolutionState(SolutionState.Status.Done);

            var err = report.ToIRunnerError();
            st.SetError(err);
            if (err == SolutionState.Error.NoError)
            {
                st.SetAcceptInfo(true);
            }

            if (err != SolutionState.Error.InternalError)
            {
                st.SetNumber((uint)report.FirstFailedTestNo);
            }
            else
            {
                if (report.Outcome == SubmissionOutcome.GeneralFailure)
                {
                    switch (report.GeneralFailureReason)
                    {
                        case FailReason.GeneratorNotFound:
                            st.SetNumber(238); // Testing Error: test generator's file not found
                            break;
                        case FailReason.GeneratorNotCompiled:
                            st.SetNumber(239); // Testing Error: gererator compilation error
                            break;
                        case FailReason.GenerationFailed:
                            st.SetNumber(240); // Testing Error: not possible to run file generator
                            break;
                        case FailReason.NoFileGenerated:
                            st.SetNumber(241); // Testing Error: generator haven't generated a file
                            break;
                        case FailReason.SolutionNotFound:
                            st.SetNumber(312); // Solution file is missed in db
                            break;
                        case FailReason.CheckerNotFound:
                            st.SetNumber(366); // Checker file not found
                            break;
                        case FailReason.CheckerNotCompiled:
                            st.SetNumber(311); // Checker compilation error
                            break;
                        case FailReason.ProblemNotFound:
                            st.SetNumber(603); // No Tasks Found
                            break;
                    }
                }
                else if (report.Outcome == SubmissionOutcome.Incorrect)
                {
                    var testResult = report.ResultOfFirstFailedTest;
                    if (testResult != null && testResult.Outcome == TestCaseOutcome.CheckFailed)
                    {
                        switch (testResult.CFReason)
                        {
                            case CheckFailedReason.NoCheckerOutFileFound:
                                st.SetNumber(309); // checker hasn't produced checker.out file
                                break;
                            case CheckFailedReason.CheckerOutHasWrongFormat:
                                st.SetNumber(310); // checker produced checker.out with wrong format
                                break;
                            case CheckFailedReason.CheckerTimeLimitExceeded:
                                st.SetNumber(307); // Checker error: time limit exceed. Contact your administrator.
                                break;
                            case CheckFailedReason.UnexpectedExitCode:
                                st.SetNumber(308); // runtime exception in checker
                                break;
                            case CheckFailedReason.ReportedByChecker:
                                st.SetNumber(793); // got answer better than author's
                                break;
                        }
                    }
                }
            }
            return st;
        }

        public static IRunnerTestCaseResult GenerateTestCaseResult(TestCaseResult testResult)
        {
            return new IRunnerTestCaseResult(Convert.ToInt32(testResult.TestID),
                        testResult.Outcome.ToIRunnerError(),
                        testResult.TimeUsed,
                        testResult.MemoryUsed,
                        testResult.ExitCode,
                        testResult.OK,
                        testResult.Score,
                        testResult.CheckerMessage.AsString(NaturalLanguage.Russian));
        }
    }
}
