﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MySql.Data.MySqlClient;

namespace UCode.Worker.IRunnerMode
{
    static class DBOperationRetryer
    {
        public delegate void Action();

        public static void Run(Action action)
        {
            const int maxAttempts = 10;
            int timeOut = 1;

            for (int i = 0; i < maxAttempts; ++i)
            {
                try
                {
                    action();
                    return;
                }
                catch (Exception ex)
                {
                    if (IsNetworkTimeoutException(ex) && (i + 1 < maxAttempts))
                    {
                        // will try one more time
                        Logger.Error("Failure ({0}), will retry after {1} s", ex.Message, timeOut);
                        Thread.Sleep(timeOut * 1000);
                        timeOut *= 2;
                    }
                    else
                    {
                        // give up
                        throw;
                    }
                }
            }
        }

        static private bool IsNetworkTimeoutException(Exception ex)
        {
            if (ex is System.TimeoutException)
            {
                return true;
            }
            if (ex is MySqlException)
            {
                return true;
            }
            if (ex is System.Net.Sockets.SocketException)
            {
                return true;
            }

            return false;
        }
    }
}
