﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Reflection;

namespace UCode.Worker
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static int Main(string[] cmdLineArgs)
        {
            bool runAsService = !Environment.UserInteractive;
            string homeDirectory = runAsService ? AppDomain.CurrentDomain.BaseDirectory : Directory.GetCurrentDirectory();
            
            System.AppDomain.CurrentDomain.AssemblyResolve += (sender, args) => CustomResolve(sender, args, homeDirectory);
            
            Logger.Init(homeDirectory);

            WorkerService serviceToRun = new WorkerService(homeDirectory);

            if (runAsService)
            {
                ServiceBase.Run(serviceToRun);
                return 0;
            }
            else
            {
                Console.WriteLine("Hello, Sir! It's test server.");
                serviceToRun.RunAsConsoleApp(cmdLineArgs);
                return serviceToRun.ExitCode;
            }
        }

        private static Assembly FindInDir(string path, string name)
        {
            string fileName = Path.Combine(path, String.Format("{0}.dll", name));
            return File.Exists(fileName) ? Assembly.LoadFile(fileName) : null;
        }

        private static Assembly CustomResolve(object sender, System.ResolveEventArgs args, string homeDirectory)
        {
            Assembly res = null;
            string name = new AssemblyName(args.Name).Name;

            res = FindInDir(Path.Combine(homeDirectory, "Lib", Environment.Is64BitProcess ? "x64" : "x86"), name);
            if (res != null) return res;

            res = FindInDir(Path.Combine(homeDirectory, "Lib"), name);
            if (res != null) return res;

            return null;
        }
    }
}
