﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    interface ISingleFileStorage: IDisposable
    {
        IFileLocation Put(IDataSource source);
    }

    interface ISingleFileStorageFactory
    {
        ISingleFileStorage Create();
        ISingleFileStorage Create(string nameHint);
    }

    class RandomFileNameStorage : DisposableDirectory, ISingleFileStorage
    {
        public RandomFileNameStorage(string root)
            : base(root)
        {
        }

        public IFileLocation Put(IDataSource source)
        {
            string path = Path.Combine(Path_, CreateNewFileName());
            try
            {
                source.SaveToFile(path);
                return new FileLocation(path);
            }
            catch
            {
                try
                {
                    File.Delete(path);
                }
                catch
                {
                }
                throw;
            }
        }

        private string CreateNewFileName()
        {
            return Path.GetRandomFileName();
        }
    }

    class SeqNumericFileNameStorage : DisposableDirectory, ISingleFileStorage
    {
        private const int FilesInDirectoryLimit = 1000;
        private int FileCount_ = 0;

        private HashSet<int> FilesAvailable;
        private HashSet<int> DirectoriesCreated;

        public SeqNumericFileNameStorage(string root)
            : base(root)
        {
            FilesAvailable = new HashSet<int>();
            DirectoriesCreated = new HashSet<int>();
        }

        public IFileLocation Put(IDataSource src)
        {
            int fileDescr = FileCount_;
            FileCount_++;

            var loc = GetFileLocation(fileDescr, false);
            if (!DirectoriesCreated.Contains(loc.DirectoryNo))
            {
                var dir = Path.Combine(Path_, GetDirectoryByNo(loc.DirectoryNo));
                Directory.CreateDirectory(dir);
                DirectoriesCreated.Add(loc.DirectoryNo);
            }

            Handle handle = new Handle(fileDescr, this);
            var path = GetFilePath(handle, false);
            src.SaveToFile(path);
            FilesAvailable.Add(fileDescr);
            return handle;
        }

        private struct Location
        {
            public readonly int DirectoryNo;
            public readonly int LocalFileNo;

            public Location(int directoryNo, int localFileNo)
            {
                DirectoryNo = directoryNo;
                LocalFileNo = localFileNo;
            }
        }

        private Location GetFileLocation(int fileDescr, bool verified = true)
        {
            if (verified && !FilesAvailable.Contains(fileDescr))
            {
                throw new MechanismException(String.Format("file {0} does not exist", fileDescr));
            }
            int directoryNo = fileDescr / FilesInDirectoryLimit;
            int localFileNo = fileDescr % FilesInDirectoryLimit;
            return new Location(directoryNo, localFileNo);
        }

        private static string GetDirectoryByNo(int directoryNo)
        {
            return directoryNo.ToString("D4");
        }

        private static string GetFileByNo(int localFileNo)
        {
            return localFileNo.ToString("D3");
        }

        /// <returns>Absolute path to file</returns>
        private String GetFilePath(Handle handle, bool verified = true)
        {
            var loc = GetFileLocation(handle.FileDescr, verified);
            return Path.Combine(Path_, GetDirectoryByNo(loc.DirectoryNo), GetFileByNo(loc.LocalFileNo));
        }

        public class Handle : FileLocationBase
        {
            public readonly int FileDescr;
            private readonly SeqNumericFileNameStorage FS_;

            public Handle(int fileDescr, SeqNumericFileNameStorage fs)
            {
                FileDescr = fileDescr;
                FS_ = fs;
            }

            public override string ToString()
            {
                return "#" + FileDescr.ToString();
            }

            protected override string Path_
            {
                get { return FS_.GetFilePath(this); }
            }
        }
    }
}
