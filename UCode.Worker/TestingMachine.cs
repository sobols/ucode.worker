﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker
{
    delegate TestingJob JobFetcher(long jobID);

    struct JobReport
    {
        public TestingJob Job;
        public TestingReport Report;
    }

    sealed class TestingMachine
    {
        private readonly CompilationMachine CompMachine_;
        private readonly ExecutionMachine ExecMachine_;
        private readonly CheckerFactory CheckerFactory_;
        private readonly ISingleFileStorage StorageForProblemOutputs_;
        private readonly bool StoreOutputs_;
        private readonly int? CpuCore_;

        public event EventHandler<CompilingEventArgs> Compiling;
        public event EventHandler<CompiledEventArgs> Compiled;
        public event EventHandler<TestingCasesEventArgs> TestingCases;
        public event EventHandler<TestCaseTestingEventArgs> TestCaseTesting;
        public event EventHandler<TestCaseTestedEventArgs> TestCaseTested;
        public event EventHandler<FileGeneratingEventArgs> FileGenerating;
        public event EventHandler<FileGeneratedEventArgs> FileGenerated;

        public TestingMachine(CompilationMachine compMachine, ExecutionMachine execMachine, CheckerFactory checkerFactory, ISingleFileStorage storageForProblemOutputs, bool storeOutputs, int? cpuCore)
        {
            CompMachine_ = compMachine;
            ExecMachine_ = execMachine;
            CheckerFactory_ = checkerFactory;
            StorageForProblemOutputs_ = storageForProblemOutputs;
            StoreOutputs_ = storeOutputs;
            CpuCore_ = cpuCore;
        }

        #region public interface
        
        public JobReport FetchAndTest(long jobID, JobFetcher fetcher, int thrNum, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            TestingReportFactory reportFactory = new TestingReportFactory(jobID);
            TestingJob job = null;
            TestingReport report = null;
            try
            {
                job = fetcher(jobID);
                report = DoTest(job, reportFactory, thrNum, token);
            }
            catch (AbortTestingException ex)
            {
                report = reportFactory.RespondWithGeneralFailure(ex.Reason, ex.Message);
            }
            WriteReportToLog(report);

            return new JobReport() { Job = job, Report = report };
        }
        
        public TestingReport Test(TestingJob job, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            TestingReportFactory reportFactory = new TestingReportFactory(job.ID);
            TestingReport report;
            try
            {
                report = DoTest(job, reportFactory, 0, token);
            }
            catch (CliRunLib2.RunException ex)
            {
                report = reportFactory.RespondWithGeneralFailure(FailReason.RunLibError, ex.Message);
            }
            WriteReportToLog(report);
            return report;
        }

        #endregion

        private TestingReport DoTest(TestingJob job, TestingReportFactory reportFactory, int thrNum, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();

            Logger.Info("===== T{0}: Testing job {1} =====", thrNum, job.ID);

            // compiling solution
            CompiledProgram solutionProgram = null;
            if (job.Solution != null)
            {
                OnCompiling(new CompilingEventArgs(job.Solution, ProgramType.Solution));
                var compiledSolution = CompMachine_.Compile(job.Solution, ProgramType.Solution, CpuCore_, token);
                EnsureProgrammingLanguageIsKnown(job.Solution, compiledSolution);
                OnCompiled(new CompiledEventArgs(job.Solution, ProgramType.Solution, compiledSolution));
                reportFactory.SolutionCompilationLog = compiledSolution.CompilationLog;
                reportFactory.SolutionCompilationLogFile = compiledSolution.CompilationLogFile;

                if (!compiledSolution.OK)
                {
                    return reportFactory.RespondWithCompilationError(ProgramType.Solution);
                }
                solutionProgram = compiledSolution.Program;
            }

            token.ThrowIfCancellationRequested();

            ProblemInstance problem = job.Problem;
            if (problem != null)
            {
                // compiling checker if required
                CompiledProgram checkerProgram = null;
                if (problem.CheckerSource != null)
                {
                    OnCompiling(new CompilingEventArgs(problem.CheckerSource, ProgramType.Checker));
                    var compiledChecker = CompMachine_.Compile(problem.CheckerSource, ProgramType.Checker, CpuCore_, token);
                    EnsureProgrammingLanguageIsKnown(problem.CheckerSource, compiledChecker);
                    OnCompiled(new CompiledEventArgs(problem.CheckerSource, ProgramType.Checker, compiledChecker));
                    reportFactory.CheckerCompilationLog = compiledChecker.CompilationLog;
                    reportFactory.CheckerCompilationLogFile = compiledChecker.CompilationLogFile;

                    if (!compiledChecker.OK)
                    {
                        return reportFactory.RespondWithCompilationError(ProgramType.Checker);
                    }
                    checkerProgram = compiledChecker.Program;
                }

                IChecker checker = CheckerFactory_.Create(problem.CheckerKind, checkerProgram);

                IValidator validator = null;

                if (problem.ValidatorSource != null)
                {
                    // compiling validator if required
                    OnCompiling(new CompilingEventArgs(problem.ValidatorSource, ProgramType.Validator));
                    var compiledValidator = CompMachine_.Compile(problem.ValidatorSource, ProgramType.Validator, CpuCore_, token);
                    EnsureProgrammingLanguageIsKnown(problem.ValidatorSource, compiledValidator);
                    OnCompiled(new CompiledEventArgs(problem.ValidatorSource, ProgramType.Validator, compiledValidator));

                    if (!compiledValidator.OK)
                    {
                        throw new AbortTestingException(FailReason.ValidatorNotCompiled);
                    }
                    validator = new TestLibValidator(ExecMachine_, compiledValidator.Program);
                }

                IScorer scorer = null;
                if (problem.ScorerSource != null)
                {
                    // compiling scorer if required
                    OnCompiling(new CompilingEventArgs(problem.ScorerSource, ProgramType.Scorer));
                    var compiledScorer = CompMachine_.Compile(problem.ScorerSource, ProgramType.Scorer, CpuCore_, token);
                    EnsureProgrammingLanguageIsKnown(problem.ScorerSource, compiledScorer);
                    OnCompiled(new CompiledEventArgs(problem.ScorerSource, ProgramType.Scorer, compiledScorer));

                    if (!compiledScorer.OK)
                    {
                        return reportFactory.RespondWithCompilationError(ProgramType.Scorer);
                    }
                    scorer = new TestLibScorer(ExecMachine_, compiledScorer.Program);
                }
                else
                {
                    scorer = new SumUpScorer();
                }

                IInteractor interactor = null;
                if (problem.InteractorSource != null)
                {
                    // compiling interactor if required
                    OnCompiling(new CompilingEventArgs(problem.InteractorSource, ProgramType.Interactor));
                    var compiledInteractor = CompMachine_.Compile(problem.InteractorSource, ProgramType.Interactor, CpuCore_, token);
                    EnsureProgrammingLanguageIsKnown(problem.InteractorSource, compiledInteractor);
                    OnCompiled(new CompiledEventArgs(problem.InteractorSource, ProgramType.Interactor, compiledInteractor));

                    if (!compiledInteractor.OK)
                    {
                        return reportFactory.RespondWithCompilationError(ProgramType.Interactor);
                    }
                    interactor = new TestLibInteractor(compiledInteractor.Program);
                }

                List<TestCase> testCases = GenerateAllTests(problem, reportFactory, token);

                OnTestingCases(new TestingCasesEventArgs(job));

                for (int i = 0; i < testCases.Count; ++i)
                {
                    var test = testCases[i];
                    var testNo = i + 1; // 1-based

                    token.ThrowIfCancellationRequested();

                    OnTestCaseTesting(new TestCaseTestingEventArgs(testNo, test));
                    bool storeOutputs = StoreOutputs_ || test.IsSample || testCases.Count == 1;
                    var testRes = RunSingleTest(test, problem.InputFileName, problem.OutputFileName, solutionProgram, interactor, checker, validator, storeOutputs, problem.RunTwice, token);
                    OnTestCaseTested(new TestCaseTestedEventArgs(testNo, test, testRes));
                    reportFactory.AddTestResult(testRes);

                    if (testRes.Outcome != TestCaseOutcome.OK)
                    {
                        if (job.StopAfterFirstFailedTest)
                        {
                            break;
                        }
                        if (test.IsSample)
                        {
                            break;
                        }
                    }
                }

                var scoringResult = scorer.Evaluate(reportFactory.GetResults(), token);
                if (scoringResult.OK)
                {
                    reportFactory.SetScore(scoringResult.Score);
                }
                else
                {
                    return reportFactory.RespondWithGeneralFailure(FailReason.ScoringFailed, scoringResult.ToString());
                }
            }
            return reportFactory.RespondWithSuccessfulTermination();
        }

        private void EnsureProgrammingLanguageIsKnown(SourceCode source, CompilationResult result)
        {
            if (result.Outcome == CompilationResult.OutcomeEnum.UnknownProgrammingLanguage)
            {
                throw new AbortTestingException(FailReason.UnknownProgrammingLanguage, source.LangID);
            }
        }

        private List<TestCase> GenerateAllTests(ProblemInstance problem, TestingReportFactory reportFactory, CancellationToken token)
        {
            var geners = new Dictionary<SourceCode, CompiledProgram>();

            foreach (var testCase in problem.Tests)
            {
                if (testCase.Input != null && testCase.Input.NeedsToBeGenerated)
                {
                    geners[testCase.Input.Generator] = null;
                }
                if (testCase.Answer != null && testCase.Answer.NeedsToBeGenerated)
                {
                    geners[testCase.Answer.Generator] = null;
                }
            }
            if (geners.Count > 0)
            {
                Logger.Info("Need to compile {0} {1}", geners.Count, geners.Count == 1 ? "generator" : "generators");
            }

            foreach (var gener in geners.Keys.ToArray())
            {
                OnCompiling(new CompilingEventArgs(gener, ProgramType.Generator));
                var compiledGener = CompMachine_.Compile(gener, ProgramType.Generator, CpuCore_, token);
                OnCompiled(new CompiledEventArgs(gener, ProgramType.Generator, compiledGener));
                if (!compiledGener.OK)
                {
                    throw new AbortTestingException(FailReason.GeneratorNotCompiled);
                }
                geners[gener] = compiledGener.Program;
            }

            List<TestCase> newTestCases = new List<TestCase>();

            for (int i = 0; i < problem.Tests.Count; ++i)
            {
                var testNo = i + 1;

                TestCaseFile input = problem.Tests[i].Input;
                TestCaseFile answer = problem.Tests[i].Answer;

                if (input != null && input.NeedsToBeGenerated)
                {
                    OnFileGenerating(new FileGeneratingEventArgs(testNo, GenerableFileType.Input));
                    IFileLocation handle = GenerateFile(geners[input.Generator], testNo, input.Attributes, token);
                    input = input.SetGeneratedFile(handle);
                    OnFileGenerated(new FileGeneratedEventArgs(testNo, GenerableFileType.Input, handle));
                }
                if (answer != null && answer.NeedsToBeGenerated)
                {
                    OnFileGenerating(new FileGeneratingEventArgs(testNo, GenerableFileType.Answer));
                    IFileLocation handle = GenerateFile(geners[answer.Generator], testNo, answer.Attributes, token);
                    answer = answer.SetGeneratedFile(handle);
                    OnFileGenerated(new FileGeneratedEventArgs(testNo, GenerableFileType.Answer, handle));
                }

                TestCase old = problem.Tests[i];
                newTestCases.Add(new TestCase(old.ID, input, answer, old.TimeLimit, old.MemoryLimit, old.MaxScore, old.IsSample));
            }

            return newTestCases;
        }
        
        private IFileLocation GenerateFile(CompiledProgram gener, int testNo, string attributes, CancellationToken token)
        {
            string outputFileName = "output.txt";
            var settings = new ExecutionSettings();
            var command = new ExecutionCommand(gener);
            var output = new OneFileStorageMoveDataDestination(StorageForProblemOutputs_);
            settings.OutputFiles[outputFileName] = output;
            command.CommandLineArgs.Add(outputFileName);
            command.CommandLineArgs.Add(testNo.ToString());
            settings.CpuCore = CpuCore_;
            if (attributes != null)
            {
                command.CommandLineArgs.Add(attributes);
            }

            var execResult = ExecMachine_.Run(settings, command, token);
            if (!execResult.OK)
            {
                throw new AbortTestingException(FailReason.GenerationFailed, execResult.ToString());
            }
            if (output.Handle == null)
            {
                throw new AbortTestingException(FailReason.NoFileGenerated);
            }
            return output.Handle;
        }

        private TestCaseResult RunSingleTest(TestCase test, string inputFileName, string outputFileName, CompiledProgram solution, IInteractor interactor, IChecker checker, IValidator validator, bool storeOutputs, bool runTwice, CancellationToken token)
        {
            IFileLocation inputFileLocation = (test.Input != null) ? test.Input.Handle : null;
            IFileLocation answerFileLocation = (test.Answer != null) ? test.Answer.Handle : null;

            if (validator != null && inputFileLocation != null)
            {
                ValidationResult validationResult = validator.Validate(inputFileLocation, token);
                if (!validationResult.IsValid)
                {
                    return new TestCaseResult(
                        test.ID,
                        TestCaseOutcome.CheckFailed,
                        CheckFailedReason.InvalidInput,
                        0,
                        0,
                        0,
                        0,
                        new Messages.RawStringMessage(validationResult.Message),
                        inputFileLocation,
                        null,
                        answerFileLocation,
                        null,
                        null);
                }
            }

            if (solution == null)
            {
                // nothing to execute
                return new TestCaseResult(
                    test.ID,
                    TestCaseOutcome.OK,
                    CheckFailedReason.Unknown,
                    0,
                    0,
                    0,
                    test.MaxScore,
                    Messages.EmptyMessage.Instance,
                    inputFileLocation,
                    null,
                    answerFileLocation,
                    null,
                    null);
            }

            if (interactor != null || runTwice) // TODO: Switch all to TestCaseRunner
            {
                var testRunner = new TestCaseRunner(ExecMachine_, StorageForProblemOutputs_, test, inputFileName, outputFileName, solution, checker, interactor, runTwice, storeOutputs, CpuCore_);
                return testRunner.Run(token);
            }

            var settings = new ExecutionSettings();
            var command = new ExecutionCommand(solution);
            // input
            if (inputFileLocation != null)
            {
                if (!String.IsNullOrEmpty(inputFileName))
                {
                    settings.InputFiles[inputFileName] = inputFileLocation.GetDataSource();
                }
                else
                {
                    command.StdIn = inputFileLocation.GetDataSource();
                }
            }

            OneFileStorageMoveDataDestination stdOutDestination = new OneFileStorageMoveDataDestination(StorageForProblemOutputs_);
            command.StdOut = stdOutDestination;

            OneFileStorageMoveDataDestination stdErrDestination = new OneFileStorageMoveDataDestination(StorageForProblemOutputs_);
            command.StdErr = stdErrDestination;

            // output
            OneFileStorageMoveDataDestination outputDestination = null;
            if (!String.IsNullOrEmpty(outputFileName))
            {
                outputDestination = new OneFileStorageMoveDataDestination(StorageForProblemOutputs_);
                settings.OutputFiles[outputFileName] = outputDestination;
            }
            else
            {
                outputDestination = stdOutDestination;
            }

            // limits
            command.TimeLimit = test.TimeLimit;
            command.MemoryLimit = test.MemoryLimit;
            settings.CpuCore = CpuCore_;

            var execResult = ExecMachine_.Run(settings, command, token);

            if (execResult.Outcome != ExecutionResult.OutcomeEnum.OK)
            {
                return new TestCaseResult(
                    test.ID,
                    execResult.Outcome.ToTestCaseOutcome(),
                    CheckFailedReason.Unknown,
                    execResult.TimeUsed,
                    execResult.MemoryUsed,
                    execResult.ExitCode,
                    0.0,
                    Messages.EmptyMessage.Instance,
                    inputFileLocation,
                    storeOutputs ? outputDestination.Handle : null,
                    answerFileLocation,
                    storeOutputs ? stdOutDestination.Handle : null,
                    storeOutputs ? stdErrDestination.Handle : null);
            }
            else
            {
                var handles = new CheckingHandles(inputFileLocation, outputDestination.Handle, answerFileLocation);
                var checkingResult = checker.Check(handles, CpuCore_, token, test.MaxScore);

                // replace Ok with checker output
                return new TestCaseResult(
                    test.ID,
                    checkingResult.Outcome.ToTestCaseOutcome(),
                    checkingResult.FailReason,
                    execResult.TimeUsed,
                    execResult.MemoryUsed,
                    execResult.ExitCode,
                    checkingResult.Score.Get(test.MaxScore),
                    checkingResult.Message,
                    inputFileLocation,
                    storeOutputs ? outputDestination.Handle : null,
                    answerFileLocation,
                    storeOutputs ? stdOutDestination.Handle : null,
                    storeOutputs ? stdErrDestination.Handle : null);
            }
        }
    
        private static void WriteReportToLog(TestingReport report)
        {
            var outcome = report.Outcome;
            const string testingDone = "Testing done:";
            switch (outcome)
            {
                case SubmissionOutcome.Correct:
                    Logger.InfoPositive(testingDone, outcome.ToString());
                    break;
                case SubmissionOutcome.Incorrect:
                    Logger.InfoNegative(testingDone, report.ResultOfFirstFailedTest.GetOutcomeString(), String.Format("on test {0}", report.FirstFailedTestNo));
                    break;
                case SubmissionOutcome.SolutionCompilationError:
                    Logger.InfoNegative(testingDone, outcome.ToString());
                    break;
                case SubmissionOutcome.GeneralFailure:
                    Logger.InfoNegative(testingDone, outcome.ToString());
                    Logger.Error("Reason: {0}", report.GeneralFailureReason);
                    if (!String.IsNullOrEmpty(report.GeneralFailureMessage))
                    {
                        Logger.Error("Message: \"{0}\"", report.GeneralFailureMessage);
                    }
                    break;
                default:
                    throw new MechanismException();
            }
        }

        private void OnCompiling(CompilingEventArgs args)
        {
            Logger.Info("Compiling {0} using {1}...", args.Type, args.Code.LangID);
            if (Compiling != null)
            {
                Compiling(this, args);
            }
        }
        private void OnCompiled(CompiledEventArgs args)
        {
            if (args.Result.OK)
            {
                Logger.InfoPositive("Compilation", "succeeded");
            }
            else
            {
                Logger.InfoNegative("Compilation", "failed:", args.Result.Outcome.ToString());
            }
            if (Compiled != null)
            {
                Compiled(this, args);
            }
        }
        private void OnTestingCases(TestingCasesEventArgs args)
        {
            Logger.Info("Problem has {0} tests", args.Job.Problem.Tests.Count);
            if (TestingCases != null)
            {
                TestingCases(this, args);
            }
        }
        private void OnTestCaseTesting(TestCaseTestingEventArgs args)
        {
            if (TestCaseTesting != null)
            {
                TestCaseTesting(this, args);
            }
        }
        private void OnTestCaseTested(TestCaseTestedEventArgs args)
        {
            string number = String.Format("{0} {1,-2}", (args.TestCase.IsSample ? "*" : "#"), args.TestNumber);
            TestCase testCase = args.TestCase;
            TestCaseResult result = args.Result;
            var code = String.Format("[{0}]", result.Outcome.TwoLetterCode());
            var formattedMessage = String.Format("{0,2} p {1,5} ms {2,5:0.0} MB  code {3}, {4}/{5}",
                result.Score,
                result.TimeUsed,
                Util.BytesToMegabytes(result.MemoryUsed),
                result.ExitCode,
                (testCase.Input != null && testCase.Input.Handle != null) ? testCase.Input.Handle.ToString() : "N/A",
                (testCase.Answer != null && testCase.Answer.Handle != null) ? testCase.Answer.Handle.ToString() : "N/A");

            if (result.Outcome == TestCaseOutcome.OK)
            {
                Logger.InfoPositive(number, code, formattedMessage);
            }
            else
            {
                Logger.InfoNegative(number, code, formattedMessage);
            }

            if (TestCaseTested != null)
            {
                TestCaseTested(this, args);
            }
        }
        private void OnFileGenerating(FileGeneratingEventArgs args)
        {
            if (FileGenerating != null)
            {
                FileGenerating(this, args);
            }
        }
        private void OnFileGenerated(FileGeneratedEventArgs args)
        {
            Logger.InfoPositive(null, "Generated", String.Format("{0} {1} for test #{2}", args.FileType, args.GeneratedFile, args.TestNumber));
            if (FileGenerated != null)
            {
                FileGenerated(this, args);
            }
        }
    }

    #region event args
    class CompilingEventArgs : EventArgs
    {
        public SourceCode Code { get; private set; }
        public ProgramType Type { get; private set; }

        public CompilingEventArgs(SourceCode code, ProgramType type)
        {
            Code = code;
            Type = type;
        }
    }
    class CompiledEventArgs : EventArgs
    {
        public SourceCode Code { get; private set; }
        public ProgramType Type { get; private set; }
        public CompilationResult Result { get; private set; }

        public CompiledEventArgs(SourceCode code, ProgramType type, CompilationResult result)
        {
            Code = code;
            Type = type;
            Result = result;
        }
    }
    class TestingCasesEventArgs : EventArgs
    {
        public TestingJob Job { get; private set; }

        public TestingCasesEventArgs(TestingJob job)
        {
            Job = job;
        }
    }
    class TestCaseTestingEventArgs : EventArgs
    {
        public int TestNumber { get; private set; }
        public TestCase TestCase { get; private set; }

        public TestCaseTestingEventArgs(int testNumber, TestCase testCase)
        {
            TestNumber = testNumber;
            TestCase = testCase;
        }
    }
    class TestCaseTestedEventArgs : EventArgs
    {
        public int TestNumber { get; private set; }
        public TestCase TestCase { get; private set; }
        public TestCaseResult Result { get; private set; }

        public TestCaseTestedEventArgs(int testNumber, TestCase testCase, TestCaseResult result)
        {
            TestNumber = testNumber;
            TestCase = testCase;
            Result = result;
        }
    }
    class FileGeneratingEventArgs : EventArgs
    {
        public int TestNumber { get; private set; }
        public GenerableFileType FileType { get; private set; }

        public FileGeneratingEventArgs(int testNumber, GenerableFileType fileType)
        {
            TestNumber = testNumber;
            FileType = fileType;
        }
    }
    class FileGeneratedEventArgs : EventArgs
    {
        public int TestNumber { get; private set; }
        public GenerableFileType FileType { get; private set; }
        public IFileLocation GeneratedFile { get; private set; }

        public FileGeneratedEventArgs(int testNumber, GenerableFileType fileType, IFileLocation generatedFile)
        {
            TestNumber = testNumber;
            FileType = fileType;
            GeneratedFile = generatedFile;
        }
    }
    #endregion

    class TestingMachineFactory
    {
        private readonly CompilationMachine CompilationMachine_;
        private readonly ExecutionMachine ExecutionMachine_;
        private readonly CheckerFactory CheckerFactory_;
        private readonly bool StoreOutputs_;

        public TestingMachineFactory(CompilationMachine compilationMachine, ExecutionMachine executionMachine, CheckerFactory checkerFactory, bool storeOutputs)
        {
            CompilationMachine_ = compilationMachine;
            ExecutionMachine_ = executionMachine;
            CheckerFactory_ = checkerFactory;
            StoreOutputs_ = storeOutputs;
        }

        public TestingMachine Create(ISingleFileStorage storageForProblemOutputs, int? cpuCore = null)
        {
            return new TestingMachine(CompilationMachine_, ExecutionMachine_, CheckerFactory_, storageForProblemOutputs, StoreOutputs_, cpuCore);
        }
    }
}
