﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCode.Worker
{
    enum NaturalLanguage
    {
        English,
        Russian
    }

    interface ICheckerMessage
    {
        string AsString(NaturalLanguage lang);
    }

    namespace Messages
    {
        class EmptyMessage : ICheckerMessage
        {
            public string AsString(NaturalLanguage lang)
            {
                return String.Empty;
            }

            public static readonly EmptyMessage Instance = new EmptyMessage();
        }

        class RawStringMessage : ICheckerMessage
        {
            private string Message;

            public RawStringMessage(string message)
            {
                Message = (message != null) ? message : String.Empty;
            }
            public string AsString(NaturalLanguage lang)
            {
                return Message;
            }
        }

        class OutputFileNotFoundMessage : ICheckerMessage
        {
            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return "выходной файл не найден";
                    default:
                        return "output file not found";
                }
            }
        }

        class OKMessage : ICheckerMessage
        {
            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return "OK";
                    default:
                        return "OK";
                }
            }
        }

        class FilesDifferAtLineMessage : ICheckerMessage
        {
            private int LineNumber;
            public FilesDifferAtLineMessage(int lineNumber)
            {
                LineNumber = lineNumber;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("файлы различаются в строке {0}", LineNumber);
                    default:
                        return String.Format("files differ at line {0}", LineNumber);
                }
            }
        }

        class FilesDifferAtPos : ICheckerMessage
        {
            private long Pos;
            public FilesDifferAtPos(long pos)
            {
                Pos = pos;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("файлы различаются в позиции {0}", Pos);
                    default:
                        return String.Format("files differ at position {0}", Pos);
                }
            }
        }
        
        // IRunner checkers
        class CheckerOutLineNotFoundMessage : ICheckerMessage
        {
            private int LineNumber;
            public CheckerOutLineNotFoundMessage(int lineNumber)
            {
                LineNumber = lineNumber;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("не удаётся прочитать строку {0} файла checker.out", LineNumber);
                    default:
                        return String.Format("unable to read line {0} of checker.out file", LineNumber);
                }
            }
        }

        class CheckerOutWrongFirstLineMessage : ICheckerMessage
        {
            private string Line;
            public CheckerOutWrongFirstLineMessage(string line)
            {
                Line = line;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("в первой строке файла checker.out записано '{0}', ожидалось 0 или 1", Line);
                    default:
                        return String.Format("first line of checker.out is '{0}', expected 0 or 1", Line);
                }
            }
        }

        class CheckerOutWrongDoubleNumber : ICheckerMessage
        {
            private string Line;
            public CheckerOutWrongDoubleNumber(string line)
            {
                Line = line;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("не удалось преобразовать '{0}' в вещественное число", Line);
                    default:
                        return String.Format("unable to parse '{0}' as real number", Line);
                }
            }
        }
        class CheckerOutWrongIntegerNumber : ICheckerMessage
        {
            private string Line;
            public CheckerOutWrongIntegerNumber(string line)
            {
                Line = line;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("не удалось преобразовать '{0}' в целое число", Line);
                    default:
                        return String.Format("unable to parse '{0}' as integer number", Line);
                }
            }
        }
        class CheckerOutUnknownErrorCode : ICheckerMessage
        {
            private int Code;
            public CheckerOutUnknownErrorCode(int code)
            {
                Code = code;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("неизвестный код ошибки {0} в checker.out", Code);
                    default:
                        return String.Format("unknown error code {0} in checker.out", Code);
                }
            }
        }

        class LengthDiffer : ICheckerMessage
        {
            private long OutputLength;
            private long AnswerLength;

            public LengthDiffer(long outputLength, long answerLength)
            {
                OutputLength = outputLength;
                AnswerLength = answerLength;
            }

            public string AsString(NaturalLanguage lang)
            {
                switch (lang)
                {
                    case NaturalLanguage.Russian:
                        return String.Format("размеры файлов отличаются: выход {0} Б, ответ {1} Б", OutputLength, AnswerLength);
                    default:
                        return String.Format("file sizes differ: output is {0} B, answer is {1} B", OutputLength, AnswerLength);
                }
            }
        }

        class MultiMessage : ICheckerMessage
        {
            private readonly List<ICheckerMessage> Messages_ = new List<ICheckerMessage>();

            public void Add(ICheckerMessage message)
            {
                Messages_.Add(message);
            }
            public string AsString(NaturalLanguage lang)
            {
                return String.Join(" // ", Messages_.Select(m => m.AsString(lang)));
            }
        }
    }
}
