﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    enum BoundaryScore
    {
        None,
        Min,
        Max,
    }
    internal struct Score
    {
        private readonly BoundaryScore BoundaryScore_;
        private readonly double? ExplicitScore_;

        public double Get(double maxScoreForTest)
        {
            if (BoundaryScore_ == BoundaryScore.Min)
            {
                return 0.0;
            }
            if (BoundaryScore_ == BoundaryScore.Max)
            {
                return maxScoreForTest;
            }
            if (ExplicitScore_.HasValue)
            {
                return ExplicitScore_.Value;
            }
            return 0.0;
        }
        private Score(BoundaryScore boundaryScore, double? explicitScore = null)
        {
            BoundaryScore_ = boundaryScore;
            ExplicitScore_ = explicitScore;
        }
        public static Score None()
        {
            return new Score(BoundaryScore.None);
        }
        public static Score Min()
        {
            return new Score(BoundaryScore.Min);
        }
        public static Score Max()
        {
            return new Score(BoundaryScore.Max);
        }
        public static Score Points(double explicitScore)
        {
            return new Score(BoundaryScore.None, explicitScore);
        }
        public static Score Merge(Score first, Score second)
        {
            if (first.BoundaryScore_ == BoundaryScore.Min || second.BoundaryScore_ == BoundaryScore.Min)
            {
                return Min();
            }
            if (first.ExplicitScore_.HasValue && second.ExplicitScore_.HasValue)
            {
                return Points(Math.Min(first.ExplicitScore_.Value, second.ExplicitScore_.Value));
            }
            if (first.ExplicitScore_.HasValue)
            {
                return Points(first.ExplicitScore_.Value);
            }
            if (second.ExplicitScore_.HasValue)
            {
                return Points(second.ExplicitScore_.Value);
            }
            return Max();
        }
    }
}
