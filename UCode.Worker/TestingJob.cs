﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    enum ProgramType
    {
        Solution,
        Checker,
        Generator,
        Validator,
        Scorer,
        Interactor,
    }

    enum GenerableFileType
    {
        Input,
        Answer,
    }

    class TestingJob
    {
        public readonly long ID;
        public readonly ProblemInstance Problem;
        public readonly SourceCode Solution;
        public readonly bool StopAfterFirstFailedTest;

        public TestingJob(long id, ProblemInstance problem, SourceCode solution, bool stopAfterFirstFail = false)
        {
            ID = id;
            Problem = problem;
            Solution = solution;
            StopAfterFirstFailedTest = stopAfterFirstFail;
        }
    }
}
