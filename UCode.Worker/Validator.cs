﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class ValidationResult
    {
        public readonly bool IsValid;
        public readonly string Message;

        private ValidationResult(bool isValid, string message)
        {
            IsValid = isValid;
            Message = message;
        }

        public static ValidationResult Valid()
        {
            return new ValidationResult(true, null);
        }
        public static ValidationResult Invalid(string message)
        {
            return new ValidationResult(false, message);
        }
    }

    interface IValidator
    {
        ValidationResult Validate(IFileLocation input, CancellationToken token);
    }

    class TestLibValidator: IValidator
    {
        private ExecutionMachine ExecutionMachine_;
        private CompiledProgram ValidatorProgram_;

        private const long VALIDATOR_TIME_LIMIT = 30 * 1000; // 30 seconds
        private const string CUT_PREFIX = "FAIL ";
        private const string CUT_SUFFIX = " (stdin)";

        public TestLibValidator(ExecutionMachine executionMachine, CompiledProgram validatorProgram)
        {
            ExecutionMachine_ = executionMachine;
            ValidatorProgram_ = validatorProgram;
        }

        public ValidationResult Validate(IFileLocation input, CancellationToken token)
        {
            MemoryDataDestination validatorOut = new MemoryDataDestination();
            var execSettings = new ExecutionSettings();
            var execCommand = new ExecutionCommand(ValidatorProgram_);
            execCommand.TimeLimit = VALIDATOR_TIME_LIMIT;
            execCommand.StdIn = input.GetDataSource();
            execCommand.StdErr = validatorOut;
            ExecutionResult execResult = ExecutionMachine_.Run(execSettings, execCommand, token);

            switch (execResult.Outcome)
            {
                case ExecutionResult.OutcomeEnum.OK:
                    return ValidationResult.Valid();
                case ExecutionResult.OutcomeEnum.RuntimeError:
                    return ValidationResult.Invalid(ParseValidatorOut(validatorOut.Stream));
                default:
                    return ValidationResult.Invalid(String.Format("validator: {0}", execResult));
            }
        }

        private string ParseValidatorOut(MemoryStream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                string s = reader.ReadLine();
                if (String.IsNullOrEmpty(s))
                {
                    return "";
                }
                if (s.StartsWith(CUT_PREFIX))
                {
                    s = s.Substring(CUT_PREFIX.Length);
                }
                if (s.EndsWith(CUT_SUFFIX))
                {
                    s = s.Substring(0, s.Length - CUT_SUFFIX.Length);
                }
                return s;
            }
        }
    }
}
