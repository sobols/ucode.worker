﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UCode.Worker
{
    // public is set for XML serializer

    public class Configuration
    {
        public RunningMode Mode;
        public bool Debug;
        public string MinLogLevel;
        public int NumThreads = 1;
        public double TimeLimitFactor = 1.0;
        public bool RobustTimeLimits = false;
        public ushort HttpServerPort;
        public PublicFileStorageSettings PublicFileStorageSettings;
        public bool UploadOutputs = true;
        public bool KeepAlive = true;

        [XmlArray("Affinity")]
        [XmlArrayItem("Core")]
        public List<int> Cores;

        // paths
        public string RuntimeDirectory;
        public string SandboxDirectory;

        [XmlElement]
        public string[] LanguagesDirectory;
        
        public string TestLibsDirectory;
        public string SelfTestingDataDirectory = "SelfTestingData";

        // mode-specific configs
        public SelfTestingMode.CustomConfiguration SelfTestingModeConfig;
        public IRunnerMode.CustomConfiguration IRunnerModeConfig;
        public ApiMode.CustomConfiguration ApiModeConfiguration;

        // language params
        public List<Param> GlobalLanguageParams;

        public const string DefaultConfigFileName = "config.xml";

        public static Configuration Load(string homeDirectory)
        {
            var path = Path.Combine(homeDirectory, DefaultConfigFileName);
            XmlSerializer configSerializer = new XmlSerializer(typeof(Configuration));
            using (var input = new StreamReader(path))
            {
                Configuration config = (Configuration)configSerializer.Deserialize(input);
                config.Validate(homeDirectory);
                return config;
            }
        }

        private void Validate(string homeDirectory)
        {
            NormalizePath(homeDirectory, ref RuntimeDirectory, false);
            NormalizePath(homeDirectory, ref SandboxDirectory, false);
            for (int i = 0; i < LanguagesDirectory.Length; ++i)
            {
                NormalizePath(homeDirectory, ref LanguagesDirectory[i]);
            }
            NormalizePath(homeDirectory, ref TestLibsDirectory);
            NormalizePath(homeDirectory, ref SelfTestingDataDirectory);

            if (Cores.Count == 0)
            {
                Cores = null;
            }
            else if (Cores.Count != NumThreads)
            {
                throw new MechanismException(String.Format("Invalid affinity configuration: {0} cores expected, {1} found", NumThreads, Cores.Count));
            }
        }

        private static void NormalizePath(string home, ref string path, bool checkExists = true)
        {
            if (String.IsNullOrEmpty(path))
            {
                return;
            }
            path = Path.IsPathRooted(path) ? path : Path.Combine(home, path);
            path = Path.GetFullPath(path);
            if (checkExists && !Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path);
            }
        }
    }

    public class Param
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlText]
        public string Value;
    }

    public class PublicFileStorageSettings
    {
        public string Directory;
        public string URL;
    }
}
