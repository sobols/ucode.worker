﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class DisposableDirectory : IDisposable
    {
        protected readonly string Path_;
        private bool Disposed_;

        public DisposableDirectory(string path)
        {
            Path_ = path;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Disposed_)
            {
                return;
            }

            if (disposing)
            {
                FileSystem.Utils.CompletelyRemoveDirectory(Path_);
            }
            Disposed_ = true;
        }
    }
}
