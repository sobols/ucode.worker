﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace UCode.Worker
{
    public class CrossProcessLockFactory
    {
        private static int DefaultTimoutInMinutes = 10;

        public static IDisposable CreateCrossProcessLock()
        {
            return new ProcessLock(TimeSpan.FromMinutes(DefaultTimoutInMinutes));
        }

        public static IDisposable CreateCrossProcessLock(TimeSpan timespan)
        {
            return new ProcessLock(timespan);
        }
    }

    public class ProcessLock : IDisposable
    {
        // the name of the global mutex;
        private const string MutexName = "Global\\{9DA3017C-DEE1-4E58-9AED-E2D1354C28BF}";

        private Mutex GlobalMutex;

        private bool Owned = false;


        public ProcessLock(TimeSpan timeToWait)
        {
            try
            {
                bool createdNew;

                var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
                var securitySettings = new MutexSecurity();
                securitySettings.AddAccessRule(allowEveryoneRule);
                
                GlobalMutex = new Mutex(true, MutexName, out createdNew, securitySettings);
                if (createdNew)
                {
                    Owned = true;
                }
                else
                {
                    try
                    {
                        Owned = GlobalMutex.WaitOne(0);
                        if (!Owned)
                        {
                            // did not get the mutex, wait for it.
                            Logger.Info("Waiting for mutex...");
                            Owned = GlobalMutex.WaitOne(timeToWait);
                            Logger.Info("Mutex is acquired successfully.");
                        }
                    }
                    catch (AbandonedMutexException)
                    {
                        Owned = true;
                        Logger.Info("Mutex is acquired, but it is likely that previous testing process was aborted. Please check!");
                    }
                }
            }
            catch (Exception)
            {
                Logger.Error("Unable to catch the mutex.");
                throw;
            }
        }

        public void Dispose()
        {
            if (Owned)
            {
                GlobalMutex.ReleaseMutex();
            }
            GlobalMutex = null;
        }
    }
}
