﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UCode.Worker
{
    static class StringUtils
    {
        private static Regex ControlRegex = new Regex(@"\p{Cc}");
        private static UTF8Encoding StrictUTF = new UTF8Encoding(false, true);

        private static string DecodeSafe(byte[] blob)
        {
            try
            {
                return StrictUTF.GetString(blob);
            }
            catch (DecoderFallbackException)
            {
                return Encoding.GetEncoding(1251).GetString(blob);
            }
        }

        private static string CharEscaper(Match m)
        {
            char c = m.Value[0];
            switch (c)
            {
                case '\0':
                    return @"\0";
                case '\n':
                    return @"\n";
                case '\r':
                    return @"\r";
                case '\t':
                    return @"\t";
            }
            return String.Format(@"\x{0:X2}", (byte)c);
        }

        public static string Escape(string s)
        {
            return ControlRegex.Replace(s, CharEscaper);
        }

        public static string BlobToStringSafe(byte[] blob)
        {
            string s = DecodeSafe(blob);
            return Escape(s);
        }

        public class PointResult
        {
            public double Points;
            public string Message;
        }

        public static PointResult TryExtractPoints(string message)
        {
            var tokens = message.Split(new Char[] { ' ' }, 2);
            double points;
            if ((tokens.Length == 1 || tokens.Length == 2) && DoubleIO.TryFromString(tokens[0], out points))
            {
                return new PointResult { Points = points, Message = tokens.Length > 1 ? tokens[1] : String.Empty };
            }
            return null;
        }

        public static bool IsValidUTF8(byte[] blob)
        {
            try
            {
                StrictUTF.GetCharCount(blob);
                return true;
            }
            catch (DecoderFallbackException)
            {
                return false;
            }
        }
    }

    static class DoubleIO
    {
        public static string ToString(double value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public static bool TryFromString(string str, out double result)
        {
            return Double.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out result);
        }
    }
}
