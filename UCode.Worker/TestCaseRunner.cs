﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UCode.Worker.Messages;
using static System.Net.Mime.MediaTypeNames;
using static UCode.Worker.SeqNumericFileNameStorage;

namespace UCode.Worker
{
    internal class TestCaseRunner
    {
        private readonly IExecutionMachine ExecMachine_;
        private readonly ISingleFileStorage StorageForProblemOutputs_;
        private readonly TestCase Test_;
        private readonly string InputFileName_;
        private readonly string OutputFileName_;
        private readonly CompiledProgram Solution_;
        private readonly IChecker Checker_;
        private readonly IInteractor Interactor_;
        private readonly bool RunTwice_;
        private readonly bool StoreOutputs_;
        private readonly int? CpuCore_;

        private long TimeUsed_ = 0;
        private long MemoryUsed_ = 0;
        private int ExitCode_ = 0;
        private MultiMessage CheckerMessage_ = new MultiMessage();
        private Score Score_ = Score.None();

        private IFileLocation Output_ = null;
        private IFileLocation StdOut_ = null;
        private IFileLocation StdErr_ = null;

        public TestCaseRunner(IExecutionMachine execMachine, ISingleFileStorage storage, TestCase test,
                              string inputFileName, string outputFileName,
                              CompiledProgram solution, IChecker checker, IInteractor interactor,
                              bool runTwice, bool storeOutputs, int? cpuCore)
        {
            ExecMachine_ = execMachine;
            StorageForProblemOutputs_ = storage;
            Test_ = test;
            InputFileName_ = inputFileName;
            OutputFileName_ = outputFileName;
            Solution_ = solution;
            Checker_ = checker;
            Interactor_ = interactor;
            RunTwice_ = runTwice;
            StoreOutputs_ = storeOutputs;
            CpuCore_ = cpuCore;
        }

        public TestCaseResult Run(CancellationToken token)
        {
            TestCaseResult result = null;

            // Stage 1
            result = (Interactor_ != null) ? RunInteractive(token) : RunNonInteractive(token);
            if (result != null)
            {
                return result;
            }

            // Stage 2 (optional)
            if (RunTwice_)
            {
                result = RunSecondTime(token);
                if (result != null)
                {
                    return result;
                }
            }

            // Stage 3
            return RunChecker(token);
        }

        private TestCaseResult RunNonInteractive(CancellationToken token)
        {
            var settings = MakeSettings();
            var solutionCommand = MakeSolutionExecutionCommand();

            PrepareInput(settings, solutionCommand, GetHandle(Test_.Input));
            var outputDestinations = PrepareOutput(settings, solutionCommand);

            var execResult = ExecMachine_.Run(settings, solutionCommand, token);

            SaveSolutionExecutionResult(execResult);
            Output_ = GetHandle(outputDestinations.Output);
            StdOut_ = GetHandle(outputDestinations.StdOut);
            StdErr_ = GetHandle(outputDestinations.StdErr);

            if (execResult.Outcome != ExecutionResult.OutcomeEnum.OK)
            {
                return MakeTestCaseResult(execResult.Outcome.ToTestCaseOutcome());
            }
            return null;
        }
        private TestCaseResult RunInteractive(CancellationToken token)
        {
            var settings = MakeSettings();
            var solutionCommand = MakeSolutionExecutionCommand();

            // Solution stdout goes to interactor stdin
            var solutionStdErr = MakeOutputDestination();
            solutionCommand.StdErr = solutionStdErr;

            using (var inputToken = Test_.Input.Handle.MakeReadable())
            {
                using (var answerToken = Test_.Answer.Handle.MakeReadable())
                {
                    var interactorOutput = MakeOutputDestination();
                    var interaction = Interactor_.NewInteraction(inputToken.Path, interactorOutput, answerToken.Path);
                    interaction.PrepareInteractiveExecution(settings);
                    var interactorCommand = interaction.GetInteractorCommand();

                    var result = ExecMachine_.RunInteractive(settings, solutionCommand, interactorCommand, token);

                    SaveSolutionExecutionResult(result.Solution);
                    Output_ = interactorOutput.Handle;
                    StdErr_ = solutionStdErr.Handle;

                    // https://codeforces.com/blog/entry/5152
                    foreach (var program in result.Order)
                    {
                        switch (program)
                        {
                            case InteractiveExecutionResult.FirstTerminatedEnum.Solution:
                                if (result.Solution.Outcome != ExecutionResult.OutcomeEnum.OK)
                                {
                                    return MakeTestCaseResult(result.Solution.Outcome.ToTestCaseOutcome());
                                }
                                break;
                            case InteractiveExecutionResult.FirstTerminatedEnum.Interactor:
                                var checkingResult = interaction.GetResult(result.Interactor);
                                SaveCheckingResult(checkingResult);
                                if (checkingResult.Outcome != CheckerOutcome.OK)
                                {
                                    return MakeTestCaseResult(checkingResult);
                                }
                                break;
                        }
                    }
                }
            }
            return null;
        }
        private TestCaseResult RunSecondTime(CancellationToken token)
        {
            var settings = MakeSettings();
            var solutionCommand = MakeSolutionExecutionCommand();

            PrepareInput(settings, solutionCommand, Output_);
            var outputDestinations = PrepareOutput(settings, solutionCommand);

            var execResult = ExecMachine_.Run(settings, solutionCommand, token);

            SaveSolutionExecutionResult(execResult);
            Output_ = GetHandle(outputDestinations.Output);
            StdOut_ = Merge(StdOut_, GetHandle(outputDestinations.StdOut));
            StdErr_ = Merge(StdErr_, GetHandle(outputDestinations.StdErr));

            if (execResult.Outcome != ExecutionResult.OutcomeEnum.OK)
            {
                return MakeTestCaseResult(execResult.Outcome.ToTestCaseOutcome());
            }
            return null;
        }
        private TestCaseResult RunChecker(CancellationToken token)
        {
            var handles = new CheckingHandles(GetHandle(Test_.Input), Output_, GetHandle(Test_.Answer));
            var checkingResult = Checker_.Check(handles, CpuCore_, token, Test_.MaxScore);
            SaveCheckingResult(checkingResult);

            // replace Ok with checker output
            return MakeTestCaseResult(checkingResult);
        }
        private void SaveSolutionExecutionResult(ExecutionResult execResult)
        {
            TimeUsed_ = Math.Max(TimeUsed_, execResult.TimeUsed);
            MemoryUsed_ = Math.Max(MemoryUsed_, execResult.MemoryUsed);
            if (ExitCode_ == 0)
            {
                ExitCode_ = execResult.ExitCode;
            }
            if (execResult.Outcome != ExecutionResult.OutcomeEnum.OK)
            {
                // Solution has failed
                Score_ = Score.Min();
            }
        }
        private void SaveCheckingResult(CheckingResult checkingResult)
        {
            CheckerMessage_.Add(checkingResult.Message);
            Score_ = Score.Merge(Score_, checkingResult.Score);
        }
        private TestCaseResult MakeTestCaseResult(CheckingResult checkingResult)
        {
            return MakeTestCaseResult(
                checkingResult.Outcome.ToTestCaseOutcome(),
                checkingResult.FailReason
            );
        }
        private TestCaseResult MakeTestCaseResult(
            TestCaseOutcome outcome,
            CheckFailedReason failReason = CheckFailedReason.Unknown)
        {
            return new TestCaseResult(
                Test_.ID,
                outcome,
                failReason,
                TimeUsed_,
                MemoryUsed_,
                ExitCode_,
                Score_.Get(Test_.MaxScore),
                CheckerMessage_,
                GetHandle(Test_.Input),
                StoreOutputs_ ? Output_ : null,
                GetHandle(Test_.Answer),
                StoreOutputs_ ? StdOut_ : null,
                StoreOutputs_ ? StdErr_ : null);
        }
        private ExecutionSettings MakeSettings()
        {
            var settings = new ExecutionSettings();
            settings.CpuCore = CpuCore_;
            return settings;
        }
        private ExecutionCommand MakeSolutionExecutionCommand()
        {
            var command = new ExecutionCommand(Solution_);
            // limits
            command.TimeLimit = Test_.TimeLimit;
            command.MemoryLimit = Test_.MemoryLimit;
            return command;
        }
        private OneFileStorageMoveDataDestination MakeOutputDestination()
        {
            return new OneFileStorageMoveDataDestination(StorageForProblemOutputs_);
        }
        static private IFileLocation GetHandle(TestCaseFile file)
        {
            return (file != null) ? file.Handle : null;
        }
        static private IFileLocation GetHandle(OneFileStorageMoveDataDestination dataDestination)
        {
            return (dataDestination != null) ? dataDestination.Handle : null;
        }
        private void PrepareInput(ExecutionSettings settings, ExecutionCommand solutionCommand, IFileLocation inputFileLocation)
        {
            if (inputFileLocation != null)
            {
                if (!String.IsNullOrEmpty(InputFileName_))
                {
                    settings.InputFiles[InputFileName_] = inputFileLocation.GetDataSource();
                }
                else
                {
                    solutionCommand.StdIn = inputFileLocation.GetDataSource();
                }
            }
        }

        private class OutDestinations
        {
            public OneFileStorageMoveDataDestination Output;
            public OneFileStorageMoveDataDestination StdOut;
            public OneFileStorageMoveDataDestination StdErr;
        }
        private OutDestinations PrepareOutput(ExecutionSettings settings, ExecutionCommand solutionCommand)
        {
            var outDestinations = new OutDestinations();
            solutionCommand.StdErr = outDestinations.StdErr = MakeOutputDestination();
            if (!String.IsNullOrEmpty(OutputFileName_))
            {
                settings.OutputFiles[OutputFileName_] = outDestinations.Output = MakeOutputDestination();
                solutionCommand.StdOut = outDestinations.StdOut = MakeOutputDestination();
            }
            else
            {
                solutionCommand.StdOut = outDestinations.Output = MakeOutputDestination();
            }
            return outDestinations;
        }
        private IFileLocation Merge(IFileLocation first, IFileLocation second)
        {
            if (IsNullOrEmptyFile(second))
            {
                return first;
            }
            if (IsNullOrEmptyFile(first))
            {
                return second;
            }
            byte[] data = null;
            using (var stream = new MemoryStream())
            {
                using (var out1 = first.OpenRead())
                {
                    out1.CopyTo(stream);
                }
                const int bufferSize = 1024;
                using (var sw = new StreamWriter(stream, Encoding.ASCII, bufferSize, /*leaveOpen=*/true))
                {
                    sw.WriteLine("===");
                    sw.Flush();
                }
                using (var out2 = second.OpenRead())
                {
                    out2.CopyTo(stream);
                }
                stream.Flush();
                data = stream.ToArray();
            }
            return StorageForProblemOutputs_.Put(new MemoryDataSource(data));
        }
        private static bool IsNullOrEmptyFile(IFileLocation location)
        {
            return location == null || location.Size == 0;
        }
    }
}
