﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class TempDir : DisposableDirectory
    {
        public TempDir(string path)
            : base(path)
        {
        }

        public string Path
        {
            get { return Path_; }
        }
    }

    interface ITempDirFactory
    {
        TempDir MakeTempDir();
    }
}
