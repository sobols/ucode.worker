﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    interface IFileLocation
    {
        FileStream OpenRead();
        ReadAccessToken MakeReadable();
        void CopyTo(string destination);
        IDataSource GetDataSource();
        long Size { get; }
        void Trim(long maxSize);
    }

    abstract class FileLocationBase : IFileLocation
    {
        protected abstract string Path_
        {
            get;
        }

        public FileStream OpenRead()
        {
            return File.OpenRead(Path_);
        }

        public ReadAccessToken MakeReadable()
        {
            return new ReadAccessToken(Path_);
        }

        public void CopyTo(string destination)
        {
            File.Copy(Path_, destination);
        }

        public IDataSource GetDataSource()
        {
            return new CopyOtherFileDataSource(Path_);
        }

        public long Size
        {
            get { return new FileInfo(Path_).Length; }
        }

        public override string ToString()
        {
            string fullPath = Path_;
            return (fullPath == null) ? "[null]" : Path.GetFileName(fullPath);
        }

        public void Trim(long maxSize)
        {
            using (var fs = new FileStream(Path_, FileMode.Open))
            {
                if (fs.Length > maxSize)
                {
                    fs.SetLength(maxSize);
                }
            }
        }
    }

    class FileLocation : FileLocationBase
    {
        private readonly string FilePath_;

        public FileLocation(string path)
        {
            FilePath_ = path;
        }

        protected override string Path_
        {
            get { return FilePath_; }
        }
    }
}
