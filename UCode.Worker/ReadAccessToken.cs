﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class ReadAccessToken : IDisposable
    {
        private string Path_;

        public ReadAccessToken(string path)
        {
            Path_ = path;
        }

        public string Path
        {
            get { return Path_; }
        }

        public void Dispose()
        {
        }
    }
}
