﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class ExecutionSettings
    {
        public readonly Dictionary<string, IDataSource> InputFiles;
        public readonly Dictionary<string, IDataDestination> OutputFiles;

        public int? CpuCore;

        public ExecutionSettings()
        {
            InputFiles = new Dictionary<string, IDataSource>();
            OutputFiles = new Dictionary<string, IDataDestination>();
        }
    }

    class ExecutionCommand
    {
        public long? TimeLimit;		// in milliseconds
        public long? MemoryLimit;	// in bytes

        public readonly CompiledProgram Program;
        public string RawCommandLine;
        public readonly List<string> CommandLineArgs;

        public IDataSource StdIn;
        public IDataDestination StdOut;
        public IDataDestination StdErr;

        public ExecutionCommand(CompiledProgram program)
        {
            Program = program;
            CommandLineArgs = new List<string>();
        }
    }
}
