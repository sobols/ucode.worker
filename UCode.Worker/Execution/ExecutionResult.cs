﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker
{
    class ExecutionResult
    {
        public enum OutcomeEnum
        {
            // Note: Ok in InvocationOutcome does not mean Ok in TestCaseOutcome,
            // because, ex., checker may judge the given answer as wrong.
            OK,
            TimeLimitExceeded,
            MemoryLimitExceeded,
            IdlenessLimitExceeded,
            RuntimeError,
            SecurityViolation,
        }

        public OutcomeEnum Outcome;
        public readonly long TimeUsed;
        public readonly long MemoryUsed;
        public readonly int ExitCode;

        public bool OK
        {
            get
            {
                return Outcome == OutcomeEnum.OK;
            }
        }

        public ExecutionResult(CliRunLib2.Result runResult)
        {
            TimeUsed = (long)runResult.TimeUsed;
            MemoryUsed = (long)runResult.MemoryUsed;
            ExitCode = (int)runResult.ExitCode;

            switch (runResult.Outcome)
            {
                case CliRunLib2.OutcomeEnum.Ok:
                    Outcome = OutcomeEnum.OK;
                    break;
                case CliRunLib2.OutcomeEnum.RuntimeError:
                    Outcome = OutcomeEnum.RuntimeError;
                    break;
                case CliRunLib2.OutcomeEnum.TimeLimitExceeded:
                    Outcome = OutcomeEnum.TimeLimitExceeded;
                    break;
                case CliRunLib2.OutcomeEnum.MemoryLimitExceeded:
                    Outcome = OutcomeEnum.MemoryLimitExceeded;
                    break;
                case CliRunLib2.OutcomeEnum.IdlenessLimitExceeded:
                    Outcome = OutcomeEnum.IdlenessLimitExceeded;
                    break;
                case CliRunLib2.OutcomeEnum.SecurityViolation:
                    Outcome = OutcomeEnum.SecurityViolation;
                    break;
                default:
                    var excMsg = String.Format("Execution aborted with unexpected result:\n{0}", runResult);
                    throw new MechanismException(excMsg);
            }
        }

        public override string ToString()
        {
            var result = Util.SplitPascalStringToWords(Outcome.ToString());
            switch (Outcome)
            {
                case OutcomeEnum.RuntimeError:
                    result += String.Format(" (exit code {0})", ExitCode);
                    break;
                case OutcomeEnum.IdlenessLimitExceeded:
                case OutcomeEnum.TimeLimitExceeded:
                    result += String.Format(" ({0} ms)", TimeUsed);
                    break;
                case OutcomeEnum.MemoryLimitExceeded:
                    result += String.Format(" ({0:0.0} MB)", Util.BytesToMegabytes(MemoryUsed));
                    break;
            }
            return result;
        }
    }

    class InteractiveExecutionResult
    {
        public enum FirstTerminatedEnum
        {
            Solution,
            Interactor,
        }
        public ExecutionResult Solution;
        public ExecutionResult Interactor;
        public FirstTerminatedEnum[] Order;

        public InteractiveExecutionResult(CliRunLib2.Result solutionResult, CliRunLib2.Result interactorResult, FirstTerminatedEnum[] order)
        {
            Solution = new ExecutionResult(solutionResult);
            Interactor = new ExecutionResult(interactorResult);
            Order = order;
        }
    }

    static class ExecutionResultExtensions
    {
        static public TestCaseOutcome ToTestCaseOutcome(this ExecutionResult.OutcomeEnum outcome)
        {
            switch (outcome)
            {
                case ExecutionResult.OutcomeEnum.OK:
                    return TestCaseOutcome.OK;
                case ExecutionResult.OutcomeEnum.IdlenessLimitExceeded:
                    return TestCaseOutcome.IdlenessLimitExceeded;
                case ExecutionResult.OutcomeEnum.MemoryLimitExceeded:
                    return TestCaseOutcome.MemoryLimitExceeded;
                case ExecutionResult.OutcomeEnum.RuntimeError:
                    return TestCaseOutcome.RuntimeError;
                case ExecutionResult.OutcomeEnum.SecurityViolation:
                    return TestCaseOutcome.SecurityViolation;
                case ExecutionResult.OutcomeEnum.TimeLimitExceeded:
                    return TestCaseOutcome.TimeLimitExceeded;
                default:
                    throw new InvalidCastException();
            }
        }
    }
}
