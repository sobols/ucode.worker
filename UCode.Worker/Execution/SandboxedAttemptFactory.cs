﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.Execution
{
    abstract class SandboxedAttemptFactory : IAttemptFactory, IDisposable
    {
        private readonly ITempDirFactory TempDirFactory_;
        private readonly List<SandboxedAttempt> Attempts_;

        public SandboxedAttemptFactory(ITempDirFactory tempDirFactory)
        {
            TempDirFactory_ = tempDirFactory;
            Attempts_ = new List<SandboxedAttempt>();
        }
        public IAttempt Run(long? timeLimit, CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            TempDir sandbox = TempDirFactory_.MakeTempDir();
            SandboxedAttempt attempt = RunImpl(sandbox, timeLimit, token);
            Attempts_.Add(attempt);
            return attempt;
        }
        public abstract SandboxedAttempt RunImpl(TempDir sandbox, long? timeLimit, CancellationToken token);
        public void Dispose()
        {
            var exceptions = new List<Exception>();
            foreach (var item in Attempts_)
            {
                try
                {
                    item.Dispose();
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }
            if (exceptions.Count != 0)
            {
                throw new AggregateException(exceptions);
            }
        }
    }
    abstract class SandboxedAttempt : IAttempt, IDisposable
    {
        private readonly TempDir Sandbox_;

        protected SandboxedAttempt(TempDir sandbox)
        {
            Sandbox_ = sandbox;
            Logger.Debug("Sandbox dir for execution: '{0}'", sandbox.Path);
        }
        protected void PutFilesToSandbox(Dictionary<string, IDataSource> inputFiles)
        {
            foreach (var pair in inputFiles)
            {
                pair.Value.SaveToFile(Path.Combine(Sandbox_.Path, pair.Key));
            }
        }
        protected void PutInputToSandbox(IDataSource source, string fullPath)
        {
            if (source != null)
            {
                source.SaveToFile(fullPath);
            }
        }
        protected void GetFilesFromSandbox(Dictionary<string, IDataDestination> outputFiles)
        {
            foreach (var pair in outputFiles)
            {
                var fullPath = Path.Combine(Sandbox_.Path, pair.Key);
                if (FileSystem.Security.SanitizeFileAccessIfExists(fullPath))
                {
                    pair.Value.LoadFromFile(fullPath);
                }
            }
        }
        protected void GetOutputFromSandbox(IDataDestination destination, string fullPath)
        {
            if (destination != null)
            {
                if (FileSystem.Security.SanitizeFileAccessIfExists(fullPath))
                {
                    destination.LoadFromFile(fullPath);
                }
            }
        }
        public void Dispose()
        {
            Sandbox_.Dispose();
        }
        public abstract long GetTimeUsed();
        public abstract bool IsTimeLimitExceeded();
        public abstract void SetTimeLimitExceeded();
    }
}
