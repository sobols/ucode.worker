﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCode.Worker.SelfTestingMode;

namespace UCode.Worker.Execution
{
    internal class PreparedCommand
    {
        public ProgrammingLanguage Language;
        public StringFormatter Formatter;
        public string FullCommandLine;
        public long? MemoryLimit;

        public PreparedCommand(ExecutionCommand command, ProgrammingLanguageContainer progLangs)
        {
            var program = command.Program;
            if (program.LangID != null)
            {
                Language = progLangs.Get(program.LangID);
                Formatter = CreateFormatter(progLangs, program, Language);
            }
            FullCommandLine = MakeCommandLine(program, Language, Formatter, command.RawCommandLine, command.CommandLineArgs);
            MemoryLimit = command.MemoryLimit;
        }

        private static StringFormatter CreateFormatter(ProgrammingLanguageContainer progLangs, CompiledProgram program, ProgrammingLanguage language)
        {
            var formatterArgs = new Dictionary<string, string>() {
                    { "title" , program.Title },
                    { "location" , program.BaseDirectory },
                    { "home", language.HomeDirectory },
                    { "common", progLangs.CommonLibsDirectory },
                    { "windows", Environment.GetFolderPath(Environment.SpecialFolder.Windows) }
                };
            return new StringFormatter(formatterArgs);
        }
        private static string MakeCommandLine(CompiledProgram program, ProgrammingLanguage language, StringFormatter formatter, string rawCommandLine, List<string> extraArgs)
        {
            string fullCommandLine;
            if (language != null)
            {
                fullCommandLine = formatter.Format(language.RunCmd);
            }
            else
            {
                // directly run exe that was externally compiled
                fullCommandLine = String.Format("{0}\\{1}.exe", program.BaseDirectory, program.Title);
            }
            if (rawCommandLine != null)
            {
                fullCommandLine += " " + rawCommandLine;
            }
            if (extraArgs.Count > 0)
            {
                fullCommandLine += " " + ShLex.Join(extraArgs);
            }
            return fullCommandLine;
        }
    }
}
