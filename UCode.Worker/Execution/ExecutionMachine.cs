﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UCode.Worker.Execution;
using static UCode.Worker.InteractiveExecutionResult;

namespace UCode.Worker
{
    interface IExecutionMachine
    {
        ExecutionResult Run(ExecutionSettings settings, ExecutionCommand command, CancellationToken token);
        InteractiveExecutionResult RunInteractive(ExecutionSettings settings, ExecutionCommand solution, ExecutionCommand interactor, CancellationToken token);
    }
    class ExecutionMachine : IExecutionMachine
    {
        private ProgrammingLanguageContainer ProgLangs_;
        private ITempDirFactory TempDirFactory_;
        private TimeLimitScaler TimeLimitScaler_;
        private bool RobustTimeLimits_;

        public ExecutionMachine(ProgrammingLanguageContainer progLangs, ITempDirFactory tempDirFactory, double timeLimitFactor, bool robustTimeLimits)
        {
            ProgLangs_ = progLangs;
            TempDirFactory_ = tempDirFactory;
            TimeLimitScaler_ = new TimeLimitScaler(timeLimitFactor);
            RobustTimeLimits_ = robustTimeLimits;
        }

        public ExecutionResult Run(ExecutionSettings settings, ExecutionCommand command, CancellationToken token)
        {
            using (var factory = new AttemptFactory(ProgLangs_, TempDirFactory_, TimeLimitScaler_, settings, command))
            {
                var attempt = RobustTimeLimitRunner.Run(factory, command.TimeLimit, RobustTimeLimits_, token) as AttemptFactory.Attempt;
                return attempt.FormResult(settings, command);
            }
        }

        public InteractiveExecutionResult RunInteractive(ExecutionSettings settings, ExecutionCommand solution, ExecutionCommand interactor, CancellationToken token)
        {
            using (var factory = new AttemptFactoryInteractive(ProgLangs_, TempDirFactory_, TimeLimitScaler_, settings, solution, interactor))
            {
                var attempt = RobustTimeLimitRunner.Run(factory, solution.TimeLimit, RobustTimeLimits_, token) as AttemptFactoryInteractive.Attempt;
                return attempt.FormResult(settings, solution, interactor);
            }
        }

        abstract class AttemptFactoryBase : SandboxedAttemptFactory
        {
            protected TimeLimitScaler TimeLimitScaler_;
            protected Dictionary<string, IDataSource> InputFiles_;
            protected int? CpuCore_;

            public AttemptFactoryBase(ITempDirFactory tempDirFactory, TimeLimitScaler timeLimitScaler,
                                      ExecutionSettings settings)
                : base(tempDirFactory)
            {
                TimeLimitScaler_ = timeLimitScaler;
                InputFiles_ = settings.InputFiles;
                CpuCore_ = settings.CpuCore;
            }
            protected CliRunLib2.Command CreateRunLibCommand(PreparedCommand preparedCommand, TempDir sandbox, long? timeLimit, bool setAffinity = true)
            {
                var command = new CliRunLib2.Command(preparedCommand.FullCommandLine);
                try
                {
                    command.CurrentDirectory = sandbox.Path;
                    command.TimeLimit = TimeLimitScaler_.GetActualTimeLimit(timeLimit);
                    command.MemoryLimit = (ulong?)preparedCommand.MemoryLimit;
                    command.MemoryAccountingMode = (preparedCommand.Language != null && preparedCommand.Language.MeasureWorkingSet)
                        ? CliRunLib2.MemoryAccountingModeEnum.WorkingSetSize
                        : CliRunLib2.MemoryAccountingModeEnum.CommitCharge;
                    command.IdlenessLimit = true;
                    command.ProcessLimit = 1;
                    command.LimitAccountingMode = CliRunLib2.LimitAccountingModeEnum.PerProcess;
                    command.ProcessPriority = CliRunLib2.ProcessPriorityEnum.High;
                    if (setAffinity && CpuCore_.HasValue)
                    {
                        command.AffinityMask = 1UL << CpuCore_.Value;
                    }

                    // Env vars
                    command.IsEnvironmentOverridden = true;
                    if (preparedCommand.Language != null)
                    {
                        foreach (var kv in preparedCommand.Language.EnvVarSettings)
                        {
                            command.AddEnvironmentVariable(kv.Name, preparedCommand.Formatter.Format(kv.Value));
                        }
                    }
                    return command;
                }
                catch
                {
                    command.Dispose();
                    throw;
                }
            }
            protected CliRunLib2.Result AdjustTimeUsed(CliRunLib2.Result runResult)
            {
                runResult.TimeUsed = TimeLimitScaler_.GetEffectiveTimeUsed(runResult.TimeUsed);
                return runResult;
            }
        }

        class AttemptFactory : AttemptFactoryBase
        {
            private IDataSource StdIn_;
            private bool NeedStdOut_;
            private bool NeedStdErr_;
            protected PreparedCommand Command_;

            public AttemptFactory(ProgrammingLanguageContainer progLangs, ITempDirFactory tempDirFactory, TimeLimitScaler timeLimitScaler,
                                  ExecutionSettings settings, ExecutionCommand mainCommand)
                : base(tempDirFactory, timeLimitScaler, settings)
            {
                StdIn_ = mainCommand.StdIn;
                NeedStdOut_ = mainCommand.StdOut != null;
                NeedStdErr_ = mainCommand.StdErr != null;
                Command_ = new PreparedCommand(mainCommand, progLangs);
            }
            public override SandboxedAttempt RunImpl(TempDir sandbox, long? timeLimit, CancellationToken token)
            {
                return new Attempt(this, sandbox, timeLimit, token);
            }

            public class Attempt: SandboxedAttempt
            {
                private string StdInFile_;
                private string StdOutFile_;
                private string StdErrFile_;
                private CliRunLib2.Result RunResult;

                public Attempt(AttemptFactory factory, TempDir sandbox, long? timeLimit, CancellationToken token) : base(sandbox)
                {
                    string sandboxPath = sandbox.Path;
                    StdInFile_ = (factory.StdIn_ != null) ? Path.Combine(sandboxPath, Path.GetRandomFileName()) : null;
                    StdOutFile_ = factory.NeedStdOut_ ? Path.Combine(sandboxPath, Path.GetRandomFileName()) : null;
                    StdErrFile_ = factory.NeedStdErr_ ? Path.Combine(sandboxPath, Path.GetRandomFileName()) : null;

                    PutFilesToSandbox(factory.InputFiles_);
                    PutInputToSandbox(factory.StdIn_, StdInFile_);

                    using (var command = factory.CreateRunLibCommand(factory.Command_, sandbox, timeLimit))
                    {
                        command.RedirectStdInputFrom(StdInFile_);
                        command.RedirectStdOutputTo(StdOutFile_);
                        command.RedirectStdErrorTo(StdErrFile_);

                        RunResult = CliRunLib2.Runner.Run(command, token);

                        RunResult = factory.AdjustTimeUsed(RunResult);
                    }
                }
                public ExecutionResult FormResult(ExecutionSettings settings, ExecutionCommand command)
                {
                    GetFilesFromSandbox(settings.OutputFiles);
                    GetOutputFromSandbox(command.StdOut, StdOutFile_);
                    GetOutputFromSandbox(command.StdErr, StdErrFile_);
                    return new ExecutionResult(RunResult);
                }
                public override long GetTimeUsed()
                {
                    return (long)RunResult.TimeUsed;
                }
                public override bool IsTimeLimitExceeded()
                {
                    return RunResult.Outcome == CliRunLib2.OutcomeEnum.TimeLimitExceeded;
                }
                public override void SetTimeLimitExceeded()
                {
                    RunResult.Outcome = CliRunLib2.OutcomeEnum.TimeLimitExceeded;
                }
            }
        }

        class AttemptFactoryInteractive : AttemptFactoryBase
        {
            private bool NeedSolutionErr_;
            private bool NeedInteractorErr_;
            private PreparedCommand SolutionCommand_;
            private PreparedCommand InteractorCommand_;
            private long? InteractorTimeLimit_;

            public AttemptFactoryInteractive(ProgrammingLanguageContainer progLangs, ITempDirFactory tempDirFactory, TimeLimitScaler timeLimitScaler,
                                             ExecutionSettings settings, ExecutionCommand solutionCommand, ExecutionCommand interactorCommand)
                : base(tempDirFactory, timeLimitScaler, settings)
            {
                NeedSolutionErr_ = solutionCommand.StdErr != null;
                NeedInteractorErr_ = interactorCommand.StdErr != null;
                SolutionCommand_ = new PreparedCommand(solutionCommand, progLangs);
                InteractorCommand_ = new PreparedCommand(interactorCommand, progLangs);
                InteractorTimeLimit_ = interactorCommand.TimeLimit; // not changed via `robust TL` logic
            }
            public override SandboxedAttempt RunImpl(TempDir sandbox, long? timeLimit, CancellationToken token)
            {
                return new Attempt(this, sandbox, timeLimit, token);
            }

            public class Attempt : SandboxedAttempt
            {
                private string SolutionStdErrFile_;
                private string InteractorStdErrFile_;
                private CliRunLib2.Result SolutionRunResult;
                private CliRunLib2.Result InteractorRunResult;
                private FirstTerminatedEnum[] Order;

                public Attempt(AttemptFactoryInteractive factory, TempDir sandbox, long? timeLimit, CancellationToken token) : base(sandbox)
                {
                    string sandboxPath = sandbox.Path;
                    SolutionStdErrFile_ = factory.NeedSolutionErr_ ? Path.Combine(sandboxPath, Path.GetRandomFileName()) : null;
                    InteractorStdErrFile_ = factory.NeedInteractorErr_ ? Path.Combine(sandboxPath, Path.GetRandomFileName()) : null;

                    using (var solutionCommand = factory.CreateRunLibCommand(factory.SolutionCommand_, sandbox, timeLimit))
                    {
                        using (var interactorCommand = factory.CreateRunLibCommand(factory.InteractorCommand_, sandbox, factory.InteractorTimeLimit_))
                        {
                            solutionCommand.RedirectStdErrorTo(SolutionStdErrFile_);
                            interactorCommand.RedirectStdErrorTo(InteractorStdErrFile_);
                            // solutionCommand.StdInput is internally connected to interactorCommand.StdOutput
                            // solutionCommand.StdOutput is internally connected to interactorCommand.StdInput

                            var commands = new CliRunLib2.Command[] { solutionCommand, interactorCommand };

                            CliRunLib2.PipelineResult result = CliRunLib2.Runner.RunPipeline(commands, /*loop=*/true, token);

                            SolutionRunResult = factory.AdjustTimeUsed(result.Results[0]);
                            InteractorRunResult = factory.AdjustTimeUsed(result.Results[1]);
                            Order = result.OrderOfTermination.Select(idx => idx == 0 ? FirstTerminatedEnum.Solution : FirstTerminatedEnum.Interactor).ToArray();
                        }
                    }
                }
                public InteractiveExecutionResult FormResult(ExecutionSettings settings, ExecutionCommand solution, ExecutionCommand interactor)
                {
                    GetFilesFromSandbox(settings.OutputFiles);
                    GetOutputFromSandbox(solution.StdErr, SolutionStdErrFile_);
                    GetOutputFromSandbox(interactor.StdErr, InteractorStdErrFile_);
                    return new InteractiveExecutionResult(SolutionRunResult, InteractorRunResult, Order);
                }
                public override long GetTimeUsed()
                {
                    return (long)SolutionRunResult.TimeUsed;
                }
                public override bool IsTimeLimitExceeded()
                {
                    return SolutionRunResult.Outcome == CliRunLib2.OutcomeEnum.TimeLimitExceeded;
                }
                public override void SetTimeLimitExceeded()
                {
                    SolutionRunResult.Outcome = CliRunLib2.OutcomeEnum.TimeLimitExceeded;
                }
            }
        }
    }
}
