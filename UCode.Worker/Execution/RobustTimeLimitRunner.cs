﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.Execution
{
    interface IAttempt
    {
        long GetTimeUsed();
        bool IsTimeLimitExceeded();
        void SetTimeLimitExceeded();
    }
    interface IAttemptFactory
    {
        IAttempt Run(long? timeLimit, CancellationToken token);
    }

    static class RobustTimeLimitRunner
    {
        public static IAttempt Run(IAttemptFactory factory, long? timeLimit, bool robustTimeLimits, CancellationToken token)
        {
            if (!robustTimeLimits || !timeLimit.HasValue)
            {
                // Use original TL as is
                return factory.Run(timeLimit, token);
            }

            long requestedTimeLimit = timeLimit.Value;
            long robustTimeLimit = requestedTimeLimit + (requestedTimeLimit / 10); // +10%

            var result = factory.Run(robustTimeLimit, token);
            if (
                result.IsTimeLimitExceeded() ||                    // It's TLE even with an increased TL: no chance
                PassesTL(result.GetTimeUsed(), requestedTimeLimit) // No matter whether the TL was increased or not
            )
            {
                return result;
            }

            Logger.Warning("Robust TL: took {0} ms, running again", result.GetTimeUsed());

            for (int attempt = 1; attempt <= 2; ++attempt)
            {
                var current = factory.Run(robustTimeLimit, token);
                Logger.Warning("Robust TL: extra attempt #{0} took {1} ms", attempt, result.GetTimeUsed());
                if (PassesTL(current.GetTimeUsed(), requestedTimeLimit))
                {
                    return current;
                }
                if (current.GetTimeUsed() < result.GetTimeUsed())
                {
                    result = current; // choose faster attempt
                }
            }
            result.SetTimeLimitExceeded();
            return result;
        }
        private static bool PassesTL(long timeUsed, long requestedTimeLimit)
        {
            return timeUsed <= requestedTimeLimit;
        }
    }

    class TimeLimitScaler
    {
        double Factor_;

        public TimeLimitScaler(double factor = 1.0)
        {
            Factor_ = factor;
        }
        public ulong? GetActualTimeLimit(long? timeLimit)
        {
            return timeLimit.HasValue ? (ulong?)(timeLimit.Value * Factor_) : null;
        }
        public ulong GetEffectiveTimeUsed(ulong timeUsed)
        {
            return (ulong)(timeUsed / Factor_);
        }
    }
}
