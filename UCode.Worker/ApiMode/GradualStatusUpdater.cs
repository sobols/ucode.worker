﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class GradualStatusUpdater : IDisposable
    {
        const int INTERVAL = 1000; // ms

        Thread Thread_;
        bool ShouldStop_ = false;
        string CurrentJsonStatus_ = null;
        readonly object Sync_ = new object();
        readonly HttpApiClient Client_;
        readonly long JobId_;

        public GradualStatusUpdater(HttpApiClient client, long jobId)
        {
            Client_ = client;
            JobId_ = jobId;

            Thread_ = new Thread(this.RunThread);
            Thread_.Start();
        }

        public void Dispose()
        {
            lock (Sync_)
            {
                ShouldStop_ = true;
                Monitor.Pulse(Sync_);
            }
            Thread_.Join();
        }

        public void Update(string jsonStatus)
        {
            lock (Sync_)
            {
                CurrentJsonStatus_ = jsonStatus;
            }
        }

        public void RunThread()
        {
            while (true)
            {
                lock (Sync_)
                {
                    Monitor.Wait(Sync_, INTERVAL);
                    if (ShouldStop_)
                    {
                        return;
                    }
                    if (CurrentJsonStatus_ != null)
                    {
                        DoUpdate(CurrentJsonStatus_);
                        CurrentJsonStatus_ = null;
                    }
                }
            }
        }

        private void DoUpdate(string jsonStatus)
        {
            Client_.UpdateState(JobId_, jsonStatus);
        }
    }
}
