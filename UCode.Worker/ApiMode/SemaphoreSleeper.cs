﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class SemaphoreSleeper : ISleeper
    {
        private const long MIN_DELAY = 5 * TimeSpan.TicksPerSecond;

        private bool FirstTime_;
        private bool LastRequestWasSuccessful_;
        private readonly HttpClient HttpClient_;
        private readonly Uri SemaphoreEndpoint_;

        public SemaphoreSleeper(HttpClient client, string semaphore)
        {
            FirstTime_ = true;
            HttpClient_ = client;
            SemaphoreEndpoint_ = !String.IsNullOrEmpty(semaphore) ? new Uri(semaphore) : null;
        }

        public void Sleep(CancellationToken token)
        {
            if (LastRequestWasSuccessful_)
            {
                LastRequestWasSuccessful_ = false;
                return;
            }

            long beginTicks = DateTime.Now.Ticks;

            bool fastPass = false;

            if (HasSemaphore())
            {
                fastPass = AskSemaphore(token);
            }
            else
            {
                fastPass = FirstTime_;
            }

            FirstTime_ = false;

            if (!fastPass)
            {
                long endTicks = DateTime.Now.Ticks;
                long ticksPassed = endTicks - beginTicks;
                if (ticksPassed >= 0 && ticksPassed < MIN_DELAY)
                {
                    token.WaitHandle.WaitOne(new TimeSpan(MIN_DELAY - ticksPassed));
                }                
            }
        }

        private bool AskSemaphore(CancellationToken token)
        {
            HttpContent requestContent = new StringContent(String.Empty);
            var task = HttpClient_.PostAsync(new Uri(SemaphoreEndpoint_, "wait"), requestContent, token);
            try
            {
                task.Wait(token);
            }
            catch (AggregateException ae)
            {
                return false;
            }
            return task.Result.IsSuccessStatusCode;
        }

        private bool HasSemaphore()
        {
            return HttpClient_ != null && SemaphoreEndpoint_ != null;
        }

        public void Reset()
        {
            LastRequestWasSuccessful_ = true;
        }
    }
}
