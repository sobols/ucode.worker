﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    /**
     * General
     */

    class WorkerHelloJson
    {
        public string MachineName;
        public string Name;
    }

    /**
     * Testing Job
     */

    class SourceCodeJson
    {
        [JsonProperty(Required = Required.Always)]
        public string Compiler;

        [JsonProperty(Required = Required.Always)]
        public string ResourceId;

        [JsonProperty(Required = Required.Always)]
        public string Filename;
    }

    class CheckerJson
    {
        public string Kind;
        public SourceCodeJson Source;
    }

    class ValidatorJson
    {
        public SourceCodeJson Source;
    }
    class ScorerJson
    {
        public SourceCodeJson Source;
    }
    class InteractorJson
    {
        public SourceCodeJson Source;
    }
    class FileJson
    {
        [JsonProperty(Required = Required.Always)]
        public string ResourceId;
    }

    class TestJson
    {
        public long Id;
        public FileJson Input;
        public FileJson Answer;
        public long TimeLimit;
        public long MemoryLimit;
        public int MaxScore = 1;
        public bool IsSample = false;
    }

    class ProblemJson
    {
        public long Id;
        public string Name;
        public string InputFileName = "";
        public string OutputFileName = "";
        public CheckerJson Checker;
        public ValidatorJson Validator;
        public ScorerJson Scorer;
        public InteractorJson Interactor;
        public List<TestJson> Tests;
        public bool RunTwice;
    }

    class TestingJobJson
    {
        public long Id;
        public SourceCodeJson Solution;
        public bool StopAfterFirstFailedTest;
        public ProblemJson Problem;
    }

    /**
     * Testing Report
     */

    class TestingReportJson
    {
        public string Outcome;
        public int FirstFailedTest = 0;
        public List<TestCaseResultJson> Tests;
        public int Score;
        public double FloatScore;
        public int MaxScore;
        public List<LogJson> Logs;
        public string GeneralFailureReason;
        public string GeneralFailureMessage;
        public bool SampleTestsPassed;
    }

    class LogJson
    {
        public string Kind;
        public string ResourceId;
    }

    class TestCaseResultJson
    {
        public long Id;
        public string Outcome;
        public int ExitCode;
        public long TimeLimit;
        public long TimeUsed;
        public long MemoryLimit;
        public long MemoryUsed;
        public int Score;
        public double FloatScore;
        public int MaxScore;
        public string CheckerMessage;
        public string InputResourceId;
        public string OutputResourceId;
        public string AnswerResourceId;
        public string StdoutResourceId;
        public string StderrResourceId;
        public bool IsSample;
    }

    /**
     * State
     */
    class StateJson
    {
        public string Status;
        public int TestNumber;
    }

    class CompilerJson
    {
        public string Handle;
        public string CompileCommand;
        public string RunCommand;
    }
}
