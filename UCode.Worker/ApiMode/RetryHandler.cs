﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    public class RetryHandler : DelegatingHandler
    {
        // https://stackoverflow.com/a/19650002

        // Strongly consider limiting the number of retries - "retry forever" is
        // probably not the most user friendly way you could respond to "the
        // network cable got pulled out."
        private const int MaxRetries = 3;

        public RetryHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            const int maxAttempts = 3;
            int timeout = 1;
            HttpResponseMessage response = null;

            for (int i = 0; i < maxAttempts; ++i)
            {
                try
                {
                    response = await base.SendAsync(request, cancellationToken);
                    break;
                }
                catch (HttpRequestException ex)
                {
                    Logger.Warning("HttpRequestException: {0}", ex.InnerException.Message);
                    if (i + 1 == maxAttempts)
                    {
                        throw;
                    }
                    await Task.Delay(timeout * 1000);
                    timeout *= 2;
                }
            }
            return response;
        }
    }
}
