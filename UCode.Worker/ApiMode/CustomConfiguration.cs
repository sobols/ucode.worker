﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace UCode.Worker.ApiMode
{
    [XmlType("ApiMode.CustomConfiguration")]
    public class CustomConfiguration
    {
        public string Endpoint;
        public string Token;
        public string Semaphore;
    }
}
