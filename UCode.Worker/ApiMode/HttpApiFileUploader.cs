﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace UCode.Worker.ApiMode
{
    class HttpApiUploadPack: IUploadPack
    {
        private readonly HttpClient Client_;
        private readonly RemoteFileKeeper Keeper_;
        private readonly Dictionary<string, IFileLocation> ToUpload_;

        public HttpApiUploadPack(HttpClient client, RemoteFileKeeper keeper)
        {
            Client_ = client;
            Keeper_ = keeper;
            ToUpload_ = new Dictionary<string, IFileLocation>();
        }

        public string EnqueueToUpload(IFileLocation file)
        {
            if (file == null)
            {
                return null;
            }

            using (Stream inputStream = file.OpenRead())
            {
                ResourceIdEx idEx = ResourceIdUtils.CalculateEx(inputStream);
                if (idEx.NeedToUpload && !ToUpload_.ContainsKey(idEx.ResourceId))
                {
                    ToUpload_.Add(idEx.ResourceId, file);
                }
                return idEx.ResourceId;
            }
        }

        public void Upload()
        {
            HashSet<string> newFiles = GetFilesToUpload(ToUpload_.Keys);
            Logger.Info("Uploading files: {0} of {1}", newFiles.Count, ToUpload_.Count);

            foreach (string resourceID in newFiles)
            {
                UploadSingleFile(resourceID, ToUpload_[resourceID]);
            }

            Logger.Info("Uploading done");
        }

        private static string FsUrl(string path, string queryString = null)
        {
            return (queryString == null)
                ? String.Format("fs/{0}", path)
                : String.Format("fs/{0}?{1}", path, queryString);
        }

        private void SetStates(IEnumerable<string> resourceIds, Dictionary<string, bool?> states)
        {
            var queryParams = resourceIds.Select(s => new KeyValuePair<string, string>("id", s));
            string query;
            using (var content = new FormUrlEncodedContent(queryParams))
            {
                query = content.ReadAsStringAsync().Result;
            }
            HttpResponseMessage response = Client_.GetAsync(FsUrl("status", query)).Result;
            response.EnsureSuccessStatusCode();

            var dict = JsonConvert.DeserializeObject<Dictionary<string, bool>>(response.Content.ReadAsStringAsync().Result);
            foreach (var kv in dict)
            {
                if (!states.ContainsKey(kv.Key))
                {
                    throw new MechanismException("API answers the files it was not requested about");
                }
                states[kv.Key] = kv.Value;
            }
        }

        private void SetAllStates(IEnumerable<string> resourceIds, Dictionary<string, bool?> states)
        {
            var currentIds = new List<string>();

            const int limit = 10;

            foreach (string resourceId in resourceIds)
            {
                if (!states.ContainsKey(resourceId))
                {
                    if (Keeper_.IsFileSurelyPresent(resourceId))
                    {
                        states.Add(resourceId, true);
                    }
                    else
                    {
                        states.Add(resourceId, null);
                        currentIds.Add(resourceId);
                    }
                }
                if (currentIds.Count >= limit)
                {
                    SetStates(currentIds, states);
                    currentIds.Clear();
                }
            }

            if (currentIds.Count > 0)
            {
                SetStates(currentIds, states);
                currentIds.Clear();
            }
        }

        private HashSet<string> GetFilesToUpload(IEnumerable<string> resourceIds)
        {
            var states = new Dictionary<string, bool?>();
            SetAllStates(resourceIds, states);
            
            var result = new HashSet<string>();

            foreach (var kv in states)
            {
                if (!kv.Value.HasValue)
                {
                    throw new MechanismException(String.Format("Unable to check state of file {0}", kv.Key));
                }
                if (!kv.Value.Value)
                {
                    result.Add(kv.Key);
                }
                else
                {
                    Keeper_.SetFileIsSurelyPresent(kv.Key);
                }
            }
            return result;
        }

        private void UploadSingleFile(string resourceId, IFileLocation location)
        {
            Logger.Debug("Uploading file {0}", resourceId);
            using (Stream stream = location.OpenRead())
            {
                Logger.Debug("Path: {0}, size: {1}", location, location.Size);
                var result = Client_.PutAsync(FsUrl(resourceId), new StreamContent(stream)).Result;
                result.EnsureSuccessStatusCode();
            }
            Keeper_.SetFileIsSurelyPresent(resourceId);
        }
    }

    class HttpApiFileUploader: IFileUploader
    {
        HttpClient HttpClient_;
        RemoteFileKeeper Keeper_;

        public HttpApiFileUploader(HttpClient httpClient, RemoteFileKeeper keeper)
        {
            HttpClient_ = httpClient;
            Keeper_ = keeper;
        }

        public IUploadPack CreatePack()
        {
            return new HttpApiUploadPack(HttpClient_, Keeper_);
        }
    }
}

