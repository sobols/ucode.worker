﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class HttpApiClient
    {
        private readonly HttpClient HttpClient_;

        private const string JSON_CONTENT_TYPE = "application/json";

        public HttpApiClient(HttpClient httpClient)
        {
            HttpClient_ = httpClient;
        }

        public class JobDescription
        {
            public enum Outcome
            {
                OK,
                NothingToTest,
                Error,
            }

            public Outcome Result;
            public long ID;
            public string Json;

            public bool IsError
            {
                get
                {
                    return Result == Outcome.Error;
                }
            }
            public bool IsNothingToTest
            {
                get
                {
                    return Result == Outcome.NothingToTest;
                }
            }

            public static JobDescription Job(long id, string json)
            {
                return new JobDescription() { Result = Outcome.OK, ID = id, Json = json };
            }
            public static JobDescription Nothing()
            {
                return new JobDescription() { Result = Outcome.NothingToTest };
            }
            public static JobDescription Error()
            {
                return new JobDescription() { Result = Outcome.Error };
            }
        }

        public void PushCompilerSettings(string compilerSettings)
        {
            HttpContent requestContent = new StringContent(compilerSettings, Encoding.UTF8, JSON_CONTENT_TYPE);
            var task = HttpClient_.PostAsync("compiler-settings", requestContent);
            task.Wait();
        }

        /**
         * Returns JSON data or null if no job is ready to process.
         */
        public JobDescription FetchJob(string welcomeMessage)
        {
            HttpContent requestContent = new StringContent(welcomeMessage, Encoding.UTF8, JSON_CONTENT_TYPE);

            var sw = Stopwatch.StartNew();

            var task = HttpClient_.PostAsync("jobs/take", requestContent);
            try
            {
                task.Wait();
            }
            catch (AggregateException ae)
            {
                ae.Handle((x) =>
                {
                    Logger.Info("Fetch job: HTTP request error ({0})", x.Message);
                    return true;
                });
                return JobDescription.Error();
            }

            var response = task.Result;
            sw.Stop();

            if (response.IsSuccessStatusCode)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                JToken token = JObject.Parse(json);
                long id = (long)token.SelectToken("id");
                return JobDescription.Job(id, json);
            }
            else
            {
                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Logger.Info("Nothing to test (took {0} ms)", sw.ElapsedMilliseconds);
                    return JobDescription.Nothing();
                }
                else
                {
                    Logger.Info("Fetch job: HTTP response error ({0})", response.StatusCode);
                    try
                    {
                        Logger.Info(response.Content.ReadAsStringAsync().Result);
                    }
                    catch (Exception)
                    {
                    }
                    return JobDescription.Error();
                }
            }
        }

        public void UpdateState(long jobId, string json)
        {
            HttpContent requestContent = new StringContent(json, Encoding.UTF8, JSON_CONTENT_TYPE);
            string url = String.Format("jobs/{0}/state", jobId);
            HttpClient_.PutAsync(url, requestContent); // do not wait for result
        }

        public void PutReport(long jobId, string json)
        {
            HttpContent requestContent = new StringContent(json, Encoding.UTF8, JSON_CONTENT_TYPE);
            string url = String.Format("jobs/{0}/result", jobId);
            var result = HttpClient_.PutAsync(url, requestContent).Result;
            if (!result.IsSuccessStatusCode)
            {
                try
                {
                    Logger.Info(result.Content.ReadAsStringAsync().Result);
                }
                catch (Exception)
                {
                }
                throw new MechanismException();
            }
        }

        public void Cancel(long jobId)
        {
            HttpContent requestContent = new StringContent("");
            string url = String.Format("jobs/{0}/cancel", jobId);
            var result = HttpClient_.PutAsync(url, requestContent).Result;
            if (result.IsSuccessStatusCode)
            {
                Logger.Info("Job #{0} has been cancelled", jobId);
            }
            else
            {
                Logger.Warning("Unable to gracefully cancel job #{0}", jobId);
            }
        }
    }
}
