﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class HttpApiFileDowloader: IFileDownloader
    {
        private readonly HttpClient Client_;
        private readonly ISingleFileStorage FileStorage_;
        private readonly RemoteFileKeeper Keeper_;
        private readonly bool VerifyIntegrity_;
        private readonly ConcurrentDictionary<string, IFileLocation> Cache_;

        public HttpApiFileDowloader(HttpClient client, ISingleFileStorage fileStroage, RemoteFileKeeper keeper, bool verifyIntegrity = true)
        {
            Client_ = client;
            FileStorage_ = fileStroage;
            Keeper_ = keeper;
            VerifyIntegrity_ = verifyIntegrity;
            Cache_ = new ConcurrentDictionary<string, IFileLocation>();
        }

        public IFileLocation Download(string id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            IFileLocation file;
            if (Cache_.TryGetValue(id, out file))
            {
                return file;
            }

            return Cache_[id] = DoDownload(id);
        }

        private IFileLocation DoDownload(string id)
        {
            IFileLocation file = null;

            Logger.Debug("Downloading file {0}", id);
            string path = "fs/" + id;
            using (Stream httpStream = Client_.GetStreamAsync(path).Result)
            {
                file = FileStorage_.Put(new StreamDataSource(httpStream));
            }

            if (VerifyIntegrity_)
            {
                VerifyFile(file, id);
            }
            Keeper_.SetFileIsSurelyPresent(id);
            return file;
        }

        private static void VerifyFile(IFileLocation file, string idExpected)
        {
            using (Stream fileStream = file.OpenRead())
            {
                string idFound = ResourceIdUtils.Calculate(fileStream);
                if (idFound != idExpected)
                {
                    string msg = String.Format(
                        "downloaded file is corrupt (file has id {0} instead of {1})",
                        idFound, idExpected
                    );
                    throw new MechanismException(msg);
                }
            }
        }
    }
}
