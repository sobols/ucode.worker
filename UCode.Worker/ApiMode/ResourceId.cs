﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    struct ResourceIdEx
    {
        public readonly string ResourceId;
        public readonly bool NeedToUpload;

        public ResourceIdEx(string resourceId, bool needToUpload)
        {
            ResourceId = resourceId;
            NeedToUpload = needToUpload;
        }
    }

    static class ResourceIdUtils
    {
        private const int Limit_ = 20;

        public static ResourceIdEx CalculateEx(Stream inputStream)
        {
            if (inputStream.Length < Limit_)
            {
                using (var memoryStream = new MemoryStream())
                {
                    inputStream.CopyTo(memoryStream);
                    byte[] key = memoryStream.ToArray();
                    return new ResourceIdEx(ToHex(key), false);
                }
            }
            else
            {
                SHA1Managed sha = new SHA1Managed();
                byte[] key = sha.ComputeHash(inputStream);
                return new ResourceIdEx(ToHex(key), true);
            }
        }

        public static string Calculate(Stream inputStream)
        {
            return CalculateEx(inputStream).ResourceId;
        }

        private static string ToHex(byte[] data)
        {
            return BitConverter.ToString(data).Replace("-", String.Empty).ToLower();
        }
    }
}
