﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    interface IFileDownloader
    {
        IFileLocation Download(string id);
    }

    interface IUploadPack
    {
        string EnqueueToUpload(IFileLocation file);
        void Upload();
    }

    interface IFileUploader
    {
        IUploadPack CreatePack();
    }

    delegate void StatusUpdater(string statusJson);

    class JsonTester
    {
        private TestingMachineFactory MachineFactory_;
        private ISingleFileStorageFactory OutputFileStorageFactory_;
        private IFileDownloader FileDownloader_;
        private IFileUploader FileUploader_;

        private readonly WorkerHelloJson HelloJson_;
        private readonly JsonSerializerSettings JsonSettings_;

        private readonly Object FetchJobLock = new Object();

        private const long FAKE_JOB_ID = 0;

        private const string ACCEPTED = "ACCEPTED";
        private const string COMPILATION_ERROR = "COMPILATION_ERROR";
        private const string CHECK_FAILED = "CHECK_FAILED";

        private const string COMPILING = "COMPILING";
        private const string TESTING = "TESTING";
        private const string FINISHING = "FINISHING";

        private const string SOLUTION_COMPILATION = "SOLUTION_COMPILATION";
        private const string CHECKER_COMPILATION = "CHECKER_COMPILATION";

        private const string IRUNNER = "IRUNNER";
        private const string TESTLIB_H = "TESTLIB_H";
        private const string ACCEPT_ALL = "ACCEPT_ALL";

        private const long MAX_STD_STREAM_LENGTH = 1 * 1024 * 1024;
        private const long MAX_OUTPUT_LENGTH = 32 * 1024 * 1024;

        public JsonTester(TestingMachineFactory machineFactory, ISingleFileStorageFactory outputFileStorageFactory,
                          IFileDownloader fileDownloader, IFileUploader fileUploader, JsonSerializerSettings jsonSettings)
        {
            MachineFactory_ = machineFactory;
            OutputFileStorageFactory_ = outputFileStorageFactory;
            FileDownloader_ = fileDownloader;
            FileUploader_ = fileUploader;
            JsonSettings_ = jsonSettings;

            string name = Guid.NewGuid().ToString("N");
            HelloJson_ = new WorkerHelloJson
            {
                MachineName = Environment.MachineName,
                Name = name,
            };
            Logger.Info("I am {0}", name);
            
        }

        public string GetWelcomeMessage()
        {
            return JsonConvert.SerializeObject(HelloJson_, JsonSettings_);
        }

        public string Run(string json, int thrNum, int? cpuCore, CancellationToken token, StatusUpdater statusUpdater = null)
        {
            using (ISingleFileStorage fs = OutputFileStorageFactory_.Create("outputs"))
            {
                TestingMachine tm = MachineFactory_.Create(fs, cpuCore);

                if (statusUpdater != null)
                {
                    tm.Compiling += (s, e) => UpdateState(statusUpdater, COMPILING);
                    tm.TestCaseTesting += (s, e) => UpdateState(statusUpdater, TESTING, e.TestNumber);
                }

                JobReport jobReport = tm.FetchAndTest(FAKE_JOB_ID, (jobId) => DoFetch(json), thrNum, token);

                if (statusUpdater != null)
                {
                    UpdateState(statusUpdater, FINISHING);
                }

                IUploadPack pack = FileUploader_.CreatePack();
                string report = MakeReportJson(jobReport.Job, jobReport.Report, pack);
                pack.Upload();
                return report;
            }
        }

        private TestingJob DoFetch(string json)
        {
            lock (FetchJobLock)
            {
                TestingJobJson jobJson = ParseJson(json);
                ProblemInstance problem = DoFetchProblem(jobJson.Problem);
                SourceCode source = DoFetchSourceCode(jobJson.Solution);
                return new TestingJob(jobJson.Id, problem, source, jobJson.StopAfterFirstFailedTest);
            }
        }

        private TestingJobJson ParseJson(string jsonData)
        {
            TestingJobJson jobJson = null;
            try
            {
                jobJson = JsonConvert.DeserializeObject<TestingJobJson>(jsonData, JsonSettings_);
            }
            catch (JsonException ex)
            {
                throw new AbortTestingException(FailReason.InvalidJobJson, ex.Message);
            }
            if (jobJson == null)
            {
                throw new AbortTestingException(FailReason.InvalidJobJson, "null job");
            }
            return jobJson;
        }

        private ProblemInstance DoFetchProblem(ProblemJson json)
        {
            if (json == null)
            {
                // no problem: "just compile the solution" job
                return null;
            }

            List<TestCase> testCases = new List<TestCase>();

            if (json.Tests != null)
            {
                foreach (TestJson test in json.Tests)
                {
                    testCases.Add(DoFetchTestCase(test));
                }
            }

            CheckerKind checkerKind = CheckerKind.FileCompare;
            SourceCode checkerSource = null;
            if (json.Checker != null)
            {
                if (json.Checker.Kind == IRUNNER)
                {
                    checkerKind = CheckerKind.IRunner;
                    checkerSource = DoFetchSourceCode(json.Checker.Source);
                }
                if (json.Checker.Kind == TESTLIB_H)
                {
                    checkerKind = CheckerKind.TestlibH;
                    checkerSource = DoFetchSourceCode(json.Checker.Source);
                }
                if (json.Checker.Kind == ACCEPT_ALL)
                {
                    checkerKind = CheckerKind.AcceptAll;
                }
            }

            SourceCode validatorSource = null;
            if (json.Validator != null)
            {
                validatorSource = DoFetchSourceCode(json.Validator.Source);
            }
            SourceCode scorerSource = null;
            if (json.Scorer != null)
            {
                scorerSource = DoFetchSourceCode(json.Scorer.Source);
            }
            SourceCode interactorSource = null;
            if (json.Interactor != null)
            {
                interactorSource = DoFetchSourceCode(json.Interactor.Source);
            }

            ProblemInstance result = new ProblemInstance(
                json.Name,
                json.InputFileName,
                json.OutputFileName,
                checkerKind,
                checkerSource,
                validatorSource,
                scorerSource,
                interactorSource,
                testCases,
                json.RunTwice
            );
            return result;
        }

        private SourceCode DoFetchSourceCode(SourceCodeJson json)
        {
            if (json == null)
            {
                // no source code: i.e. "just validate tests" job
                return null;
            }

            IFileLocation location = FileDownloader_.Download(json.ResourceId);
            byte[] code;
            using (ReadAccessToken rt = location.MakeReadable())
            {
                code = File.ReadAllBytes(rt.Path);
            }
            return new SourceCode(json.Compiler, code, json.Filename);
        }

        private TestCase DoFetchTestCase(TestJson json)
        {
            return new TestCase(
                json.Id,
                DoFetchTestCaseFile(json.Input),
                DoFetchTestCaseFile(json.Answer),
                LongOrNone(json.TimeLimit),
                LongOrNone(json.MemoryLimit),
                json.MaxScore,
                json.IsSample
            );
        }

        private TestCaseFile DoFetchTestCaseFile(FileJson json)
        {
            if (json != null)
            {
                if (json.ResourceId != null)
                {
                    return new TestCaseFile(FileDownloader_.Download(json.ResourceId));
                }
            }
            return null;
        }

        private static long? LongOrNone(long x)
        {
            if (x == 0)
            {
                return null;
            }
            else
            {
                return x;
            }
        }

        private void TrimFiles(TestCaseResult result)
        {
            if (result.OutputHandle != null)
            {
                result.OutputHandle.Trim(MAX_OUTPUT_LENGTH);
            }
            if (result.StdOutHandle != null && result.StdOutHandle != result.OutputHandle)
            {
                result.StdOutHandle.Trim(MAX_STD_STREAM_LENGTH);
            }
            if (result.StdErrHandle != null && result.StdErrHandle != result.OutputHandle)
            {
                result.StdErrHandle.Trim(MAX_STD_STREAM_LENGTH);
            }
        }

        private TestingReportJson ToJson(ProblemInstance problem, TestingReport report, IUploadPack pack)
        {
            TestingReportJson json = new TestingReportJson();

            switch (report.Outcome)
            {
                case SubmissionOutcome.Correct:
                    json.Outcome = ACCEPTED;
                    break;
                case SubmissionOutcome.SolutionCompilationError:
                    json.Outcome = COMPILATION_ERROR;
                    break;
                case SubmissionOutcome.Incorrect:
                    json.Outcome = ToJson(report.ResultOfFirstFailedTest.Outcome);
                    break;
                default:
                    json.Outcome = CHECK_FAILED;
                    break;
            }
            json.FirstFailedTest = report.FirstFailedTestNo;

            if (problem != null)
            {
                json.Score = Convert.ToInt32(report.Score);
                json.FloatScore = report.Score;
                json.MaxScore = Convert.ToInt32(problem.CalculateMaxScore());

                if (report.TestCaseResults.Count > 0)
                {
                    // The solution has been run on tests, probably the samples got passed.
                    json.SampleTestsPassed = true;
                }

                json.Tests = new List<TestCaseResultJson>();
                for (int i = 0; i < report.TestCaseResults.Count; ++i)
                {
                    TrimFiles(report.TestCaseResults[i]);
                    json.Tests.Add(ToJson(problem.Tests[i], report.TestCaseResults[i], pack));

                    if (problem.Tests[i].IsSample && !report.TestCaseResults[i].OK)
                    {
                        json.SampleTestsPassed = false;
                    }
                }
            }

            json.Logs = new List<LogJson>();
            if (report.SolutionCompilationLogFile != null)
            {
                json.Logs.Add(new LogJson
                {
                    Kind = SOLUTION_COMPILATION,
                    ResourceId = pack.EnqueueToUpload(report.SolutionCompilationLogFile)
                });
            }
            if (report.GeneralFailureReason == FailReason.CheckerNotCompiled && report.CheckerCompilationLogFile != null)
            {
                json.Logs.Add(new LogJson
                {
                    Kind = CHECKER_COMPILATION,
                    ResourceId = pack.EnqueueToUpload(report.CheckerCompilationLogFile)
                });
            }

            if (report.GeneralFailureReason != FailReason.Unknown)
            {
                json.GeneralFailureReason = Util.CapitalizeEnum(report.GeneralFailureReason.ToString());
                json.GeneralFailureMessage = report.GeneralFailureMessage;
            }
            return json;
        }

        private static string Compact(string s, int maxLength)
        {
            if (!String.IsNullOrEmpty(s) && (s.Length > maxLength))
            {
                return s.Substring(0, maxLength);
            }
            return s;
        }

        private TestCaseResultJson ToJson(TestCase test, TestCaseResult result, IUploadPack pack)
        {
            return new TestCaseResultJson()
            {
                Id = result.TestID,
                Outcome = ToJson(result.Outcome),
                ExitCode = result.ExitCode,
                TimeLimit = test.TimeLimit.GetValueOrDefault(),
                TimeUsed = result.TimeUsed,
                MemoryLimit = test.MemoryLimit.GetValueOrDefault(),
                MemoryUsed = result.MemoryUsed,
                MaxScore = Convert.ToInt32(test.MaxScore),
                Score = Convert.ToInt32(result.Score),
                FloatScore = result.Score,
                CheckerMessage = Compact(result.CheckerMessage.AsString(NaturalLanguage.English), 255),
                InputResourceId = Upload(result.InputHandle, pack),
                OutputResourceId = Upload(result.OutputHandle, pack),
                AnswerResourceId = Upload(result.AnswerHandle, pack),
                StdoutResourceId = Upload(result.StdOutHandle, pack),
                StderrResourceId = Upload(result.StdErrHandle, pack),
                IsSample = test.IsSample,
            };
        }

        private static string Upload(IFileLocation location, IUploadPack pack)
        {
            return (location != null) ? pack.EnqueueToUpload(location) : null;
        }

        private string ToJson(TestCaseOutcome outcome)
        {
            if (outcome == TestCaseOutcome.OK)
            {
                return ACCEPTED;
            }
            else
            {
                return Util.CapitalizeEnum(outcome.ToString());
            }
        }

        string MakeReportJson(TestingJob job, TestingReport report, IUploadPack pack)
        {
            TestingReportJson testingReportJson = ToJson(job != null ? job.Problem : null, report, pack);
            return JsonConvert.SerializeObject(testingReportJson, JsonSettings_);
        }

        void UpdateState(StatusUpdater updater, string status, int testNumber = 0)
        {
            StateJson stateJson = new StateJson { Status = status, TestNumber = testNumber };
            updater(JsonConvert.SerializeObject(stateJson, JsonSettings_));
        }
    }
}
