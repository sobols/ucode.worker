﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class Instance : WorkerInstance
    {
        private HttpClient HttpClient_;
        private RemoteFileKeeper Keeper_;
        private HttpApiFileDowloader FileDownloader_;
        private HttpApiFileUploader FileUploader_;
        private HttpApiClient ApiClient_;
        private JsonSerializerSettings JsonSettings_;
        private int NumThreads_;
        private List<int> Cores_;

        public Instance(Configuration config)
            : base(config)
        {
            string token = config.ApiModeConfiguration.Token;
            string endpoint = config.ApiModeConfiguration.Endpoint;
            string semaphoreEndpoint = config.ApiModeConfiguration.Semaphore;
            Logger.Info("API endpoint is {0}", endpoint);

            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.DefaultConnectionLimit = 4;
            ServicePointManager.DnsRefreshTimeout = -1; // infinite
            if (!config.KeepAlive)
            {
                ServicePointManager.SetTcpKeepAlive(false, 0, 0);
            }

            HttpClient_ = CreateClient(endpoint, token, config.KeepAlive);

            Keeper_ = new RemoteFileKeeper();

            FileDownloader_ = new HttpApiFileDowloader(HttpClient_, RootDir_.Data.Create("download"), Keeper_);
            FileUploader_ = new HttpApiFileUploader(HttpClient_, Keeper_);

            ApiClient_ = new HttpApiClient(HttpClient_);

            JsonSettings_ = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                MissingMemberHandling = MissingMemberHandling.Ignore,
            };

            Sleeper_ = new SemaphoreSleeper(HttpClient_, semaphoreEndpoint);

            NumThreads_ = config.NumThreads;
            if (NumThreads_ != 1)
            {
                Logger.Info("Running {0} threads", NumThreads_);
            }

            Cores_ = config.Cores;
            if (Cores_ != null)
            {
                Logger.Info("Process affinity: {0}", String.Join(", ", Cores_));
            }
        }

        protected override void DoRun(CancellationToken token)
        {
            ApiClient_.PushCompilerSettings(MakeCompilersJson());

            var tester = new JsonTester(TestingMachineFactory_, RootDir_.Data, FileDownloader_, FileUploader_, JsonSettings_);

            using (var q = new BlockingCollection<int>())
            {
                for (int i = 0; i < NumThreads_; ++i)
                {
                    q.Add(i);
                }
                List<Task> tasks = new List<Task>();
                try
                {
                    while (!token.IsCancellationRequested)
                    {
                        int thrNum = q.Take(token);
                        tasks.RemoveAll(t => t.IsCompleted);

                        Sleeper_.Sleep(token);
                        var j = ApiClient_.FetchJob(tester.GetWelcomeMessage());
                        if (j.IsError || j.IsNothingToTest)
                        {
                            q.Add(thrNum);
                            continue;
                        }
                        Logger.Info("Got job!");
                        Sleeper_.Reset();

                        var newTask = Task.Factory.StartNew(() => ProcessJob(tester, j, q, thrNum, token));
                        tasks.Add(newTask);
                    }
                }
                catch (OperationCanceledException)
                {
                }
                finally
                {
                    Logger.Info("{0} tasks to wait", tasks.Count);
                    Task.WaitAll(tasks.ToArray());
                }
            }
        }

        private void ProcessJob(JsonTester tester, HttpApiClient.JobDescription j, BlockingCollection<int> q, int thrNum, CancellationToken token)
        {
            try
            {
                string json;
                int? cpuCore = null;
                if (Cores_ != null)
                {
                    cpuCore = Cores_[thrNum];
                }
                using (var statusUpdater = new GradualStatusUpdater(ApiClient_, j.ID))
                {
                    json = tester.Run(j.Json, thrNum, cpuCore, token, (s) => statusUpdater.Update(s));
                }
                ApiClient_.PutReport(j.ID, json);
            }
            catch (OperationCanceledException)
            {
                ApiClient_.Cancel(j.ID);
            }
            catch (Exception ex)
            {
                // task is left in incomplete state
                Logger.Error("Unhandled exception while running job #{0}: {1}\n{2}", j.ID, ex.GetType().Name, ex.ToString());
            }
            finally
            {
                q.Add(thrNum);
            }
        }
 
        private static HttpClient CreateClient(string endpoint, string token, bool keepAlive)
        {
            HttpClientHandler hch = new HttpClientHandler();
            hch.Proxy = null;
            hch.UseProxy = false;

            var client = new HttpClient(new RetryHandler(hch));
            client.BaseAddress = new Uri(endpoint);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
            client.DefaultRequestHeaders.Add("Worker-Token", token);
            client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest"); // To force django return error pages in plain text instead of HTML
            client.DefaultRequestHeaders.ConnectionClose = !keepAlive;
            client.DefaultRequestHeaders.ExpectContinue = false;
            return client;
        }

        private string MakeCompilersJson()
        {
            var list = new List<CompilerJson>();
            foreach (string langID in ProgLangs_.ListLangIDs())
            {
                var progLang = ProgLangs_.Get(langID);
                list.Add(new CompilerJson
                {
                    Handle = langID,
                    CompileCommand = progLang.CompileCmd,
                    RunCommand =progLang.RunCmd,
                });
            }
            return JsonConvert.SerializeObject(list, JsonSettings_);
        }
    }
}
