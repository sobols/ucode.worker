﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UCode.Worker.ApiMode
{
    class RemoteFileKeeper
    {
        private ConcurrentDictionary<string, bool> Ids_;

        public RemoteFileKeeper()
        {
            Ids_ = new ConcurrentDictionary<string, bool>();
        }

        public bool IsFileSurelyPresent(string resourceId)
        {
            return Ids_.ContainsKey(resourceId);
        }

        public void SetFileIsSurelyPresent(string resourceId)
        {
            Ids_.TryAdd(resourceId, true);
        }
    }
}
